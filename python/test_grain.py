import pytest

@pytest.mark.parametrize("number,expected", [
    (1, 1),
    (2, 2),
    (3, 4),
    (4, 8),
    (5, 16)
])
def test_square(number, expected):
    assert square(number) == expected

def square(number):
    return 2 ** (number - 1)