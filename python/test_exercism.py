import pytest

def test_steps_when_number_is_1():
    assert steps(1) == 0

def test_steps_when_number_is_2():
    assert steps(2) == 1

def test_steps_when_number_is_12():
    assert steps(12) == 9

def test_steps_when_number_is_16():
    assert steps(16) == 4

def test_large_number_of_even_and_odd_steps():
    assert steps(1000000), 152

def steps(number):
    step = 0
    while number != 1:
        step += 1
        if number % 2 == 0:
            number = number / 2
        else:
            number = number * 3 + 1
    return step