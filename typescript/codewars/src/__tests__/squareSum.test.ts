test('empty array', () => {
    expect(squareSum([])).toBe(0);
  });

test('one number in array', () =>  {
    expect(squareSum([1])).toBe(1);
    expect(squareSum([2])).toBe(4);
});

test('two numbers in array', () =>  {
    expect(squareSum([1, 2])).toBe(5);
    expect(squareSum([0, 1, 2, 5])).toBe(30);
});

function squareSum(numbers: number[]): number {
    return numbers.length == 0 ? 
        0 
        : Math.pow(numbers[0], 2) + squareSum(numbers.slice(1, numbers.length + 1));
}

// meilleure solution : 
// return numbers.reduce((prev, curr) => prev + curr * curr, 0);