# Kata

This is my personal dojo. 
The code will not always be clean, it will depend on my goal for each kata.

## Sources

* [codewars](https://www.codewars.com/kata/search/java?q=&r[]=-5&xids=played&beta=false&order_by=popularity%20desc)
* [codingame](https://www.codingame.com/home)
* https://katalyst.codurance.com/browse
* https://www.sammancoaching.org/kata_descriptions/index.html
* https://www.sammancoaching.org/kata_descriptions/index.html
* https://kata-log.rocks/
* http://codekata.com/
* https://github.com/christianhujer/expensereport
* https://github.com/gamontal/awesome-katas
* https://cyber-dojo.org/creator/choose_problem

## DOC

* [Junit @ParametrizedTest](https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests)