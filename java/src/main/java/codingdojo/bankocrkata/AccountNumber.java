package codingdojo.bankocrkata;

import java.util.*;
import java.util.stream.*;

class AccountNumber {

    private final String value;

    public AccountNumber(String accountNumber) {
        this.value = accountNumber;
    }

    public boolean isValid() {
        return hasUnparsableNumber() && hasValidChecksum();
    }

    public boolean hasUnparsableNumber() {
        return !value.contains("?");
    }

    private boolean hasValidChecksum() {
        List<Integer> accountAsList = Arrays.stream(value.split(""))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        Collections.reverse(accountAsList);
        int sum = 0;
        for (int i = 0; i < accountAsList.size(); i++) {
            sum += accountAsList.get(i) * (i + 1);
        }
        return sum % 11 == 0;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountNumber accountNumber = (AccountNumber) o;
        return value.equals(accountNumber.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return value;
    }
}
