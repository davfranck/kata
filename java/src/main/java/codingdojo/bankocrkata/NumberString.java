package codingdojo.bankocrkata;

import java.util.*;

class NumberString {

    private static final NumberString ZERO = new NumberString(
            " _ ",
            "| |",
            "|_|");
    private static final NumberString ONE = new NumberString(
            "   ",
            "  |",
            "  |");
    private static final NumberString TWO = new NumberString(
            " _ ",
            " _|",
            "|_ ");
    private static final NumberString THREE = new NumberString(
            " _ ",
            " _|",
            " _|");
    private static final NumberString FOUR = new NumberString(
            "   ",
            "|_|",
            "  |");
    private static final NumberString FIVE = new NumberString(
            " _ ",
            "|_ ",
            " _|");
    private static final NumberString SIX = new NumberString(
            " _ ",
            "|_ ",
            "|_|");
    private static final NumberString SEVEN = new NumberString(
            " _ ",
            "  |",
            "  |");
    private static final NumberString EIGHT = new NumberString(
            " _ ",
            "|_|",
            "|_|");
    private static final NumberString NINE = new NumberString(
            " _ ",
            "|_|",
            " _|");

    private static final Map<NumberString, Character> CHAR_BY_NUMBER = Map.of(
            ZERO, '0', ONE, '1', TWO, '2', THREE, '3', FOUR, '4', FIVE, '5',
            SIX, '6', SEVEN, '7', EIGHT, '8', NINE, '9');

    private final String line1;
    private final String line2;
    private final String line3;

    public NumberString(String line1, String line2, String line3, int i) {
        this.line1 = line1.substring(i, i + 3);
        this.line2 = line2.substring(i, i + 3);
        this.line3 = line3.substring(i, i + 3);
    }

    public NumberString(String line1, String line2, String line3) {
        this(line1, line2, line3, 0);
    }

    public char toChar() {
        return CHAR_BY_NUMBER.getOrDefault(this, '?');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumberString that = (NumberString) o;
        return line1.equals(that.line1) && line2.equals(that.line2) && line3.equals(that.line3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(line1, line2, line3);
    }
}
