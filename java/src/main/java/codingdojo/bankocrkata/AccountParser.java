package codingdojo.bankocrkata;

import java.util.*;

public class AccountParser {

    private AccountParser() {
        
    }

    public static List<AccountNumber> parseAccounts(List<String> allLines) {
        List<AccountNumber> accountNumbers = new ArrayList<>();
        for (int i = 0; i < allLines.size(); i = i + 4) {
            AccountNumber accountNumber = getOneAccount(allLines, i);
            accountNumbers.add(accountNumber);
        }
        return accountNumbers;
    }

    private static AccountNumber getOneAccount(List<String> allLines, int firstLineIndex) {
        String line1 = allLines.get(firstLineIndex);
        String line2 = allLines.get(firstLineIndex + 1);
        String line3 = allLines.get(firstLineIndex + 2);
        StringBuilder accountNumber = new StringBuilder();
        for (int i = 0; i <= 24; i = i + 3) {
            NumberString numberString = new NumberString(line1, line2, line3, i);
            char number = numberString.toChar();
            accountNumber.append(number);
        }
        return new AccountNumber(accountNumber.toString());
    }

}
