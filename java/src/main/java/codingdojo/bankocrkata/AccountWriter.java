package codingdojo.bankocrkata;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class AccountWriter {

    private AccountWriter() {
    }

    public static void writeAccountsToFile(Path tempFile, List<AccountNumber> accountNumbers) throws IOException {
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(tempFile))) {
            accountNumbers.forEach(accountNumber -> {
                if (accountNumber.isValid()) {
                    printWriter.println(accountNumber.toString());
                } else if (accountNumber.hasUnparsableNumber()) {
                    printWriter.println(accountNumber.toString() + " ILL");
                } else {
                    printWriter.println(accountNumber.toString() + " ERR");
                }
            });
        }

    }
}
