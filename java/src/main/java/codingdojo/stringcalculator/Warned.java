package codingdojo.stringcalculator;

public class Warned {
    public final String message;
    public final Double value;

    private Warned(String message, Double value) {
        this.message = message;
        this.value = value;
    }

    public static Warned ofValue(Double value) {
        return new Warned(null, value);
    }

    public static Warned ofError(String message) {
        return new Warned(message, null);
    }

    public boolean hasValue() {
        return value != null;
    }
}
