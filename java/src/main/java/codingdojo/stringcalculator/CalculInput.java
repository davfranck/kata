package codingdojo.stringcalculator;

import java.util.*;
import java.util.stream.*;

public class CalculInput {
    public final String input;
    public final Character delimiter;

    public CalculInput(String input, Character delimiter) {
        this.input = input;
        this.delimiter = delimiter;
    }

    public static CalculInput from(String input) {
        Character delimiter = null;
        String calcIn = input;
        if (input.startsWith("//")) {
            delimiter = input.charAt(2);
            calcIn = input.substring(4);
        }
        return new CalculInput(calcIn, delimiter);
    }

    public boolean isEmpty() {
        return input.isEmpty();
    }

    public String[] split(List<Character> separators) {
        String delimiters = Stream.concat(Stream.ofNullable(delimiter), separators.stream())
                .map(String::valueOf)
                .collect(Collectors.joining());
        return input.split("[" + delimiters + "]");
    }
}
