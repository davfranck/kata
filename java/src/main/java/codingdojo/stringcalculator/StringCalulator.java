package codingdojo.stringcalculator;

import java.util.*;

public class StringCalulator {

    static final List<Character> SEPARATORS = List.of(',', '\n');

    private StringCalulator() {

    }

    public static Warned add(String input) {
        var calculInput = CalculInput.from(input);
        return checkError(calculInput.input)
                .map(Warned::ofError)
                .orElseGet(() -> calculate(calculInput));
    }

    private static Optional<String> checkError(String input) {
        if (input.endsWith(",")) {
            return Optional.of("Number expected but EOF found");
        }
        Integer invalidIndex = firstInvalidIndex(input);
        if (invalidIndex != null) {
            return Optional.of("Number expected but '" + input.charAt(invalidIndex) + "' found at position " + invalidIndex);
        }
        return Optional.empty();
    }

    private static Integer firstInvalidIndex(String input) {
        ArrayList<Integer> separatorsIndex = separatorsIndex(input);
        return firstInvalidIndex(separatorsIndex);
    }

    private static ArrayList<Integer> separatorsIndex(String input) {
        var separatorsIndex = new ArrayList<Integer>();
        for (var i = 0; i < input.length(); i++) {
            var charAt = input.charAt(i);
            if (SEPARATORS.contains(charAt)) {
                separatorsIndex.add(i);
            }
        }
        return separatorsIndex;
    }

    private static Warned calculate(CalculInput input) {
        if (input.isEmpty()) {
            return Warned.ofValue((double) 0);
        }
        String[] numbersAsString = input.split(SEPARATORS);
        double sum = Arrays.stream(numbersAsString)
                .mapToDouble(Double::valueOf)
                .sum();
        return Warned.ofValue(sum);
    }

    private static Integer firstInvalidIndex(ArrayList<Integer> separatorsIndex) {
        Integer invalidIndex = null;
        for (var i = 0; i < separatorsIndex.size() - 1; i++) {
            boolean nextSeparatorIsNextToCurrent = separatorsIndex.get(i) == separatorsIndex.get(i + 1) - 1;
            if (nextSeparatorIsNextToCurrent) {
                invalidIndex = separatorsIndex.get(i + 1);
                break;
            }
        }
        return invalidIndex;
    }
}
