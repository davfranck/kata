package jbrains;

public class Pgcd {

    private Integer value;
    private final int lower;
    private final int bigger;

    public Pgcd(int first, int second) {
        lower = Math.min(first, second);
        bigger = Math.max(first, second);
    }

    /**
     * @return pgcd
     */
    public int value() {
        // lazy init
        int newLower = lower;
        int newBigger = bigger;
        if (value == null) {
            while (newLower != 0) {
                int temp = newLower;
                newLower = newBigger % newLower;
                newBigger = temp;
            }
            this.value = Math.abs(newBigger);
        }
        return value;
    }
}
