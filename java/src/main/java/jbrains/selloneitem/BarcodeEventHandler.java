package jbrains.selloneitem;

import java.util.Optional;

public class BarcodeEventHandler {

    private final IDisplay display;
    private final IPriceFinder priceFinder;

    public BarcodeEventHandler(IDisplay display, IPriceFinder priceFinder) {
        this.display = display;
        this.priceFinder = priceFinder;
    }

    public void onBarcode(String barcode) {
        if (barcode == null || barcode.isEmpty()) {
            display.print("BAD INPUT");
        } else {
            processBarcodeWhenInputHasValidFormat(barcode);
        }
    }

    private void processBarcodeWhenInputHasValidFormat(String barcode) {
        Optional<Integer> optionalPrice = priceFinder.findPriceFor(barcode);
        optionalPrice.ifPresentOrElse(
                price -> display.print(String.format("%d$", price)),
                () -> display.print("UNKNOWN CODE")
        );
    }
}
