package jbrains.selloneitem;

import java.util.Optional;

public interface IPriceFinder {
    Optional<Integer> findPriceFor(String barcode);
}
