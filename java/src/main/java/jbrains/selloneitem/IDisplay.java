package jbrains.selloneitem;

public interface IDisplay {
    void print(String message);
}
