package jbrains;

import java.util.Objects;

// https://en.wikipedia.org/wiki/Fraction#Simplifying_(reducing)_fractions
public class Fraction {

    private final int numerator;
    private final int denominator;

    private Fraction(int numerator, int denominator) {
        Pgcd pgcd = new Pgcd(numerator, denominator);
        if (pgcd.value() != 1) {
            this.numerator = numerator / pgcd.value();
            this.denominator = denominator / pgcd.value();
        } else {
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }

    public static Fraction from(int numerator) {
        return new Fraction(numerator, 1);
    }

    public static Fraction from(int numerator, int denominator) {
        return new Fraction(numerator, denominator);
    }

    public Fraction add(Fraction other) {
        if (denominator == other.denominator) {
            return new Fraction(numerator + other.numerator, denominator);
        }
        int newDenominator = other.denominator * denominator;
        int myNumerator = numerator * other.denominator;
        int otherNumerator = other.numerator * denominator;
        return new Fraction(myNumerator + otherNumerator, newDenominator);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fraction fraction = (Fraction) o;
        return numerator == fraction.numerator && denominator == fraction.denominator;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numerator, denominator);
    }

    @Override
    public String toString() {
        return "Fraction{" +
                "numerator=" + numerator +
                ", denominator=" + denominator +
                '}';
    }
}
