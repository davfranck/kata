package christianhujer.expensereport;

import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringJoiner;

enum ExpenseType {
    DINNER("Dinner", true),
    BREAKFAST("Breakfast", true),
    CAR_RENTAL("Car Rental", false),
    LUNCH("Lunch", true);

    final String label;
    final boolean isMeal;

    ExpenseType(String label, boolean isMeal) {
        this.label = label;
        this.isMeal = isMeal;
    }

    public boolean isMeal() {
        return isMeal;
    }
}

class Expense {

    Expense(ExpenseType type, int amount) {
        this.type = type;
        this.amount = amount;
    }

    public final ExpenseType type;
    public final int amount;

    String expenseName() {
        return type.label;
    }

    boolean markExpense(Map<ExpenseType, Integer> markThresholdByType) {
        return amount > markThresholdByType.getOrDefault(type, Integer.MAX_VALUE);
    }

    int mealExpenses() {
        if (type.isMeal()) {
            return amount;
        }
        return 0;
    }
}

interface Report {
    void add(String line);
}


class StringReport implements Report {

    StringJoiner content = new StringJoiner(System.lineSeparator());

    @Override
    public void add(String line) {
        content.add(line);
    }
}

public class ExpenseReport {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("E LLL dd hh:mm:ss z yyyy", Locale.ENGLISH);
    private static final Map<ExpenseType, Integer> markThresholdByType = Map.of(
            ExpenseType.DINNER, 5000,
            ExpenseType.LUNCH, 2000,
            ExpenseType.BREAKFAST, 1000
    );

    private final Report report;
    private final Clock clock;

    public ExpenseReport(Report report, Clock clock) {
        this.report = report;
        this.clock = clock;
    }

    public void printReport(List<Expense> expenses) {
        int total = 0;
        int mealExpenses = 0;

        report.add("Expenses " + dateTimeFormatter.format(ZonedDateTime.now(clock)));

        for (Expense expense : expenses) {
            mealExpenses += expense.mealExpenses();

            boolean markExpense = expense.markExpense(markThresholdByType);

            String mealOverExpensesMarker = markExpense ? "X" : " ";

            report.add(expense.expenseName() + "\t" + expense.amount + "\t" + mealOverExpensesMarker);

            total += expense.amount;
        }

        report.add("Meal expenses: " + mealExpenses);
        report.add("Total expenses: " + total);
    }

}
// DONE Couverture de test (approval) => sortie à récupérer, date à fixer pour les TU
// DONE Smells : java.util.Date, System.out, le switch sur l'enum, l'expression booléenne
