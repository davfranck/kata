# ExpenseReport

Source : https://github.com/christianhujer/expensereport

The ExpenseReport legacy code refactoring kata in various languages.

This is an example of a piece of legacy code with lots of code smells. The goal is to support the following new feature
as best as you can:

    Add Lunch with an expense limit of 2000.

Process

    📚 Read the code to understand what it does and how it works.
    🦨 Read the code and check for design smells.
    🧑‍🔬 Analyze what you would have to change to implement the new requirement without refactoring the code.
    🧪 Write a characterization test. Take note of all design smells that you missed that made your life writing a test miserable.
    🔧 Refactor the code.
    🔧 Refactor the test.
    👼 Test-drive the new feature.
