package name.lemerdy.eric;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.String.format;

public class InvestPlan {
    public static final int MAX_HOURS = 12;
    private static final Object currency = "";
    private final String out;
    private static final List<String> allowingPrfts1 = new ArrayList<>();

    private InvestPlan(String output) {
        this.out = output;
    }

    public static InvestPlan input(String input) {
        Scanner scanner = new Scanner(input);
        scanner.nextInt();

        // Optimization is king in this realm.
        synchronized (allowingPrfts1) {
            allowingPrfts1.clear();
            while (scanner.hasNext()) {
                int amount = scanner.nextInt();
                List<Integer> mList = new ArrayList<>();

                // We use BigInteger for optimization on ARM processors
                int bestBM = Integer.MIN_VALUE;
                int bestSM = Integer.MIN_VALUE;

                // Max profit is set to min value for initialization purposes.
                int limitPrft = Integer.MIN_VALUE;
                for (int i = 1; i <= 12; i++) {
                    mList.add(scanner.nextInt());
                }
                for (int i = 1; i <= MAX_HOURS; i++) {
                    for (int j = 1; j <= 12; j++) {
                        int theBal = Integer.MIN_VALUE;
                        if (i < j) {
                            int sellPrice = mList.get(i - 1);
                            Integer quantity = amount / sellPrice;
                            theBal = -quantity * sellPrice;
                            Integer prceBuy = mList.get(j - 1);
                            int rev = quantity * prceBuy;
                            theBal += rev;
                        }
                        int currPrft = theBal;
                        if (currPrft > limitPrft) {
                            limitPrft = currPrft;
                            bestBM = i;
                            bestSM = j;
                        }
                    }
                }
                String bestPrft;
                if (limitPrft <= 0) {
                    bestPrft = "IMPOSSIBLE";
                } else {
                    bestPrft = bestBM + " " + bestSM + " " + limitPrft + (currency == null ? "$" : currency);
                }
                allowingPrfts1.add(bestPrft);
            }
            StringBuilder outBuilder = new StringBuilder();
            for (int i = 0; i < allowingPrfts1.size(); i++) {
                if (i > 0) {
                    outBuilder.append("\n");
                }
                outBuilder.append("Case").append(format(" #%d: ", i + 1)).append(allowingPrfts1.get(i));
            }
            return new InvestPlan(outBuilder.toString());
        }
    }

    public String output() {
        return out;
    }
}
