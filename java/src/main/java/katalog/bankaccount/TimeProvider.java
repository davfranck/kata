package katalog.bankaccount;

import java.time.LocalDate;

public interface TimeProvider {
    LocalDate now();
}
