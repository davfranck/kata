package katalog.bankaccount;

import java.time.LocalDate;

public class Transaction {
    private final int amount;
    private final LocalDate transactionDate;

    public Transaction(int amount, LocalDate transactionDate) {
        this.amount = amount;
        this.transactionDate = transactionDate;
    }

    public LocalDate getDate() {
        return transactionDate;
    }

    public int getAmount() {
        return amount;
    }
}
