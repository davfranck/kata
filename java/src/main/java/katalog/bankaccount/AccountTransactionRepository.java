package katalog.bankaccount;

import java.time.LocalDate;
import java.util.List;

public interface AccountTransactionRepository {
    void add(int amount, LocalDate transactionDate);

    List<Transaction> all();
}
