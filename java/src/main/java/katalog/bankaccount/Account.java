package katalog.bankaccount;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Account {

    private final AccountTransactionRepository transactionRepository;
    private final TimeProvider timeProvider;

    public Account(AccountTransactionRepository transactionRepository, TimeProvider timeProvider) {
        this.transactionRepository = transactionRepository;
        this.timeProvider = timeProvider;
    }

    public void deposit(int amount) {
        transactionRepository.add(amount, timeProvider.now());
    }

    public void withdraw(int amount) {
        transactionRepository.add(-amount, timeProvider.now());
    }

    public String printStatement() {
        String header = "Date        Amount  Balance";
        List<Transaction> transactions = transactionRepository.all();
        int balance = 0;
        List<String> statement = new ArrayList<>();
        statement.add(header);
        for (Transaction transaction : transactions) {
            // FIXME : formatter à injecter de préférence ou à mettre en static
            String transactionDate = DateTimeFormatter.ofPattern("dd.MM.yyyy").format(transaction.getDate());
            String amount;
            if (transaction.getAmount() > 0) {
                amount = "+" + transaction.getAmount();
            } else {
                amount = "" + transaction.getAmount();
            }
            balance += transaction.getAmount();
            statement.add(transactionDate + " " + amount + " " + balance);
        }
        return String.join(System.lineSeparator(), statement);
    }

}
