package katalog.tictactoe;

import java.util.List;
import java.util.Optional;

public class GameController {

    private final Board board;
    private final Display display;
    private final List<Player> players;
    private final Moves moves = new Moves();

    private Player currentPlayer;

    private boolean gameOver = false;

    public GameController(Board board, Display display, List<Player> players) {
        this.board = board;
        this.display = display;
        this.players = players;
        currentPlayer = players.get(0);
    }

    public void play(Move move) {
        isValid(move).ifPresentOrElse(
                display::displayErrorMessage,
                () -> doMove(move));

    }


    private void doMove(Move move) {
        board.takeField(currentPlayer, move);
        moves.add(move);
        display.render(board, players.get(0));
        if (board.hasAWinner()) {
            display.displayWinner(currentPlayer.name);
            gameOver = true;
        } else if (board.isFull()) {
            display.displayDraw();
            gameOver = true;
        } else {
            nextPlayer();
            display.displayPlayerTurn(currentPlayer.name);
        }
    }

    private Optional<String> isValid(Move move) {
        if (gameOver) {
            return Optional.of("Game is over");
        }
        if (board.isTaken(move)) {
            return Optional.of("field already occupied");
        }
        return Optional.empty();

    }

    private void nextPlayer() {
        if (currentPlayer.equals(players.get(0))) {
            currentPlayer = players.get(1);
        } else {
            currentPlayer = players.get(0);
        }
    }


    public boolean isRunning() {
        return !gameOver;
    }

    public void start() {
        display.render(board, currentPlayer);
        display.displayPlayerTurn(currentPlayer.name);
    }
}
