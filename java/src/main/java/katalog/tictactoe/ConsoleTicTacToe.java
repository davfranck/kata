package katalog.tictactoe;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class ConsoleTicTacToe {

    private Display display;
    private GameController gameController;
    private KeyboardInput input;

    public ConsoleTicTacToe(Display display,
                            GameController gameController,
                            KeyboardInput input) {
        this.display = display;
        this.gameController = gameController;
        this.input = input;
    }

    public static void main(String[] args) {
        Board board = new BoardImpl();
        Display display = new ConsoleDisplay(new ConsoleOuputWriter());
        List<Player> players = List.of(new Player("Elias"), new Player("David"));
        GameController gameController = new GameController(board, display, players);

        Scanner scanner = new Scanner(System.in);
        KeyboardInput input = new KeyboardInput(scanner);

        try {
            new ConsoleTicTacToe(display, gameController, input).playGame();
        } finally {
            scanner.close();
        }

    }

    public void playGame() {
        gameController.start();
        while (gameController.isRunning()) {
            Optional<String> inputMove = input.tryMove();
            if (inputMove.isPresent()) {
                Move.of(inputMove.get()).ifPresentOrElse(
                        gameController::play,
                        display::displayInvalidCoordinates);
            } else {
                return;// we leave the game if the input is emtpy
            }
        }
    }

}
