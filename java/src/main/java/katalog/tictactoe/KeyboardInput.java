package katalog.tictactoe;

import java.util.Optional;
import java.util.Scanner;

public class KeyboardInput {

    private final Scanner scanner;

    public KeyboardInput(Scanner scanner) {
        this.scanner = scanner;
    }

    public Optional<String> tryMove() {
        try {
            String value = scanner.nextLine();
            if ("".equals(value)) {
                return Optional.empty();
            }
            return Optional.of(value);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
