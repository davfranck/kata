package katalog.tictactoe;

public interface OutputWriter {

    void println(String message);
    
}
