package katalog.tictactoe;

import java.util.List;
import java.util.Optional;

public class ConsoleDisplay implements Display {

    private final OutputWriter outputWriter;

    public ConsoleDisplay(OutputWriter outputWriter) {
        this.outputWriter = outputWriter;
    }

    @Override
    public void render(Board board, Player firstPlayer) {
        List<Row> rows = board.rows();
        outputWriter.println("    O 1 2");
        outputWriter.println("    v v v");
        rows.stream().map(row ->
                        row.index + " > "
                                + renderColumn(row, 0, firstPlayer) + " "
                                + renderColumn(row, 1, firstPlayer) + " "
                                + renderColumn(row, 2, firstPlayer))
                .forEach(outputWriter::println);
        outputWriter.println("");
    }

    private String renderColumn(Row row, int columnIndex, Player firstPlayer) {
        Optional<Player> playerAt = row.playerAt(columnIndex);
        return playerAt.map(player -> firstPlayer.equals(player) ? "O" : "X").orElse("-");
    }

    @Override
    public void displayErrorMessage(String errorMessage) {
        outputWriter.println(errorMessage);
    }

    @Override
    public void displayDraw() {
        outputWriter.println("Draw");
    }

    @Override
    public void displayWinner(String player) {
        outputWriter.println(player + " wins the game !");
    }

    @Override
    public void displayPlayerTurn(String player) {
        outputWriter.println("it's now " + player + "'s turn");
    }

    @Override
    public void displayInvalidCoordinates() {
        outputWriter.println("invalid coordinates");
    }
}
