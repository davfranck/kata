package katalog.tictactoe;

import java.util.List;

interface Board {

    void takeField(Player player, Move move);

    boolean isTaken(Move move);

    boolean hasAWinner();

    boolean isFull();

    List<Row> rows();
}
