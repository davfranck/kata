package katalog.tictactoe;

class Field {

    public final int row;
    public final int column;

    public Field(int row, int column) {
        this.row = row;
        this.column = column;
    }
}
