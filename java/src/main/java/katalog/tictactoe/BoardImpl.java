package katalog.tictactoe;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    private Player[][] content = new Player[3][3];
    private boolean hasAWinner = false;

    @Override
    public void takeField(Player player, Move move) {
        content[move.row][move.column] = player;
        checkWinner(player, move);
    }

    private void checkWinner(Player player, Move move) {
        if (playerWinInARow(player, move)
                || playerWinInAColumn(player, move)
                || playerWinInLeftToRightDiagonal(player)
                || playerWinInRightToLeftDiagonal(player)) {
            hasAWinner = true;
        }
    }

    private boolean playerWinInARow(Player player, Move move) {
        for (int i = 0; i < 3; i++) {
            if (player != content[i][move.column]) {
                return false;
            }
        }
        return true;
    }

    private boolean playerWinInAColumn(Player player, Move move) {
        for (int i = 0; i < 3; i++) {
            if (player != content[move.row][i]) {
                return false;
            }
        }
        return true;
    }

    private boolean playerWinInLeftToRightDiagonal(Player player) {
        return player.equals(content[0][0]) && player.equals(content[1][1]) && player.equals(content[2][2]);
    }

    private boolean playerWinInRightToLeftDiagonal(Player player) {
        return player.equals(content[0][2]) && player.equals(content[1][1]) && player.equals(content[2][0]);
    }

    @Override
    public boolean isTaken(Move move) {
        if (content[move.row][move.column] != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasAWinner() {
        return hasAWinner;
    }

    @Override
    public boolean isFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (content[i][j] == null) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public List<Row> rows() {
        List<Row> rows = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            rows.add(new Row(i, content[i][0], content[i][1], content[i][2]));
        }
        return rows;
    }
}
