package katalog.tictactoe;

import java.util.List;
import java.util.Optional;

public class Row {

    final List<Optional<Player>> players;
    final int index;

    public Row(int index, Player firstColumnPlayer, Player secondColumnPlayer, Player thirdColumnPlayer) {
        this.index = index;
        players = List.of(
                Optional.ofNullable(firstColumnPlayer),
                Optional.ofNullable(secondColumnPlayer),
                Optional.ofNullable(thirdColumnPlayer)
        );
    }


    public Optional<Player> playerAt(int columnIndex) {
        return players.get(columnIndex);
    }
}
