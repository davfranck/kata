package katalog.tictactoe;

interface Display {

    void render(Board board, Player firstPlayer);

    void displayErrorMessage(String errorMessage);

    void displayDraw();

    void displayWinner(String player);

    void displayPlayerTurn(String player);

    void displayInvalidCoordinates();
}
