package katalog.tictactoe;

import java.util.Optional;

public class Move {

    public final int row;
    public final int column;

    public Move(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public static Optional<Move> of(String move) {
        String[] split = move.split("-");
        try {
            int row = Integer.parseInt(split[0]);
            int column = Integer.parseInt(split[1]);
            if (row >= 0 && row < 3 && column >= 0 && column < 3) {
                return Optional.of(new Move(row, column));
            }
            return Optional.empty();
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }
}
