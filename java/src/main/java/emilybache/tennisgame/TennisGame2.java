package emilybache.tennisgame;

import java.util.List;
import java.util.Objects;

public class TennisGame2 implements TennisGame {
    private static final List<String> SCORES = List.of("Love", "Fifteen", "Thirty", "Forty");

    private final String player1Name;
    private final String player2Name;

    private int p1point;
    private int p2point = 0;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore() {
        String score;

        if (p1point == p2point) {
            score = p1point >= 3 ? "Deuce" : SCORES.get(p1point) + "-All";
        } else if (p1point < 4 && p2point < 4) {
            score = SCORES.get(p1point) + "-" + SCORES.get(p2point);
        } else {
            score = Math.abs(p1point - p2point) >= 2 ? "Win for " : "Advantage ";
            score += p1point > p2point ? player1Name : player2Name;
        }
        return score;
    }

    public void p1Score() {
        p1point++;
    }

    public void p2Score() {
        p2point++;
    }

    public void wonPoint(String player) {
        if (Objects.equals(player, player1Name))
            p1Score();
        else
            p2Score();
    }
}