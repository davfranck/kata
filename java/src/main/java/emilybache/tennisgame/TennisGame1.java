package emilybache.tennisgame;

import java.util.List;
import java.util.Map;

public class TennisGame1 implements TennisGame {

    private static final List<String> ZERO_TO_THREE_SCORE = List.of("Love", "Fifteen", "Thirty", "Forty");
    private static final Map<Integer, String> EQUALITY_SCORE = Map.of(0, "Love-All", 1, "Fifteen-All", 2, "Thirty-All");

    private final String player1Name;
    private final String player2Name;

    private int player1Score = 0;
    private int player2Score = 0;

    public TennisGame1(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    @Override
    public void wonPoint(String playerName) {
        if (playerName.equals(player1Name))
            player1Score += 1;
        else
            player2Score += 1;
    }

    @Override
    public String getScore() {
        String score;
        if (player1Score == player2Score) {
            score = equality();
        } else if (player1Score >= 4 || player2Score >= 4) {
            score = onePlayerAtLeastHasMoreThanFourPointsWithoutEquality();
        } else {
            score = noEqualityNoAdvantageNoWin();
        }
        return score;
    }

    private String equality() {
        return EQUALITY_SCORE.getOrDefault(player1Score, "Deuce");
    }

    private String onePlayerAtLeastHasMoreThanFourPointsWithoutEquality() {
        String player = player1Score > player2Score ? player1Name : player2Name;
        if (Math.abs(player1Score - player2Score) >= 2) {
            return "Win for " + player;
        } else {
            return "Advantage " + player;
        }
    }

    private String noEqualityNoAdvantageNoWin() {
        return ZERO_TO_THREE_SCORE.get(player1Score) + "-" + ZERO_TO_THREE_SCORE.get(player2Score);
    }

}
