package emilybache.gildedrose;

// https://github.com/emilybache/GildedRose-Refactoring-Kata
class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            updateOneItem(item);
        }
    }

    private void updateOneItem(Item item) {
        // Guard clause - move to caller ?
        if (item.name.equals("Sulfuras, Hand of Ragnaros")) {
            return;
        }
        if (item.name.equals("Aged Brie")) {
            updateAgedBrie(item);
        } else if (item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {
            updateBackstagePasses(item);
        } else if (item.name.equals("Conjured")) {
            updateConjured(item);
        } else {
            updateDefault(item);
        }


    }

    private void updateBackstagePasses(Item item) {
        if (canIncreaseQuality(item)) {
            item.quality = item.quality + 1;

            if (item.sellIn < 11 && canIncreaseQuality(item)) {
                item.quality = item.quality + 1;
            }

            if (item.sellIn < 6 && canIncreaseQuality(item)) {
                item.quality = item.quality + 1;
            }
        }
        item.sellIn = item.sellIn - 1;
        if (item.sellIn < 0) {
            item.quality = 0;
        }
    }

    private void updateAgedBrie(Item item) {
        if (canIncreaseQuality(item)) {
            item.quality = item.quality + 1;
        }
        item.sellIn = item.sellIn - 1;
        if (item.sellIn < 0 && canIncreaseQuality(item)) {
            item.quality = item.quality + 1;
        }
    }

    private void updateConjured(Item item) {
        if (item.quality > 0) {
            item.quality = item.quality - 2;
        }
        item.sellIn = item.sellIn - 1;
        if (item.sellIn < 0 && item.quality > 0) {
            item.quality = item.quality - 2;
        }
    }

    private void updateDefault(Item item) {
        if (item.quality > 0) {
            item.quality = item.quality - 1;
        }
        item.sellIn = item.sellIn - 1;
        if (item.sellIn < 0 && item.quality > 0) {
            item.quality = item.quality - 1;
        }
    }

    private boolean canIncreaseQuality(Item item) {
        return item.quality < 50;
    }
}