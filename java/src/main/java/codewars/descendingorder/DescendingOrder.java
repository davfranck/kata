package codewars.descendingorder;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.reverse;
import static java.util.Collections.sort;

public class DescendingOrder {

    public static int sortDesc(int num) {
        String[] numAsStringArray = String.valueOf(num).split("");
        List<String> numAsStrings = Arrays.asList(numAsStringArray);
        sort(numAsStrings);
        reverse(numAsStrings);
        String joined = numAsStrings.stream().collect(Collectors.joining());
        return Integer.valueOf(joined);
    }

    // copié depuis les autres solutions
    public static int betterSolution(final int num) {
        return Integer.parseInt(String.valueOf(num)
                .chars()
                .mapToObj(i -> String.valueOf(Character.getNumericValue(i)))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining()));
    }
}
