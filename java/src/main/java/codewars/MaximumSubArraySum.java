package codewars;

// https://www.codewars.com/kata/54521e9ec8e60bc4de000d6c/train/java
public class MaximumSubArraySum {

    // Après lecture d'autres solutions, l'usage de Math.min / Math.max aurait été plus élégant.
    public static int sequence(int[] input) {
        int best = 0;
        int sum = 0;
        for (int i : input) {
            if (sum + i > 0) {
                sum += i;
            } else {
                sum = 0;
            }
            if (sum > best) {
                best = sum;
            }
        }
        return best;
    }
}
