package codewars.convertnumbertoreversedarray;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Digitize {
    public static int[] digitize(long num) {
        String[] numAsStringArray = String.valueOf(num).split("");
        List<String> numAsStrings = Arrays.asList(numAsStringArray);
        Collections.reverse(numAsStrings);
        return numAsStrings.stream()
                .mapToInt(Integer::valueOf)
                .toArray();
    }

    // solution intéressante
    public static int[] cleverSolution(long n) {
        return new StringBuilder().append(n)
                .reverse()
                .chars()
                .map(Character::getNumericValue)
                .toArray();
    }
}
