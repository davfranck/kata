package stackexchange.fitnessclub;

import java.util.*;

public class InputReader {

    private int age = 0;
    private int months = 0;
    private int sessions = 0;

    private final Scanner scanner;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Order order = new InputReader(scanner).processMember();
        String membershipAmount = new Pricer(order).membershipAmount();
        System.out.println("Your total price in MYR is RM" + membershipAmount);
        scanner.close();
    }

    public InputReader(Scanner scanner) {
        this.scanner = scanner;
    }

    private Order processMember() {
        informationMessage();
        welcome();
        readAge();
        readMonths();
        readSessions();
        return new Order(age, months, sessions);
    }

    private void informationMessage() {
        outWithNewLine("");
        outWithNewLine("");
        outWithNewLine("Welcome to Fitness Centre!");
        outSeparator();
        outWithNewLine("If you are a senior citizen, then you are eligible for a");
        outWithNewLine("discount of 30% of the regular membership price");
        outSeparator();
        outWithNewLine("If you buy membership for twelve months and pay today, the discount is 15%");
        outSeparator();
        outWithNewLine("If you purchased more than 5 personal training sessions");
        outWithNewLine(",the discount of each session is 20%");
        outSeparator();
    }

    private void welcome() {
        outWithNewLine("Please fill in your information");
        out("Please enter your name: ");
        String name = scanner.nextLine();
        outWithNewLine("Welcome " + name + " to our Fitness Centre!");
    }

    private void readAge() {
        outWithNewLine("Please enter your age: ");
        age = scanner.nextInt();
        scanner.nextLine();
        if (age >= 60) {
            outWithNewLine("Congratulations! You are entitled to a 30% discount.");
        }
    }

    private void readMonths() {
        while (months <= 0) {
            outWithNewLine("How many months do you want to pay for?");
            outWithNewLine("The original price per month is RM140");
            months = scanner.nextInt();
            scanner.nextLine();
        }
    }

    private void readSessions() {
        while (sessions <= 0 || sessions > 10) {
            outWithNewLine("How many training sessions do you want to purchase (0-10)");
            outWithNewLine("The original price per sessions is RM85");
            sessions = scanner.nextInt();
            scanner.nextLine();
        }
    }

    private void outWithNewLine(String content) {
        System.out.println(content);
    }

    private void out(String content) {
        System.out.print(content);
    }

    private void outSeparator() {
        outWithNewLine("--------------------------------------------");
    }

}
