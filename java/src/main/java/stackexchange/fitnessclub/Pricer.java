package stackexchange.fitnessclub;

import java.math.*;

class Pricer {
    public static final BigDecimal ONE_SESSION_PRICE = BigDecimal.valueOf(85L);
    public static final BigDecimal MEMBERSHIP_FOR_ONE_MONTH = BigDecimal.valueOf(140L);
    protected static final BigDecimal SESSION_DISCOUNT = BigDecimal.valueOf(0.15d);
    protected static final BigDecimal SENIOR_DISCOUNT = BigDecimal.valueOf(0.3);
    private final int age;
    private final BigDecimal nbMonthsToPay;
    private final BigDecimal nbSession;

    public Pricer(int age, int nbMonthsToPay, int nbSession) {
        this.age = age;
        this.nbMonthsToPay = BigDecimal.valueOf(nbMonthsToPay);
        this.nbSession = BigDecimal.valueOf(nbSession);
    }

    public Pricer(Order order) {
        this.age = order.age;
        this.nbMonthsToPay = new BigDecimal(order.months);
        this.nbSession = new BigDecimal(order.sessions);
    }

    public String membershipAmount() {
        BigDecimal membershipAmount = computeMembershipPrice();
        BigDecimal sessionAmount = computeSessionPrice();
        return membershipAmount.add(sessionAmount).toString();
    }

    private BigDecimal computeSessionPrice() {
        BigDecimal sessionAmount = ONE_SESSION_PRICE.multiply(nbSession);
        if (nbSession.intValue() >= 6) {
            return sessionAmount.subtract(sessionAmount.multiply(SESSION_DISCOUNT));
        }
        return sessionAmount;
    }

    private BigDecimal computeMembershipPrice() {
        BigDecimal membership = MEMBERSHIP_FOR_ONE_MONTH.multiply(nbMonthsToPay);
        if (age >= 60) {
            return membership.subtract(membership.multiply(SENIOR_DISCOUNT));
        }
        return membership;
    }
}
