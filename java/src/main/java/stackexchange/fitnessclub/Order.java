package stackexchange.fitnessclub;

public class Order {
    public final int age;
    public final int months;
    public final int sessions;

    public Order(int age, int months, int sessions) {
        this.age = age;
        this.months = months;
        this.sessions = sessions;
    }
}
