package codingame.achildsplay;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

public class Solution2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int w = in.nextInt();
        int h = in.nextInt();
        long n = in.nextLong();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        List<Obstacle> obstacles = new ArrayList<>();
        int startX = 0;
        int startY = 0;
        for (int i = 0; i < h; i++) {
            String line = in.nextLine();
            String[] row = line.split("");
            for (int j = 0; j < w; j++) {
                if (row[j].equals("#")) {
                    obstacles.add(Obstacle.fromCoordinate(j, i));
                } else if (row[j].equals("O")) {
                    startX = j;
                    startY = i;
                }
            }
        }
        Robot robot = Robot.atStartPositionWithObstacles(startX, startY, obstacles);
        robot.move(n, MapDimensions.fromWidthAndHeight(w, h));
        robot.logPosition();
    }

    public static class Robot implements Locatable {
        private final Position currentPosition;
        private Direction direction;
        private final List<Obstacle> obstacles;
        private final List<String> visitedPositions = new ArrayList<>();

        private Robot(int x, int y, List<Obstacle> obstacles) {
            this.currentPosition = new Position(x, y);
            this.obstacles = obstacles;
            this.direction = Direction.UP;
        }

        public static Robot atStartPosition(int x, int y) {
            return new Robot(x, y, List.of());
        }

        public static Robot atStartPositionWithObstacles(int x, int y, List<Obstacle> obstacles) {
            return new Robot(x, y, obstacles);
        }

        public void move(long times, MapDimensions mapDimensions) {
            visitedPositions.add(serializeStatus(currentPosition, direction));
            while (times != 0) {
                Position targetPosition = targetPosition();
                long alreadyVisitedIndex = visitedPositions.indexOf(serializeStatus(targetPosition, this.direction));
                if (alreadyVisitedIndex != -1) {
                    long loopSize = visitedPositions.size() - alreadyVisitedIndex;
                    times = times % loopSize;
                }
                if (times != 0
                        && !updateDirectionIfBoundaryReached(mapDimensions, targetPosition)
                        && !updateDirectionIfObstacle(targetPosition)) {
                    updatePosition(currentPosition);
                    visitedPositions.add(serializeStatus(currentPosition, direction));
                    times--;
                }
            }
        }

        private Position targetPosition() {
            Position targetPosition = new Position(currentPosition.x, currentPosition.y);
            updatePosition(targetPosition);
            return targetPosition;
        }

        private String serializeStatus(Position currentPosition, Direction direction) {
            return currentPosition.x + "-" + currentPosition.y + "-" + direction.name();
        }

        private boolean updateDirectionIfObstacle(Position targetPosition) {
            Optional<Obstacle> obstacle = obstacles.stream()
                    .filter(o -> o.isAt(targetPosition.x, targetPosition.y))
                    .findFirst();
            if (obstacle.isPresent()) {
                nextDirection();
                return true;
            }
            return false;
        }

        private boolean updateDirectionIfBoundaryReached(MapDimensions mapDimensions, Position targetPosition) {
            if (mapDimensions.isOutOfBoundaries(targetPosition)) {
                nextDirection();
                return true;
            }
            return false;
        }

        private void nextDirection() {
            if (this.direction.equals(Direction.UP)) {
                this.direction = Direction.RIGHT;
            } else if (this.direction == Direction.RIGHT) {
                this.direction = Direction.DOWN;
            } else if (this.direction == Direction.DOWN) {
                this.direction = Direction.LEFT;
            } else if (this.direction == Direction.LEFT) {
                this.direction = Direction.UP;
            }
        }

        private void updatePosition(Position position) {
            switch (this.direction) {
                case UP:
                    position.y--;
                    break;
                case RIGHT:
                    position.x++;
                    break;
                case DOWN:
                    position.y++;
                    break;
                case LEFT:
                    position.x--;
                    break;
                default:
                    throw new IllegalStateException("direction non implémentée");
            }
        }

        public boolean isAt(int x, int y) {
            return this.currentPosition.x == x && this.currentPosition.y == y;
        }

        @Override
        public String toString() {
            return "Robot{" +
                    "direction=" + direction +
                    ", x=" + currentPosition.x +
                    ", y=" + currentPosition.y +
                    '}';
        }

        public void logPosition() {
            System.out.printf("%d %d", currentPosition.x, currentPosition.y);
        }

        enum Direction {
            UP, RIGHT, DOWN, LEFT
        }

        private static class Position implements Locatable {
            int x;
            int y;

            public Position(int x, int y) {
                this.x = x;
                this.y = y;
            }

            @Override
            public boolean isAt(int x, int y) {
                return this.x == x && this.y == y;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Position position = (Position) o;
                return x == position.x && y == position.y;
            }

            @Override
            public int hashCode() {
                return Objects.hash(x, y);
            }
        }
    }

    public static class MapDimensions {
        private final int width;
        private final int height;

        public MapDimensions(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public static MapDimensions fromWidthAndHeight(int width, int height) {
            return new MapDimensions(width, height);
        }

        public boolean isXOutOfBoundaries(int x) {
            return x > width - 1;
        }

        public boolean isYOutOfBoundaries(int y) {
            return y > height - 1;
        }

        public boolean isOutOfBoundaries(Robot.Position targetPosition) {
            return targetPosition.x < 0
                    || targetPosition.y < 0
                    || isXOutOfBoundaries(targetPosition.x)
                    || isYOutOfBoundaries(targetPosition.y);
        }
    }

    public static class Obstacle implements Locatable {
        private final int x;
        private final int y;

        public Obstacle(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public static Obstacle fromCoordinate(int x, int y) {
            return new Obstacle(x, y);
        }

        @Override
        public boolean isAt(int x, int y) {
            return this.x == x && this.y == y;
        }
    }

    private interface Locatable {
        boolean isAt(int x, int y);
    }
}
