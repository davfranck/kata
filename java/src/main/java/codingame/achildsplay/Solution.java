package codingame.achildsplay;

import java.io.InputStream;
import java.util.Scanner;

// https://www.codingame.com/ide/puzzle/a-childs-play
public class Solution {

    public static void main(String args[]) {
        execute(System.in);
    }

    public static void execute(InputStream inputStream) {
        Scanner in = new Scanner(inputStream);
        int w = in.nextInt();
        int h = in.nextInt();
        long n = in.nextLong();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        String mapAsString = "";
        for (int i = 0; i < h; i++) {
            mapAsString += in.nextLine();
            if (i != n - 1) {
                mapAsString += System.lineSeparator();
            }
        }
        Map map = new Map(mapAsString);
        Robot robot = new Robot(map);
        robot.move(n);
        System.out.println(robot.position);
    }

    public static class Map {

        protected static final String OBSTACLE = "#";
        protected static final String ROBOT = "O";
        private final String[][] rows;

        public Map(String mapAsTring) {
            String[] rowsAsString = mapAsTring.split(System.lineSeparator());
            int numberOfRows = rowsAsString.length;
            int numberOfColumns = rowsAsString[0].length();
            this.rows = new String[numberOfRows][numberOfColumns];
            for (int i = 0; i < numberOfRows; i++) {
                this.rows[i] = rowsAsString[i].split("");
            }
        }

        private Position robotStartPosition() {
            for (int y = 0; y < rows.length; y++) {
                String[] row = rows[y];
                for (int x = 0; x < row.length; x++) {
                    if (row[x].equals(ROBOT)) {
                        return new Position(x, y);
                    }
                }
            }
            throw new IllegalStateException("no robot on map");
        }

        public boolean obstacle(Position position) {
            return this.rows[position.y()][position.x()].equals(OBSTACLE);
        }
    }

    public static class Robot {
        private Position position;
        private Direction direction = Direction.NORTH;
        private Map map;

        public Robot(Map map) {
            this.map = map;
            this.position = map.robotStartPosition();
        }

        public void move(long times) {
            for (long i = 0; i < times; i++) {
                this.direction.update(this.position);
                if (this.map.obstacle(this.position)) {
                    this.direction = this.direction.next();
                }
                this.direction.update(this.position);
            }
        }

        public Position position() {
            return position;
        }

    }

    private enum Direction {
        NORTH {
            @Override
            public Direction next() {
                return EAST;
            }

            @Override
            public void update(Position position) {
                position.y = position.y - 1;
            }
        }, EAST {
            @Override
            public Direction next() {
                return SOUTH;
            }

            @Override
            public void update(Position position) {
                position.x = position.x + 1;
            }
        }, SOUTH {
            @Override
            public Direction next() {
                return WEST;
            }

            @Override
            public void update(Position position) {
                position.y = position.y + 1;
            }
        }, WEST {
            @Override
            public Direction next() {
                return NORTH;
            }

            @Override
            public void update(Position position) {
                position.x = position.x - 1;
            }
        };

        public abstract Direction next();

        public abstract void update(Position position);
    }

    public static class Position {

        private int x;
        private int y;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int x() {
            return this.x;
        }

        public int y() {
            return this.y;
        }

        @Override
        public String toString() {
            return x + " " + y;
        }
    }

}
