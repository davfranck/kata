package codingame;

class CustomGameOfLife {
    private final int[] survivingCondition;
    private final int[] birthCondition;
    private String[][] currentState;

    public CustomGameOfLife(int[] survivingCondition, int[] birthCondition, String[][] startSate) {
        this.survivingCondition = survivingCondition;
        this.birthCondition = birthCondition;
        this.currentState = startSate;
    }

    public String[][] play(int nbTurn) {
        String[][] newState = new String[currentState.length][currentState[0].length];
        for (int currentTurn = 0; currentTurn < nbTurn; currentTurn++) {
            updateNewState(newState);
            currentState = newState;
            newState = new String[currentState.length][currentState[0].length];
        }
        return currentState;
    }

    private void updateNewState(String[][] newState) {
        for (int rowIndex = 0; rowIndex < currentState.length; rowIndex++) {
            for (int colIndex = 0; colIndex < currentState[rowIndex].length; colIndex++) {
                boolean isDeadCell = currentState[rowIndex][colIndex].equals(".");
                if (isDeadCell) {
                    newState[rowIndex][colIndex] = updateBirthState(rowIndex, colIndex);
                } else {
                    newState[rowIndex][colIndex] = updateSurvivingState(rowIndex, colIndex);
                }

            }
        }
    }

    private String updateBirthState(int rowIndex, int colIndex) {
        int livingNeigbhours = countLivingNeigbhours(rowIndex, colIndex);
        if (birthCondition[livingNeigbhours] == 1) {
            return "O";
        }
        return ".";
    }

    private String updateSurvivingState(int rowIndex, int colIndex) {
        int nbNeighbours = countLivingNeigbhours(rowIndex, colIndex);
        if (survivingCondition[nbNeighbours] == 1) {
            return "O";
        }
        return ".";

    }

    private int countLivingNeigbhours(int rowIndex, int colIndex) {
        int livingNeighbours = 0;
        for (int row = rowIndex - 1; row < rowIndex + 2; row++) {
            for (int col = colIndex - 1; col < colIndex + 2; col++) {
                livingNeighbours += livingNeighbours(rowIndex, colIndex, row, col);
            }
        }
        return livingNeighbours;
    }

    private int livingNeighbours(int rowIndex, int colIndex, int row, int col) {
        boolean inGridBoundaries = row >= 0 && row < currentState.length && col >= 0 && col < currentState[0].length;
        boolean currentCell = row == rowIndex && col == colIndex;
        if (inGridBoundaries && !currentCell && currentState[row][col].equals("O")) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < currentState.length; i++) {
            for (int j = 0; j < currentState[i].length; j++) {
                out.append(currentState[i][j]);
            }
            if (i != currentState.length - 1) {
                out.append(System.lineSeparator());
            }
        }
        return out.toString();
    }
}
