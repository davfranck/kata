package codingame.asteroids;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class AsteroidPositionsAtTime {
    private final Map<String, Position> asteroidPositionsAtT1ByName;
    private final int time;

    public AsteroidPositionsAtTime(Map<String, Position> asteroidPositionsAtT1ByName, int time) {
        this.asteroidPositionsAtT1ByName = asteroidPositionsAtT1ByName;
        this.time = time;
    }

    public List<Asteroid> asteroids(AsteroidPositionsAtTime asteroidPositionsAtT2) {
        return asteroidPositionsAtT1ByName.entrySet().stream()
                .map(positionAtT1ByName -> {
                    String name = positionAtT1ByName.getKey();
                    Position positionAtT2 = asteroidPositionsAtT2.asteroidPositionsAtT1ByName.get(name);
                    Position positionAtT1 = positionAtT1ByName.getValue();
                    SpeedPerRound speed = positionAtT1.speed(positionAtT2, asteroidPositionsAtT2.time - this.time);
                    return new Asteroid(name, positionAtT2, speed);
                }).collect(toList());
    }

    public int time() {
        return time;
    }
}
