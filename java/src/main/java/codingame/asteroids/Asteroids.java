package codingame.asteroids;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class Asteroids {

    final List<Asteroid> asteroids;
    final int currentRound;

    public static Asteroids from(AsteroidPositionsAtTime asteroidsAtT1, AsteroidPositionsAtTime asteroidsAtT2) {
        List<Asteroid> asteroids = asteroidsAtT1.asteroids(asteroidsAtT2);
        return new Asteroids(asteroids, asteroidsAtT2.time());
    }

    public Asteroids(List<Asteroid> asteroids, int currentRound) {
        this.asteroids = asteroids.stream().sorted().collect(Collectors.toList());
        this.currentRound = currentRound;
    }

    public Asteroids moveTo(int t3) {
        List<Asteroid> asteroids = this.asteroids.stream().map(asteroid -> asteroid.move(t3 - currentRound)).collect(Collectors.toList());
        return new Asteroids(asteroids, t3);
    }

    public Optional<Asteroid> at(int i, int j) {
        return asteroids.stream().filter(asteroid -> asteroid.isAt(i, j)).findFirst();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Asteroids asteroids1 = (Asteroids) o;
        return currentRound == asteroids1.currentRound && Objects.equals(asteroids, asteroids1.asteroids);
    }

    @Override
    public int hashCode() {
        return Objects.hash(asteroids, currentRound);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Asteroids.class.getSimpleName() + "[", "]")
                .add("asteroids=" + asteroids)
                .add("time=" + currentRound)
                .toString();
    }
}
