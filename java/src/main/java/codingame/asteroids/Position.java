package codingame.asteroids;

import java.util.Objects;
import java.util.StringJoiner;

class Position {

    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public SpeedPerRound speed(Position positionAtT2, int rounds) {
        double xSpeed = (double) (positionAtT2.x - x) / rounds;
        double ySpeed = (double) (positionAtT2.y - y) / rounds;
        return new SpeedPerRound(xSpeed, ySpeed);
    }

    public Position update(int rounds, SpeedPerRound speed) {
        double xDistance = speed.xDistanceForRounds(rounds);
        double yDistance = speed.yDistanceForRounds(rounds);
        int newX = (int) Math.floor(this.x + xDistance);
        int newY = (int) Math.floor(this.y + yDistance);
        return new Position(newX, newY);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Position.class.getSimpleName() + "[", "]")
                .add("x=" + x)
                .add("y=" + y)
                .toString();
    }
}
