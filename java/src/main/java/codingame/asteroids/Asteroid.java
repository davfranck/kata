package codingame.asteroids;

import java.util.Objects;
import java.util.StringJoiner;

public class Asteroid implements Comparable<Asteroid> {
    private final String name;
    private final SpeedPerRound speed;
    private Position position;

    public Asteroid(String name, Position position, SpeedPerRound speed) {
        this.name = name;
        this.position = position;
        this.speed = speed;
    }

    public Asteroid move(int rounds) {
        Position nextPosition = position.update(rounds, speed);
        return new Asteroid(name, nextPosition, speed);
    }

    public String name() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Asteroid asteroid = (Asteroid) o;
        return name.equals(asteroid.name) && speed.equals(asteroid.speed) && position.equals(asteroid.position);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Asteroid.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("speed=" + speed)
                .add("position=" + position)
                .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, speed, position);
    }

    @Override
    public int compareTo(Asteroid o) {
        return name.compareTo(o.name);
    }

    public boolean isAt(int i, int j) {
        return position.equals(new Position(i, j));
    }
}
