package codingame.asteroids;

import java.util.Objects;
import java.util.StringJoiner;

class SpeedPerRound {

    private final double xSpeed;
    private final double ySpeed;

    SpeedPerRound(double xSpeed, double ySpeed) {
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    public double xDistanceForRounds(int rounds) {
        return xSpeed * rounds;
    }

    public double yDistanceForRounds(int rounds) {
        return ySpeed * rounds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpeedPerRound that = (SpeedPerRound) o;
        return Double.compare(that.xSpeed, xSpeed) == 0 && Double.compare(that.ySpeed, ySpeed) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xSpeed, ySpeed);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SpeedPerRound.class.getSimpleName() + "[", "]")
                .add("xSpeed=" + xSpeed)
                .add("ySpeed=" + ySpeed)
                .toString();
    }
}
