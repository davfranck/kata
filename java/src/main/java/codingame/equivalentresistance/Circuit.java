package codingame.equivalentresistance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

class Circuit {

    private final String value;

    public Circuit(String circuit) {
        this.value = circuit;
    }

    private boolean isEmpty() {
        return value.trim().isEmpty();
    }

    public boolean isWithSubCircuit() {
        return value.contains("(") || value.contains("[");
    }

    public boolean startsWithSubCircuit() {
        return value.startsWith("(") || value.startsWith("[");
    }

    public List<String> splitToString() {
        return split().stream().map(circuit -> circuit.value).collect(toList());
    }

    private List<Circuit> split() {
        if (isEmpty()) {
            return List.of();
        }
        if (!isWithSubCircuit()) {
            return List.of(new Circuit(value.trim()));
        } else if (startsWithSubCircuit()) {
            return splitWhenBeginWithSeparator(value);
        } else {
            return splitWhenBeginByResistance();
        }
    }

    private List<Circuit> splitWhenBeginByResistance() {
        int nextSubStart = value.replaceAll("\\[", "(").indexOf("(");
        List<Circuit> subCircuits = new ArrayList<>();
        subCircuits.add(new Circuit(value.substring(0, nextSubStart).trim()));
        subCircuits.addAll(new Circuit(value.substring(nextSubStart)).split());
        return subCircuits;
    }

    private List<Circuit> splitWhenBeginWithSeparator(String circuit) {
        List<Circuit> subCircuits = new ArrayList<>();
        int endIndex = findMatchingIndex();
        subCircuits.add(new Circuit(circuit.substring(0, endIndex + 1)));
        subCircuits.addAll(new Circuit(circuit.substring(endIndex + 1).trim()).split());
        return subCircuits;
    }

    private int findMatchingIndex() {
        int nbMatch = 0;
        char startChar = value.charAt(0) == '(' ? '(' : '[';
        char endChar = startChar == '(' ? ')' : ']';
        for (int i = 0; i < value.length(); i++) {
            if (value.charAt(i) == startChar) {
                nbMatch++;
            } else if (value.charAt(i) == endChar) {
                nbMatch--;
            }
            if (nbMatch == 0) {
                return i;
            }
        }
        throw new IllegalStateException("no match found");
    }

    public List<Node> toNodes() {
        if (isWithSubCircuit()) {
            List<String> subCircuits = splitToString();
            List<Node> nodes = new ArrayList<>();
            for (String subCircuit : subCircuits) {
                if (subCircuit.contains("(") || subCircuit.contains("[")) {
                    String inside = subCircuit.substring(1, subCircuit.length() - 1);
                    nodes.add(new GroupNode(new Circuit(inside).toNodes(), GroupType.from(subCircuit.substring(0, 1))));
                } else {
                    nodes.addAll(new Circuit(subCircuit).toNodes());
                }
            }
            return nodes;
        }
        return Arrays.stream(value.trim().split(" "))
                .filter(r -> !r.trim().equals(""))
                .map(Leave::new)
                .collect(toList());
    }
}

class Graph {

    final Node root;

    public Graph(Node root) {
        this.root = root;
    }

    public Node root() {
        return root;
    }

    static Graph from(String circuit) {
        return new Graph(new Circuit(circuit).toNodes().get(0));
    }

    public double compute(Map<String, Integer> valueByResistance) {
        return root.compute(valueByResistance);
    }
}

interface Node {
    double compute(Map<String, Integer> valueByResistance);
}

class GroupNode implements Node {

    private final List<? extends Node> children;

    private final GroupType type;

    public GroupNode(List<? extends Node> children, GroupType type) {
        this.children = children;
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append(type.start).append(" ");
        for (Node child : children) {
            out.append(child.toString()).append(" ");
        }
        out.append(type.end);
        return out.toString();
    }

    @Override
    public double compute(Map<String, Integer> valueByResistance) {
        if (type.equals(GroupType.SERIES)) {
            return children.stream()
                    .mapToDouble(child -> child.compute(valueByResistance))
                    .sum();
        } else {
            double sum = children.stream()
                    .mapToDouble(child -> 1 / child.compute(valueByResistance))
                    .sum();
            return 1 / sum;
        }
    }
}

class Leave implements Node {

    private final String resistance;

    public Leave(String resistance) {
        super();
        this.resistance = resistance;
    }

    @Override
    public String toString() {
        return resistance;
    }

    @Override
    public double compute(Map<String, Integer> valueByResistance) {
        return valueByResistance.get(resistance);
    }
}

enum GroupType {
    SERIES("(", ")"), PARALLEL("[", "]");

    final String start;
    final String end;

    GroupType(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public static GroupType from(String firstCircuitChar) {
        if (firstCircuitChar.equals("(")) {
            return SERIES;
        } else if (firstCircuitChar.equals("[")) {
            return PARALLEL;
        }
        throw new IllegalArgumentException("invalid type : " + firstCircuitChar);
    }

}
