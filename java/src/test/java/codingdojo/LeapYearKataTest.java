package codingdojo;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

// http://codingdojo.org/kata/LeapYears/
public class LeapYearKataTest {

    @Test
    void is_leap_when_is_divisble_by_400() {
        assertThat(isLeapYear(800)).isTrue();
        assertThat(isLeapYear(801)).isFalse();
        assertThat(isLeapYear(1700)).isFalse();
    }

    @Test
    void is_leap_year_when_is_divisible_by_4_but_not_by_100() {
        assertThat(isLeapYear(2008)).isTrue();
        assertThat(isLeapYear(1996)).isTrue();
        assertThat(isLeapYear(1800)).isFalse();
        assertThat(isLeapYear(1600)).isTrue();
    }

    @Test
    void is_not_leap_when_not_divisible_by_4() {
        assertThat(isLeapYear(2018)).isFalse();
        assertThat(isLeapYear(2019)).isFalse();
    }

    private boolean isLeapYear(int year) {
        return year % 400 == 0
                || (year % 4 == 0 && year % 100 != 0);
    }
}
