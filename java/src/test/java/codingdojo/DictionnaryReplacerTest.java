package codingdojo;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * <a href="https://codingdojo.org/kata/DictionaryReplacer/">Kata</a>
 * <p>
 * Create a method that takes a string and a dictionary,
 * and replaces every key in the dictionary pre and suffixed with a dollar sign,
 * with the corresponding value from the Dictionary.
 * </p>
 */
class DictionnaryReplacerTest {


    public static final String SEPARATOR = "\\$";

    @Test
    void empty_input() {
        assertThat(replace("", Map.of())).isEqualTo("");
    }

    @Test
    void no_matching_entry_in_dictionnary() {
        assertThat(replace("\\$name\\$", Map.of())).isEqualTo("\\$name\\$");
    }

    @Test
    void input_matches_entry() {
        assertThat(replace("\\$name\\$", Map.of("name", "John Doe"))).isEqualTo("John Doe");
    }

    @Test
    void one_match_with_text_before_and_after() {
        assertThat(replace("Hello \\$name\\$, welcome back !", Map.of("name", "John Doe"))).isEqualTo("Hello John Doe, welcome back !");
    }

    @Test
    void many_matches() {
        String input = "\\$name\\$ is the friend of \\$friend\\$";
        Map<String, String> dictionnary = Map.of("name", "John Doe", "friend", "Jane Birkin");
        assertThat(replace(input, dictionnary)).isEqualTo("John Doe is the friend of Jane Birkin");
    }

    private String replace(String input, Map<String, String> dictionnary) {
        if (input.isEmpty() || input.indexOf(SEPARATOR) == -1) {
            return input;
        }

        int prefixIndex = input.indexOf(SEPARATOR);

        StringBuilder result = new StringBuilder();

        result.append(input, 0, prefixIndex);

        int suffixIndex = input.indexOf(SEPARATOR, prefixIndex + SEPARATOR.length());
        String keyToReplace = input.substring(prefixIndex + SEPARATOR.length(), suffixIndex);
        result.append(dictionnary.getOrDefault(keyToReplace, input));

        String remaindingText = input.substring(suffixIndex + SEPARATOR.length());
        result.append(replace(remaindingText, dictionnary));

        return result.toString();
    }
}
