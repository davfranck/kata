package codingdojo;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import static java.util.Collections.*;
import static org.assertj.core.api.Assertions.assertThat;

// https://codingdojo.org/kata/Anagram/
// Write a program that generates all two-word anagrams of the string "documenting"
// Here’s a word list you might want to use: https://gist.githubusercontent.com/calvinmetcalf/084ab003b295ee70c8fc/raw/314abfdc74b50f45f3dbbfa169892eff08f940f2/wordlist.txt.
public class AnagramKataTest {

    @Test
    void generates_no_anagram_when_words_does_not_contain_anagram() {
        // when then
        assertThat(new TwoWordAnagramFinder(List.of()).generateTwoWordsAnagram()).isEmpty();
    }

    @Test
    void generates_no_anagram_when_list_of_two_words_does_not_contain_an_anagram() {
        // when then
        assertThat(new TwoWordAnagramFinder(List.of("no", "anagram")).generateTwoWordsAnagram()).isEmpty();
    }

    @Test
    void generates_one_anagram_when_list_of_words_contains_one_anagram_with_letters_unordered() {
        List<String> words = List.of("first", "mendouc", "third", "zaza", "dkjljkf", "ting");
        // when then
        assertThat(new TwoWordAnagramFinder(words).generateTwoWordsAnagram()).containsExactly(new TwoWordAnagram("ting", "mendouc"));
    }

    //    @Disabled
    @Test
    void anagrams_from_word_list() throws IOException {
        List<String> allLines = Files.readAllLines(Path.of("src/test/java/codingdojo/words.txt"));
        List<String> words = allLines.stream()
                .map(String::trim)
                .collect(Collectors.toList());
        List<TwoWordAnagram> result = new TwoWordAnagramFinder(words).generateTwoWordsAnagram();
        System.out.println("anagrammes : " + result);
        Assertions.assertThat(result).hasSize(5);
    }

    private static class TwoWordAnagramFinder {

        static final char[] DOCUMENTING_LETTERS = "documenting".toCharArray();
        int DOCUMENTING_LENGTH = "documenting".length();

        static {
            Arrays.sort(DOCUMENTING_LETTERS);
        }

        private final Map<Integer, List<String>> wordsByLength;

        public TwoWordAnagramFinder(List<String> words) {
            this.wordsByLength = words.stream().collect(Collectors.groupingBy(String::length));
        }

        /**
         * @return la liste des anagrammes composé de deux mots du mot "documenting"
         */
        public List<TwoWordAnagram> generateTwoWordsAnagram() {
            return wordsByLength.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByKey())
                    .filter(entry -> DOCUMENTING_LENGTH - entry.getKey() >= entry.getKey())
                    .flatMap(entry -> anagrams(entry.getKey(), entry.getValue()))
                    .collect(Collectors.toList());
        }

        private Stream<TwoWordAnagram> anagrams(Integer currentLength, List<String> words) {
            return words.stream().flatMap(word -> {
                List<String> otherWords = wordsByLength.getOrDefault(DOCUMENTING_LENGTH - currentLength, emptyList());
                return otherWords.stream()
                        .flatMap(second -> getTwoWordAnagram(word, second).stream());
            });
        }

        private Optional<TwoWordAnagram> getTwoWordAnagram(String first, String second) {
            String wordToTest = first + second;
            if (wordToTest.length() != DOCUMENTING_LENGTH) {
                return Optional.empty();
            }
            char[] lettersToCheck = wordToTest.toCharArray();
            Arrays.sort(lettersToCheck);
            boolean isAnagram = Arrays.equals(DOCUMENTING_LETTERS, lettersToCheck);
            if (isAnagram) {
                return Optional.of(new TwoWordAnagram(first, second));
            } else {
                return Optional.empty();
            }
        }
    }


    private static class TwoWordAnagram {
        private final String first;
        private final String second;

        public TwoWordAnagram(String first, String second) {
            this.first = Objects.requireNonNull(first);
            this.second = Objects.requireNonNull(second);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TwoWordAnagram that = (TwoWordAnagram) o;
            return first.equals(that.first) && second.equals(that.second);
        }

        @Override
        public int hashCode() {
            return Objects.hash(first, second);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("TwoWordAnagram{");
            sb.append("first='").append(first).append('\'');
            sb.append(", second='").append(second).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
}
