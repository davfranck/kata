package codingdojo.stringcalculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * https://codingdojo.org/kata/StringCalculator/
 */
class StringCalculatorTest {

    @Test
    void add_numbers_with_empty_input() {
        assertAddIsEqualTo("", 0);
    }

    @ParameterizedTest
    @CsvSource(value = {"1:1", "2:2", "2.2:2.2"}, delimiter = ':')
    void add_numbers_with_one_number(String input, String expected) {
        assertAddIsEqualTo(input, Double.parseDouble(expected));
    }

    @ParameterizedTest
    @CsvSource(value = {"1,1:2", "2,1:3", "2.1,1,3.5:6.6"}, delimiter = ':')
    void add_more_numbers(String input, String expected) {
        assertAddIsEqualTo(input, Double.parseDouble(expected));
    }

    @Test
    void add_with_new_line_separator() {
        assertAddIsEqualTo("1\n2", 3);
        assertAddIsEqualTo("2\n5,7", 14);
    }

    private void assertAddIsEqualTo(String input, double expected) {
        Warned result = StringCalulator.add(input);
        assertThat(result.hasValue()).isTrue();
        assertThat(result.value).isEqualTo(expected);
    }

    @Test
    void cant_add_numbers_when_input_ends_with_separator() {
        Warned result = StringCalulator.add("1,2,");
        assertThat(result.hasValue()).isFalse();
        assertThat(result.message).isEqualTo("Number expected but EOF found");
    }

    @Test
    void cand_add_number_too_many_separators() {
        Warned warned = StringCalulator.add("1.12,\n,6");
        assertThat(warned.hasValue()).isFalse();
        assertThat(warned.message).isEqualTo("Number expected but '\n' found at position 5");
    }

    @Test
    void can_add_with_custom_separator() {
        //[delimiter]\n[numbers]
        assertAddIsEqualTo("//;\n1;2", 3);
        assertAddIsEqualTo("//|\n1|2|3", 6);
        assertAddIsEqualTo("//sep\n1sep2sep4", 7);// WIP
    }


}
