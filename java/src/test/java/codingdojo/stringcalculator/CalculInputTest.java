package codingdojo.stringcalculator;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CalculInputTest {

    @Test
    void noDelimiter() {
        assertThat(CalculInput.from("1,2").input).isEqualTo("1,2");
    }

    @Test
    void with_delimiter() {
        CalculInput calculInput = CalculInput.from("//;\n1,2");
        assertThat(calculInput.input).isEqualTo("1,2");
        assertThat(calculInput.delimiter).isEqualTo(';');
    }
}
