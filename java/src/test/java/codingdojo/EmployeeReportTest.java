package codingdojo;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.atIndex;

// https://codingdojo.org/kata/Employee-Report/
public class EmployeeReportTest {

    /*
    Requirement 1.
    As shop owner I want to view a list of all employees, which are older than 18 years, so that I know who is allowed
    to work on Sundays.
     */
    @Nested
    class OlderThat18 {
        @Test
        void when_all_employees_are_older_than_18_should_return_same_list() {
            assertThat(authorizedEmployeesForSunday(List.of(new Employee("Max", 18)))).hasSize(1);
        }

        @Test
        void employees_younger_thatn_18_should_not_be_authorized() {
            assertThat(authorizedEmployeesForSunday(List.of(new Employee("Bob", 17)))).isEmpty();
        }
    }

    /*
    As shop owner I want the list of employees to be sorted by their name, so I can find employees easier.
     */
    @Test
    void authorized_employees_should_be_sorted_by_name_desc() {
        List<Employee> employees = List.of(new Employee("Ab", 19), new Employee("Aa", 19), new Employee("Ac", 20));
        assertThat(authorizedEmployeesForSunday(employees))
                .satisfies(first -> assertThat(first.name).isEqualTo("Ac"), atIndex(0))
                .satisfies(second -> assertThat(second.name).isEqualTo("Ab"), atIndex(1))
                .satisfies(third -> assertThat(third.name).isEqualTo("Aa"), atIndex(2));
    }

    /*
    As shop owner I want the list of employees to be capitalized, so I can read it better.
     */
    @Test
    void employees_names_should_be_capitalized() {
        assertThat(authorizedEmployeesForSunday(List.of(new Employee("aaaa", 20))))
                .singleElement()
                .satisfies(e -> assertThat(e.name).isEqualTo("Aaaa"));
    }

    private List<Employee> authorizedEmployeesForSunday(List<Employee> employees) {
        return employees.stream()
                .filter(employee -> employee.age >= 18)
                .sorted(Comparator.comparing((Employee e) -> e.name).reversed())
                .collect(toList());
    }

    private class Employee {
        private final String name;
        private final int age;

        public Employee(String name, int age) {
            this.name = capitalized(name);
            this.age = age;
        }

        private String capitalized(String name) {
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }
    }
}
