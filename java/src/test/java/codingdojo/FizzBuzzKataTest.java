package codingdojo;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

// à partir du livre "Java By Comparaison" 
/* 
Write a program that prints the numbers from 1 to 100 to the console. 
But for the multiple of three print Fizz instead of the number anf for multiples of five print BUzz. 
For the numbers that are multiples of both three and five, print FizzBuzz. 
...
You should implement the FizzBuzz algorithm in a class called ConsoleBasedFizzBuzz, which implements the FizzBuzz interface. 
The interface provides a method that takes the first and last numbers to print as arguments.
...
*/
class FizzBuzzKataTest {

    @Test
    void print_a_number() {
        // given
        FakePrinter fakePrinter = new FakePrinter();
        // when
        new ConsoleBasedFizzBuzz(fakePrinter).print(1, 1);
        // then
        assertThat(fakePrinter.printed).isEqualTo(List.of("1"));
    }

    @Test
    void print_two_numbers() {
        // given
        FakePrinter fakePrinter = new FakePrinter();
        // when
        new ConsoleBasedFizzBuzz(fakePrinter).print(1, 2);
        // then
        assertThat(fakePrinter.printed).isEqualTo(List.of("1", "2"));
    }

    @Test
    void print_fizz() {
        // given
        FakePrinter fakePrinter = new FakePrinter();
        // when
        new ConsoleBasedFizzBuzz(fakePrinter).print(2, 4);
        // then
        assertThat(fakePrinter.printed).isEqualTo(List.of("2", "Fizz", "4"));
    }

    @Test
    void print_buzz() {
        // given
        FakePrinter fakePrinter = new FakePrinter();
        // when
        new ConsoleBasedFizzBuzz(fakePrinter).print(2, 6);
        // then
        assertThat(fakePrinter.printed).isEqualTo(List.of("2", "Fizz", "4", "Buzz", "Fizz"));
    }

    @Test
    void print_fizz_buzz() {
        // given
        FakePrinter fakePrinter = new FakePrinter();
        // when
        new ConsoleBasedFizzBuzz(fakePrinter).print(14, 16);
        // then
        assertThat(fakePrinter.printed).isEqualTo(List.of("14", "FizzBuzz", "16"));
    }

    interface FizzBUzz {
        void print(int from, int to);
    }

    static class ConsoleBasedFizzBuzz implements FizzBUzz {

        private final Printer printer;

        public ConsoleBasedFizzBuzz(FakePrinter printer) {
            this.printer = printer;
        }

        @Override
        public void print(int from, int to) {
            for (int i = from; i <= to; i++) {
                printOne(i);
            }
        }

        private void printOne(int i) {
            String toPrint = "";
            if (i % 3 != 0 && i % 5 != 0) {
                toPrint = String.valueOf(i);
            } else {
                if (i % 3 == 0) {
                    toPrint = "Fizz";
                }
                if (i % 5 == 0) {
                    toPrint += "Buzz";
                }
            }
            printer.print(toPrint);
        }
    }

    interface Printer {
        void print(String toPrint);
    }

    private static class FakePrinter implements Printer {
        List<String> printed = new ArrayList<>();

        @Override
        public void print(String toPrint) {
            printed.add(toPrint);
        }
    }
}
