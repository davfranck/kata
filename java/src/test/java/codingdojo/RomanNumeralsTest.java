package codingdojo;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

// https://codingdojo.org/kata/RomanNumerals/
class RomanNumeralsTest {

    @Test
    void convert_roman() {
        assertThat(toRoman(1)).isEqualTo("I");
        assertThat(toRoman(2)).isEqualTo("II");
        assertThat(toRoman(3)).isEqualTo("III");
        assertThat(toRoman(4)).isEqualTo("IV");
        assertThat(toRoman(5)).isEqualTo("V");
        assertThat(toRoman(6)).isEqualTo("VI");
        assertThat(toRoman(8)).isEqualTo("VIII");
        assertThat(toRoman(9)).isEqualTo("IX");
        assertThat(toRoman(10)).isEqualTo("X");
        assertThat(toRoman(13)).isEqualTo("XIII");
        assertThat(toRoman(14)).isEqualTo("XIV");
        assertThat(toRoman(15)).isEqualTo("XV");
        assertThat(toRoman(20)).isEqualTo("XX");
        assertThat(toRoman(30)).isEqualTo("XXX");
        assertThat(toRoman(40)).isEqualTo("XL");
        assertThat(toRoman(41)).isEqualTo("XLI");
        assertThat(toRoman(48)).isEqualTo("XLVIII");
        assertThat(toRoman(49)).isEqualTo("XLIX");
        assertThat(toRoman(50)).isEqualTo("L");
        assertThat(toRoman(55)).isEqualTo("LV");
    }

    private static Map<Integer, String> romanByInt = new LinkedHashMap<>();

    static {
        romanByInt.put(50, "L");
        romanByInt.put(40, "XL");
        romanByInt.put(10, "X");
        romanByInt.put(9, "IX");
        romanByInt.put(5, "V");
        romanByInt.put(4, "IV");
        romanByInt.put(1, "I");
    }

    private String toRoman(int number) {
        Map.Entry<Integer, String> first = romanByInt.entrySet()
                .stream()
                .filter(entry -> number >= entry.getKey())
                .findFirst()
                .orElse(null);
        if (number > 0) {
            return first.getValue() + toRoman(number - first.getKey());
        }
        return "";
    }
}
