package codingdojo.bankocrkata;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

// https://codingdojo.org/kata/BankOCR/
class BankOcrKataTest {

    @Nested
    class AccountNumberParserShould {

        @ParameterizedTest
        @CsvSource({
                "src/test/resources/ocr_kata/only_zeros.txt,000000000",
                "src/test/resources/ocr_kata/only_ones.txt,111111111",
                "src/test/resources/ocr_kata/one_to_nine.txt,123456789",
                "src/test/resources/ocr_kata/first_zero_then_ones.txt,011111111"})
        void converts_zeros_to_numbers(String filename, String expectedAccountNumber) throws IOException {
            // given
            List<String> allLines = Files.readAllLines(Path.of(filename));
            // when
            List<AccountNumber> accountNumberNumbers = AccountParser.parseAccounts(allLines);
            // then
            assertThat(accountNumberNumbers).singleElement().isEqualTo(new AccountNumber(expectedAccountNumber));
        }

        @Test
        void converts_two_accounts() throws IOException {
            // given
            List<String> allLines = Files.readAllLines(Path.of("src/test/resources/ocr_kata/two_accounts.txt"));
            // when
            List<AccountNumber> accountNumberNumbers = AccountParser.parseAccounts(allLines);
            // then
            assertThat(accountNumberNumbers).containsExactly(
                    new AccountNumber("123456789"),
                    new AccountNumber("011111111"));
        }

        @Test
        void converts_invalid_account() throws IOException {
            // given
            List<String> allLines = Files.readAllLines(Path.of("src/test/resources/ocr_kata/valid_and_invalid_inputs.txt"));
            // when
            List<AccountNumber> accountNumberNumbers = AccountParser.parseAccounts(allLines);
            // then
            assertThat(accountNumberNumbers).containsExactly(
                    new AccountNumber("457508000"),
                    new AccountNumber("664371495"),
                    new AccountNumber("86110??36"));
        }
    }

    @Nested
    class AccountNumberShould {

        @Test
        void be_valid() {
            assertThat(new AccountNumber("345882865").isValid()).isTrue();
        }

        @Test
        void be_invalid() {
            assertThat(new AccountNumber("000010000").isValid()).isFalse();
            assertThat(new AccountNumber("000?10000").isValid()).isFalse();
        }
    }

    @Nested
    class AccountWriterShould {
        
        @Test
        void write_account_numbers_to_file() throws IOException {
            Path tempFile = Files.createTempFile("bank_ocr_output", ".txt");
            List<AccountNumber> accountNumbers = List.of(
                    new AccountNumber("345882865"),
                    new AccountNumber("000010000"),
                    new AccountNumber("00001000?"));
            // when
            AccountWriter.writeAccountsToFile(tempFile, accountNumbers);
            // then
            assertThat(Files.readAllLines(tempFile)).containsExactly("345882865", "000010000 ILL", "00001000? ERR");
        }
    }

    // TODO : essayer de réparer une mauvaise identification 
    // Un chiffre a été identifié à la place d'un autre
    // Un chiffre n'a pas été identifié : essayer de le réparer
    // Si deux réparations possibles -> statut AMB
}
