package stackexchange;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

// https://stackoverflow.com/questions/67258770/how-to-shorten-multiple-if-statements-without-switch-statement?noredirect=1
public class IfStatementsTest {

    @Test
    void purchased_price_greater_than_zero_lower_than_ten() {
        // given
        Product product = new Product(5);
        // when
        updateProduct(product);
        // then
        Assertions.assertThat(product.mhPrice).isEqualTo(15);
        Assertions.assertThat(product.delivery).isEqualTo("XDP");

    }

    @Test
    void purchased_price_between_eleven_and_twelve() {
        // given
        Product product = new Product(15);
        // when
        updateProduct(product);
        // then
        Assertions.assertThat(product.mhPrice).isEqualTo(15 * 2.5);
        Assertions.assertThat(product.delivery).isEqualTo("XDP");

    }

    private void updateProduct(Product product) {
        ProductUpdate firstProductUpdate = new ProductUpdate(-1, 10, 3, "XDP");
        firstProductUpdate
                .next(new ProductUpdate(10, 20, 2.5, "XDP"))
                .next(new ProductUpdate(20, 50, 2, "XDP"))
                .next(new ProductUpdate(50, 100, 1.9, "XDP"))
                .next(new ProductUpdate(100, 200, 1.8, "XDP"));
        firstProductUpdate.update(product);
    }

    private class ProductUpdate {
        private final int lowerBound;
        private final int upperBound;
        private final double mult;
        private final String delivery;
        private ProductUpdate next;

        public ProductUpdate(int lowerBound, int upperBound, double mult, String delivery) {
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
            this.mult = mult;
            this.delivery = delivery;
        }

        public ProductUpdate next(ProductUpdate next) {
            this.next = next;
            return next;
        }

        public void update(Product product) {
            if (product.purchasedPrice > lowerBound && product.purchasedPrice <= upperBound) {
                product.mhPrice = product.purchasedPrice * mult;
                product.delivery = delivery;
            } else if (next == null) {
                throw new IllegalStateException("no matching boudaries");
            } else {
                next.update(product);
            }
        }
    }

    private class Product {
        private final int purchasedPrice;
        public double mhPrice = 0;
        public String delivery = "";

        public Product(int purchasedPrice) {
            this.purchasedPrice = purchasedPrice;
        }
    }
}
