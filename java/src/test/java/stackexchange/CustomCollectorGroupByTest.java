package stackexchange;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

// https://stackoverflow.com/questions/66811582/how-to-create-list-of-object-instead-of-map-with-groupinby-collector
class CustomCollectorGroupByTest {

    @Test
    void myOwnCollector() {
        List<Expense> expenses = List.of(
                new Expense("type1", 1),
                new Expense("type2", 1),
                new Expense("type3", 10),
                new Expense("type1", 3),
                new Expense("type3", 3),
                new Expense("type2", 2));

        Assertions.assertThat(expenses
                .parallelStream()
                .collect(new PieEntriesCollector()))
                .containsExactlyInAnyOrder(
                        new PieEntry("type1", 4),
                        new PieEntry("type3", 13),
                        new PieEntry("type2", 3));
    }

    private static class PieEntriesCollector implements Collector<Expense, PieEntries, Collection<PieEntry>> {

        @Override
        public Supplier<PieEntries> supplier() {
            return PieEntries::new;
        }

        @Override
        public BiConsumer<PieEntries, Expense> accumulator() {
            return (acc, expense) -> acc.add(new PieEntry(expense.type, expense.amount));
        }

        @Override
        public BinaryOperator<PieEntries> combiner() {
            return PieEntries::merge;
        }

        @Override
        public Function<PieEntries, Collection<PieEntry>> finisher() {
            return PieEntries::entries;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return Set.of(Characteristics.UNORDERED);
        }
    }

    private static class PieEntries {

        private final Map<String, PieEntry> sumByType = new HashMap<>();

        public void add(PieEntry newPieEntry) {
            this.sumByType.merge(newPieEntry.type, new PieEntry(newPieEntry.type, newPieEntry.amount), (first, second) -> first.add(second.amount));
        }

        public Collection<PieEntry> entries() {
            return sumByType.values();
        }

        public PieEntries merge(PieEntries b) {
            b.sumByType.forEach((key, value) -> this.sumByType.merge(key, value, (v1, v2) -> new PieEntry(key, v1.amount + v2.amount)));
            return this;
        }
    }

    private static class PieEntry {

        private final String type;
        private final int amount;

        public PieEntry(String type, int amount) {
            this.type = type;
            this.amount = amount;
        }

        public PieEntry add(int amount) {
            return new PieEntry(this.type, this.amount + amount);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PieEntry pieEntry = (PieEntry) o;
            return amount == pieEntry.amount;
        }

        @Override
        public int hashCode() {
            return Objects.hash(amount);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("PieEntry{");
            sb.append("type='").append(type).append('\'');
            sb.append(", amount=").append(amount);
            sb.append('}');
            return sb.toString();
        }
    }

    private static class Expense {
        private final String type;
        private final int amount;

        public Expense(String type, int amount) {
            this.type = type;
            this.amount = amount;
        }
    }
}
