package stackexchange.exceltohtml;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

import java.io.*;

public class OldExcelToHtmlFeature {

    @Test
    void empty_file() {
        ExcelToHtml excelToHtml = new ExcelToHtml("src/test/resources/poi_1.xlsx");
        File htmlFile = excelToHtml.toHtml();
        Approvals.verify(htmlFile);
    }

    private class ExcelToHtml {
        public ExcelToHtml(String excelFilePath) {
        }

        public File toHtml() {
            try {
                File tmpFile = File.createTempFile("test", ".tmp");
                FileWriter writer = new FileWriter(tmpFile);
                writer.write("<html>");
                writer.write(System.lineSeparator());
                writer.write("<table>");
                writer.write(System.lineSeparator());
                writer.write("</table>");
                writer.write(System.lineSeparator());
                writer.write("</html>");
                writer.close();
                return tmpFile;
            } catch (IOException e) {
                throw new RuntimeException("Unable to transform file", e);
            }
        }
    }
}
