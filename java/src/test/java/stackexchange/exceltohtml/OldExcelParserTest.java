package stackexchange.exceltohtml;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

// https://poi.apache.org/components/spreadsheet/quick-guide.html#ReadWriteWorkbook
// https://poi.apache.org/apidocs/5.0/index.html
public class OldExcelParserTest {

    @Test
    void raiseOneNewSheet() {
        // given
        ExcelFileEventListener listener = mock(ExcelFileEventListener.class);
        // when
        new ExcelParser("src/test/resources/poi_1.xlsx", listener).parse();
        // then
        then(listener).should().newSheet();
    }


    private interface ExcelFileEventListener {
        void newSheet();
    }

    private class ExcelParser {
        private final String path;
        private final ExcelFileEventListener listener;

        public ExcelParser(String path, ExcelFileEventListener listener) {
            this.path = path;
            this.listener = listener;
        }

        public void parse() {
            try (InputStream inp = new FileInputStream(path)) {
                Workbook wb = WorkbookFactory.create(inp);
                int numberOfSheets = wb.getNumberOfSheets();
                for (int i = 0; i < numberOfSheets; i++) {
                    listener.newSheet();
                    Sheet sheet = wb.getSheetAt(i);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
