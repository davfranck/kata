package stackexchange.exceltohtml;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.approvaltests.reporters.DiffReporter;
import org.approvaltests.reporters.UseReporter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;

class ExcelToHtmlTest {

    @Test
    @UseReporter(DiffReporter.class)
    void generate() {
        // given
        ExcelToHtml excelToHtml = new ExcelToHtml();
        File excelFile = new File("src/test/resources/poi_1.xlsx");
        // when
        File generatedHtmlFile = excelToHtml.generate(excelFile, new Style(true));
        // then
        // http://joel-costigliola.github.io/assertj/core-8/api/org/assertj/core/api/AbstractFileAssert.html#hasSameContentAs-java.io.File-
        Assertions.assertThat(generatedHtmlFile)
                .hasSameContentAs(new File("src/test/java/stackexchange/exceltohtml/ExcelToHtmlFeature.generate.expected.html"));
    }

    static class ExcelToHtml {
        public File generate(File excelFile, Style style) {
            ExcelToHtmlWriter excelToHtklWriter = new ExcelToHtmlWriter(style);
            ExcelParser excelParser = new ExcelParser(excelFile, excelToHtklWriter);
            excelParser.parse();
            return excelToHtklWriter.generatedFile();
        }
    }

    static class ExcelParser {

        private final File excelFile;
        private final ExcelFileEventListener listener;

        public ExcelParser(File excelFile, ExcelFileEventListener listener) {
            this.excelFile = excelFile;
            this.listener = listener;
        }

        public void parse() {
            try (InputStream inp = new FileInputStream(excelFile)) {
                parseWorkbook(inp);
            } catch (IOException e) {
                throw new RuntimeException("error during parsing", e);
            }
        }

        private void parseWorkbook(InputStream inp) throws IOException {
            listener.start();
            Workbook wb = WorkbookFactory.create(inp);
            int numberOfSheets = wb.getNumberOfSheets();
            for (int i = 0; i < numberOfSheets; i++) {
                parseSheet(wb, i);
            }
            listener.end();
        }

        private void parseSheet(Workbook wb, int i) {
            listener.newSheet();
            Sheet sheet = wb.getSheetAt(i);
            for (Row row : sheet) {
                parseRow(row);
            }
            listener.endSheet();
        }

        private void parseRow(Row row) {
            listener.newRow();
            for (Cell cell : row) {
                listener.newCell();
                listener.cellContent(cell.toString());
                listener.endCell();
            }
            listener.endRow();
        }
    }

    static class ExcelToHtmlWriter implements ExcelFileEventListener {

        private static final String LINE_SEPARATOR = System.lineSeparator();
        private final Style style;

        private FileWriter writer;
        private File tmpFile;

        public ExcelToHtmlWriter(Style style) {
            this.style = style;
        }

        @Override
        public void newSheet() {
            write("<table>");
            write(LINE_SEPARATOR);
        }

        @Override
        public void endSheet() {
            write("</table>");
            write(LINE_SEPARATOR);
        }

        @Override
        public void newRow() {
            write("<tr>");
            write(LINE_SEPARATOR);
        }

        @Override
        public void endRow() {
            write(LINE_SEPARATOR + "</tr>" + LINE_SEPARATOR);
        }

        @Override
        public void newCell() {
            write("<td>");
        }

        @Override
        public void endCell() {
            write("</td>");
        }

        @Override
        public void cellContent(String value) {
            write(value);
        }

        @Override
        public void start() {
            try {
                tmpFile = File.createTempFile("test", ".tmp");
                writer = new FileWriter(tmpFile);
                write("<html>" + LINE_SEPARATOR);
                write("<head>" + LINE_SEPARATOR);
                write("<style>" + LINE_SEPARATOR);
                write(style.content());
                write("</style>" + LINE_SEPARATOR);
                write("</head>" + LINE_SEPARATOR);
                write("<body>" + LINE_SEPARATOR);
            } catch (IOException e) {
                throw new RuntimeException("unable to initialize file", e);
            }
        }

        @Override
        public void end() {
            try {
                write("</body>" + LINE_SEPARATOR);
                write("</html>" + LINE_SEPARATOR);
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException("unbale to close writer", e);
            }
        }

        public File generatedFile() {
            return tmpFile;
        }

        private void write(String toWrite) {
            try {
                writer.write(toWrite);
            } catch (IOException e) {
                try {
                    writer.close();
                } catch (IOException ioException) {
                } finally {
                    throw new RuntimeException("unable to write to file", e);
                }
            }
        }
    }

    static class Style {
        private final boolean cellBorders;

        public Style(boolean cellBorders) {
            this.cellBorders = cellBorders;
        }

        public String content() {
            StringBuilder content = new StringBuilder("td {");
            content.append(System.lineSeparator());
            content.append("padding: 9px;");
            if (cellBorders) {
                content.append(System.lineSeparator());
                content.append("border: 1px solid black;");
                content.append(System.lineSeparator());
                content.append("border-collapse: collapse;");
            }
            content.append(System.lineSeparator());
            content.append("}");
            content.append(System.lineSeparator());
            return content.toString();
        }
    }

    interface ExcelFileEventListener {
        void newSheet();

        void start();

        void end();

        void endSheet();

        void newRow();

        void endRow();

        void newCell();

        void endCell();

        void cellContent(String value);
    }
}
