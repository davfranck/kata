package stackexchange.fitnessclub;

import org.junit.jupiter.api.Test;

import java.math.*;

import static org.assertj.core.api.Assertions.assertThat;

// https://codereview.stackexchange.com/questions/256682/how-to-make-my-code-more-oop/
class FitnessClub2Test {

    protected static final BigDecimal MEMBERSHIP_FOR_ONE_MONTH = BigDecimal.valueOf(140);
    protected static final BigDecimal ONE_SESSION_PRICE = BigDecimal.valueOf(85);
    protected static final BigDecimal AGE_DISCOUNT = BigDecimal.valueOf(0.3);
    protected static final BigDecimal SESSION_DISCOUNT = BigDecimal.valueOf(0.15);

    @Test
    void adultMembership() {
        assertThat(new Membership(20, 1).price()).isEqualTo(BigDecimal.valueOf(140));
        assertThat(new Membership(20, 2).price()).isEqualTo(BigDecimal.valueOf(280));
    }

    @Test
    void seniorMembership() {
        assertThat(new Membership(60, 1).price()).isEqualTo(BigDecimal.valueOf(98.0));
    }

    @Test
    void buyAPersonalTrainingSession() {
        assertThat(new PersonalTrainingSession(1).price()).isEqualTo(BigDecimal.valueOf(85));
        assertThat(new PersonalTrainingSession(2).price()).isEqualTo(BigDecimal.valueOf(170));
    }

    @Test
    void buySixPersonalTrainingSession() {
        assertThat(new PersonalTrainingSession(6).price()).isEqualTo(BigDecimal.valueOf(43350, 2));
    }

    @Test
    void adultBuyMembershipForTwoMonthAndTwoPersonalTrainingSessions() {
        Membership membershipOrder = new Membership(20, 2);
        PersonalTrainingSession personalTrainingSession = new PersonalTrainingSession(2);
        assertThat(new Order(membershipOrder, personalTrainingSession).total()).isEqualTo(BigDecimal.valueOf(450));
    }

    @Test
    void adultBuyMembershipForOneMonthAndOnePersonalTrainingSession() {
        Membership membershipOrder = new Membership(20, 1);
        PersonalTrainingSession personalTrainingSession = new PersonalTrainingSession(1);
        assertThat(new Order(membershipOrder, personalTrainingSession).total()).isEqualTo(BigDecimal.valueOf(225));
    }

    private class Membership {
        protected static final int AGE_BOUNDARY_FOR_DISCOUNT = 60;
        private final int age;
        private final BigDecimal nbMonth;

        public Membership(int age, int nbMonth) {
            this.age = age;
            this.nbMonth = BigDecimal.valueOf(nbMonth);
        }

        public BigDecimal price() {
            BigDecimal totalWithoutDiscount = MEMBERSHIP_FOR_ONE_MONTH.multiply(nbMonth);
            BigDecimal discount = age < AGE_BOUNDARY_FOR_DISCOUNT ? BigDecimal.ZERO : AGE_DISCOUNT;
            return totalWithoutDiscount.subtract(totalWithoutDiscount.multiply(discount));
        }
    }

    private class PersonalTrainingSession {
        protected static final int NB_SESSION_BOUNDARY_FOR_DISCOUNT = 6;
        private final BigDecimal nbSession;

        public PersonalTrainingSession(int nbSession) {
            this.nbSession = BigDecimal.valueOf(nbSession);
        }

        public BigDecimal price() {
            BigDecimal totalWithoutDiscount = ONE_SESSION_PRICE.multiply(nbSession);
            BigDecimal discount = nbSession
                    .intValue() >= NB_SESSION_BOUNDARY_FOR_DISCOUNT ? SESSION_DISCOUNT : BigDecimal.ZERO;
            return totalWithoutDiscount.subtract(totalWithoutDiscount.multiply(discount));
        }
    }


    private class Order {
        private final Membership membershipOrder;
        private final PersonalTrainingSession personalTrainingSession;

        public Order(Membership membershipOrder, PersonalTrainingSession personalTrainingSession) {
            this.membershipOrder = membershipOrder;
            this.personalTrainingSession = personalTrainingSession;
        }

        public BigDecimal total() {
            return membershipOrder.price().add(personalTrainingSession.price());
        }
    }
}
