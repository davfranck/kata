package stackexchange.fitnessclub;

import org.junit.jupiter.api.Test;

import java.math.*;

import static org.assertj.core.api.Assertions.assertThat;

// https://codereview.stackexchange.com/questions/256682/how-to-make-my-code-more-oop/
class FitnessClubTest {

    @Test
    void adultBuyNoSession() {
        assertThat(new Pricer(20, 0, 0).membershipAmount()).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    void adultBuyMembershipForOneMonth() {
        assertThat(new Pricer(20, 1, 0).membershipAmount()).isEqualTo(Pricer.MEMBERSHIP_FOR_ONE_MONTH);
    }

    @Test
    void adultBuyMembershipForOneMonthAndOneSession() {
        assertThat(new Pricer(20, 1, 1).membershipAmount()).isEqualTo(BigDecimal.valueOf(225));
    }

    @Test
    void adultBuyMembershipForOneMonthAndSixSessions() {
        assertThat(new Pricer(20, 1, 6).membershipAmount()).isEqualTo(BigDecimal.valueOf(57350, 2));
    }

    @Test
    void seniorBuyMemberShipForOneMonth() {
        assertThat(new Pricer(60, 1, 0).membershipAmount()).isEqualTo(BigDecimal.valueOf(98.0));
    }

}
