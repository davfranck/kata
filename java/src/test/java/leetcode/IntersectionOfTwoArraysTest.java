package leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/674/
public class IntersectionOfTwoArraysTest {

    @Test
    void intersection() {
        Assertions.assertArrayEquals(new int[]{2, 2}, intersect(new int[]{1, 2, 2, 1}, new int[]{2, 2}));
    }

    @Test
    void intersection2() {
        Assertions.assertArrayEquals(new int[]{2}, intersect(new int[]{1, 2, 2, 1}, new int[]{2}));
    }

    @Test
    void intersection3() {
        Assertions.assertArrayEquals(new int[]{4, 9}, intersect(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4}));
    }

    private int[] intersect(int[] nums1, int[] nums2) {
        List<Integer> list1 = Arrays.stream(nums1).boxed().collect(Collectors.toList());
        List<Integer> list1Copy = Arrays.stream(nums1).boxed().collect(Collectors.toList());
        List<Integer> list2 = Arrays.stream(nums2).boxed().collect(Collectors.toList());
        list2.forEach(num -> {
            int i = list1.indexOf(num);
            if (i != -1) {
                list1.remove(i);
            }
        });
        list1.forEach(num -> {
            int i = list1Copy.indexOf(num);
            if (i != -1) {
                list1Copy.remove(i);
            }
        });
        return list1Copy.stream().mapToInt(Integer::intValue).toArray();
    }
}
