package leetcode;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class RotateArrayTest {

    @Test
    void one_number() {
        assertArrayEquals(new int[]{1}, rotate(new int[]{1}, 1));
    }

    @Test
    void two_numbers_one_rotation() {
        assertArrayEquals(new int[]{2, 1}, rotate(new int[]{1, 2}, 1));
    }

    @Test
    void two_numbers_two_rotations() {
        assertArrayEquals(new int[]{1, 2}, rotate(new int[]{1, 2}, 2));
    }

    @Test
    void many_numbers_less_rotations_than_array_size() {
        assertArrayEquals(new int[]{5, 6, 7, 1, 2, 3, 4}, rotate(new int[]{1, 2, 3, 4, 5, 6, 7}, 3));
    }

    @Test
    void many_numbers_more_rotations_than_array_size() {
        assertArrayEquals(new int[]{4, 1, 2, 3,}, rotate(new int[]{1, 2, 3, 4}, 5));
    }

    private int[] rotate(int[] nums, int k) {
        if (k > nums.length) {
            k = k % nums.length;
        }
        if (k == 0) {
            return nums;
        }
        int[] end = Arrays.copyOfRange(nums, nums.length - k, nums.length);
        int[] start = Arrays.copyOfRange(nums, 0, nums.length - k);
        System.arraycopy(end, 0, nums, 0, end.length);
        System.arraycopy(start, 0, nums, end.length, start.length);
        return nums;
    }

    private void rotate(int[] nums) {
        int tmp = nums[0];
        nums[0] = nums[nums.length - 1];
        nums[nums.length - 1] = tmp;
    }
}
