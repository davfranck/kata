package leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PlusOneTest {

    @Test
    void one_number() {
        Assertions.assertArrayEquals(new int[]{1}, plusOne(new int[]{0}));
    }

    @Test
    void one_number_transformed_in_two_digits_number() {
        Assertions.assertArrayEquals(new int[]{1, 0}, plusOne(new int[]{9}));
    }

    @Test
    void three_digits_to_four_digits() {
        Assertions.assertArrayEquals(new int[]{1, 0, 0, 0}, plusOne(new int[]{9, 9, 9}));
    }

    @Test
    void should_increment_hundreds() {
        Assertions.assertArrayEquals(new int[]{9, 1, 0}, plusOne(new int[]{9, 0, 9}));
    }


    private int[] plusOne(int[] digits) {
        return plusOne(digits, digits.length - 1);
    }

    private int[] plusOne(int[] digits, int currentIndex) {
        if (currentIndex == -1) {
            // ajouter un 1 devant
            int[] newDigits = new int[digits.length + 1];
            newDigits[0] = 1;
            System.arraycopy(digits, 0, newDigits, 1, digits.length);
            return newDigits;
        } else if (digits[currentIndex] != 9) {
            digits[currentIndex] = digits[currentIndex] + 1;
            return digits;
        } else {
            digits[currentIndex] = 0;
            return plusOne(digits, currentIndex - 1);
        }
    }
}
