package leetcode;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

// https://leetcode.com/explore/featured/card/top-interview-questions-easy/92/array/727/
public class RemoveDuplicatesFromSortedArrayTest {

    // []
    @Test
    void empty_array() {
        assertEquals(0, removeDuplicates(new int[0]));
    }

    // [1]
    @Test
    void one_number() {
        assertEquals(1, removeDuplicates(new int[]{1}));
    }

    // [1,2]
    @Test
    void two_numbers() {
        assertEquals(2, removeDuplicates(new int[]{1, 2}));
    }

    // [1,1,2] -> [1,2,_]
    @Test
    void three_numbers() {
        assertEquals(2, removeDuplicates(new int[]{1, 1, 2}));
    }

    // [1,1,1,2,2,3,4,4] -> [1,2,3,4,5,_,_,_]

    @Test
    void many_number() {
        assertEquals(4, removeDuplicates(new int[]{1, 1, 1, 2, 2, 3, 4, 4}));
    }

    private int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int previous = nums[0];
        int nextIndex = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != previous) {
                nums[nextIndex] = nums[i];
                previous = nums[i];
                nextIndex++;
            }
        }
        return nextIndex;
    }

}
