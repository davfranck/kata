package leetcode;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ContainsDuplicateTest {

    @Test
    void no_duplicate() {
        assertFalse(containsDuplicate(new int[]{1, 2, 3}));
    }

    @Test
    void with_duplicate() {
        assertTrue(containsDuplicate(new int[]{1, 2, 3, 1}));
    }

    private boolean containsDuplicate(int[] nums) {
        return Arrays.stream(nums).boxed().collect(Collectors.toSet()).size() != nums.length;
    }
}
