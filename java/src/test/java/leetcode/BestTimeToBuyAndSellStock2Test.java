package leetcode;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BestTimeToBuyAndSellStock2Test {

    @Test
    void no_prices() {
        assertEquals(0, maxProfit(new int[0]));
    }

    @Test
    void one_price() {
        assertEquals(0, maxProfit(new int[]{1}));
    }

    @Test
    void two_prices_with_profit() {
        assertEquals(1, maxProfit(new int[]{1, 2}));
    }

    @Test
    void two_prices_no_profit() {
        assertEquals(0, maxProfit(new int[]{2, 1}));
    }

    @Test
    void more() {
        assertEquals(7, maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
    }

    private int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        } else {
            int profit = 0;
            int currentStockPrice = prices[0];
            for (int i = 1; i < prices.length; i++) {
                if (currentStockPrice > prices[i]) {
                    currentStockPrice = prices[i];
                } else {
                    profit += prices[i] - currentStockPrice;
                    currentStockPrice = prices[i];
                }
            }
            return profit;
        }
    }
}
