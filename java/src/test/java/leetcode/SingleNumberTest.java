package leetcode;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

// https://leetcode.com/problems/single-number/
public class SingleNumberTest {

    @Test
    void one_number_in_array() {
        assertThat(new Solution().singleNumber(new int[]{1})).isEqualTo(1);
    }

    @Test
    void three_numbers_in_array_first_is_unique() {
        assertThat(new Solution().singleNumber(new int[]{2, 1, 1})).isEqualTo(2);
    }

    @Test
    void three_numbers_in_array_second_is_unique() {
        assertThat(new Solution().singleNumber(new int[]{1, 2, 1})).isEqualTo(2);
    }

    @Test
    void five_numbers_in_array_third_is_unique() {
        assertThat(new Solution().singleNumber(new int[]{1, 2, 3, 1, 2})).isEqualTo(3);
    }

    private class Solution {
        public int singleNumber(int[] numbers) {
            Arrays.sort(numbers);
            for (int i = 0; i < numbers.length - 1; i = i + 2) {
                if (numbers[i] != numbers[i + 1]) {
                    return numbers[i];
                }
            }
            return numbers[numbers.length - 1];
        }
    }
}
