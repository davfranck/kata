package katalog.marsrover;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

class RoverShould {

    @ParameterizedTest
    @CsvSource({
            "1,3,N",
            "3,4,S"
    })
    void should_be_at_starting_point(int x, int y, String direction) {
        // when
        Rover rover = new Rover(x, y, direction);
        // then
        assertThat(rover.getPosition()).isEqualTo(new Position(x, y));
        assertThat(rover.getDirection()).isEqualTo(Direction.from(direction));
    }

    @ParameterizedTest
    @CsvSource({
            "0,0,N,0,1",
            "0,0,E,1,0",
            "0,1,S,0,0",
            "1,0,W,0,0"
    })
    void move_forward(int initialX, int initialY, String direction, int expectedX, int expectedY) {
        // given
        Rover rover = new Rover(initialX, initialY, direction);
        // when
        Rover movedRover = rover.moveForward();
        // then
        assertThat(movedRover.getPosition()).isEqualTo(new Position(expectedX, expectedY));
    }

    @ParameterizedTest
    @CsvSource({
            "0,1,N,0,0",
            "0,0,S,0,1",
            "1,0,E,0,0",
            "1,0,W,2,0"
    })
    void move_backward(int initialX, int initialY, String direction, int expectedX, int expectedY) {
        // given
        Rover rover = new Rover(initialX, initialY, direction);
        // when
        Rover movedRover = rover.moveBackward();
        // then
        assertThat(movedRover.getPosition()).isEqualTo(new Position(expectedX, expectedY));
    }

    @ParameterizedTest
    @CsvSource({
            "N,E",
            "E,S",
            "S,W",
            "W,N"
    })
    void turn_right(String initialDirection, String expectedDirection) {
        // when
        Rover turnedRover = new Rover(0, 0, initialDirection).turnRight();
        // then
        assertThat(turnedRover.getDirection()).isEqualTo(Direction.from(expectedDirection));
    }

    @ParameterizedTest
    @CsvSource({
            "N,W",
            "W,S",
            "S,E",
            "E,N"
    })
    void turn_left(String initialDirection, String expectedDirection) {
        // when
        Rover turnedRover = new Rover(0, 0, initialDirection).turnLeft();
        // then
        assertThat(turnedRover.getDirection()).isEqualTo(Direction.from(expectedDirection));
    }

    @ParameterizedTest
    @CsvSource({
            "F,2,3,N",
            "B,2,1,N",
            "L,2,2,W",
            "R,2,2,E"
    })
    void execute_one_command(String command, int expectedX, int expectedY, String expectedDirection) {
        // when
        Rover result = new Rover(2, 2, "N").execute(command);
        // then
        assertThat(result.getPosition()).isEqualTo(new Position(expectedX, expectedY));
        assertThat(result.getDirection()).isEqualTo(Direction.from(expectedDirection));
    }

    private static class Rover {

        private final Position position;
        private final Direction direction;

        public Rover(int x, int y, String direction) {
            this(new Position(x, y), Direction.from(direction));
        }

        public Rover(Position position, Direction direction) {
            this.position = position;
            this.direction = direction;
        }

        public Direction getDirection() {
            return direction;
        }

        public Rover moveForward() {
            return new Rover(direction.forward(position), direction);
        }

        public Position getPosition() {
            return position;
        }

        public Rover moveBackward() {
            return new Rover(direction.backward(position), direction);
        }

        public Rover turnRight() {
            return new Rover(position, direction.right());
        }

        public Rover turnLeft() {
            return new Rover(position, direction.left());
        }

        public Rover execute(String commandAsString) {
            return Command.from(commandAsString).execute(this);
        }
    }

    private static class Position {
        public final int x;
        public final int y;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Position position = (Position) o;
            return x == position.x && y == position.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "Position{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

    private enum Direction {
        NORTH("N", 0, 1, "E", "W"),
        EAST("E", 1, 0, "S", "N"),
        SOUTH("S", 0, -1, "W", "E"),
        WEST("W", -1, 0, "N", "S");

        private final String value;
        private final int xForwardModifier;
        private final int yFowardModifier;
        private final String whenTurnRight;
        private final String whenTurnLeft;

        Direction(String value, int xForwardModifier, int yFowardModifier, String whenTurnRight, String whenTurnLeft) {
            this.value = value;
            this.xForwardModifier = xForwardModifier;
            this.yFowardModifier = yFowardModifier;
            this.whenTurnRight = whenTurnRight;
            this.whenTurnLeft = whenTurnLeft;
        }

        public static Direction from(String direction) {
            return Arrays.stream(Direction.values()).filter(d -> d.value.equals(direction)).findFirst().orElseThrow();
        }

        public Position forward(Position position) {
            return new Position(position.x + xForwardModifier, position.y + yFowardModifier);
        }

        public Position backward(Position position) {
            return new Position(position.x - xForwardModifier, position.y - yFowardModifier);
        }

        public Direction right() {
            return Direction.from(whenTurnRight);
        }

        public Direction left() {
            return Direction.from(whenTurnLeft);
        }
    }

    private enum Command {
        FORWARD("F") {
            @Override
            public Rover execute(Rover rover) {
                return rover.moveForward();
            }
        }, BACKWARD("B") {
            @Override
            public Rover execute(Rover rover) {
                return rover.moveBackward();
            }
        }, LEFT("L") {
            @Override
            public Rover execute(Rover rover) {
                return rover.turnLeft();
            }
        }, RIGHT("R") {
            @Override
            public Rover execute(Rover rover) {
                return rover.turnRight();
            }
        };

        private String commandAsString;

        Command(String commandAsString) {
            this.commandAsString = commandAsString;
        }

        public static Command from(String commandAsString) {
            return Arrays.stream(Command.values())
                    .filter(command -> command.commandAsString.equals(commandAsString))
                    .findFirst().orElseThrow();
        }

        public abstract Rover execute(Rover rover);
    }
}
