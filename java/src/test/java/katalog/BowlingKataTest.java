package katalog;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

// https://kata-log.rocks/bowling-game-kata
// https://en.wikipedia.org/wiki/Ten-pin_bowling#Scoring
// http://butunclebob.com/files/downloads/Bowling%20Game%20Kata.ppt
public class BowlingKataTest {

    @Test
    void no_roll_score_is_0() {
        // when
        int score = new Game().score();
        // then
        assertThat(score).isEqualTo(0);
    }

    @Test
    void one_roll_one_hit() {
        // given
        Game game = new Game();
        // when
        game.roll(1);
        //
        assertThat(game.score()).isEqualTo(1);
    }

    @Test
    void three_rolls_one_pin_each() {
        // given
        Game game = new Game();
        // when
        game.roll(1);
        game.roll(1);
        game.roll(1);
        //
        assertThat(game.score()).isEqualTo(3);
    }

    @Test
    void two_rolls_spare() {
        // given
        Game game = new Game();
        // when
        game.roll(1);
        game.roll(9);
        //
        assertThat(game.score()).isEqualTo(10);
    }

    @Test
    void first_frame_is_spare_second_frame_with_zero_roll() {
        // given
        Game game = new Game();
        // when
        game.roll(1);
        game.roll(9);
        game.roll(0);
        // then
        assertThat(game.score()).isEqualTo(1 + 9 + (0 * 2));
    }

    // Spare
    // A spare is when the player knocks down all 10 pins in two rolls.
    // The bonus for that frame is the number of pins knocked down by the next roll.
    @Test
    void first_frame_is_spare_second_frame_with_one_roll() {
        // given
        Game game = new Game();
        // when
        game.roll(1);
        game.roll(9);
        game.roll(1);
        // then
        assertThat(game.score()).isEqualTo(1 + 9 + (1 * 2));
    }

    // Strike
    // A strike is when the player knocks down all 10 pins on his first roll.
    // The frame is then completed with a single roll. The bonus for that frame is the value of the next two rolls.
    @Test
    void first_frame_is_strike() {
        // given
        Game game = new Game();
        // when
        game.roll(10);
        game.roll(1);
        game.roll(2);
        // then
        assertThat(game.score()).isEqualTo(10 + (1 * 2) + (2 * 2));
    }

    private class Game {

        private List<Frame> frames = new ArrayList<>();

        public Game() {
            frames.add(new Frame());
        }

        public int score() {
            int score = 0;
            for (int i = 0; i < frames.size(); i++) {
                score += currentFrameScore(i);
            }
            return score;
        }

        private int currentFrameScore(int i) {
            int score = 0;
            Frame currentFrame = frames.get(i);
            int frameScore = currentFrame.score();
            boolean hasAnotherFrame = i + 1 < frames.size();
            if (hasAnotherFrame && currentFrame.isSpare()) {
                score += frameScore + frames.get(i + 1).firstRollScore();
            } else if (hasAnotherFrame && currentFrame.isStrike()) {
                score += frameScore + frames.get(i + 1).score();
            } else {
                score += frameScore;
            }
            return score;
        }

        public void roll(int nbPins) {
            Frame current = frames.get(frames.size() - 1);
            if (current.isFinished() || current.isStrike()) {
                current = new Frame();
                frames.add(current);
            }
            current.addRoll(nbPins);
        }
    }

    private class Frame {

        private List<Integer> rolls = new ArrayList<>();

        public boolean isFinished() {
            return this.rolls.size() == 2;
        }

        public void addRoll(int nbPins) {
            rolls.add(nbPins);
        }

        public int score() {
            return rolls.stream().mapToInt(value -> value).sum();
        }

        public boolean isSpare() {
            return this.rolls.size() == 2 && this.score() == 10;
        }

        public int firstRollScore() {
            return this.rolls.get(0);
        }

        public boolean isStrike() {
            return this.rolls.size() == 1 && this.rolls.get(0) == 10;
        }
    }
}
