package katalog.tictactoe;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class MoveTest {

    @Test
    void invalidMove() {
        Assertions.assertThat(Move.of("invalid move")).isEmpty();
    }

    @Test
    void outOfBoundMove() {
        Assertions.assertThat(Move.of("3-0")).isEmpty();
    }
}