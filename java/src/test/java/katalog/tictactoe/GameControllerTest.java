package katalog.tictactoe;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

// https://kata-log.rocks/tic-tac-toe-kata
@ExtendWith(MockitoExtension.class)
class GameControllerTest {

    @Mock
    private Display display;

    @Mock
    private Board board;

    private GameController gameController;
    private List<Player> players;

    @BeforeEach
    void setUp() {
        players = List.of(new Player("player1"), new Player("player2"));
        gameController = new GameController(board, display, players);
    }

    @Test
    void firstPlayerTakeField() {
        // given
        Move move = new Move(0, 0);
        // when
        gameController.play(move);
        // then
        then(board).should().takeField(players.get(0), move);
        then(display).should().render(board, players.get(0));
        then(display).should().displayPlayerTurn(players.get(1).name);
    }

    @Test
    void onePlayerTakeOneAlreadyTakenField() {
        // given
        Move move = new Move(0, 0);
        given(board.isTaken(move)).willReturn(true);
        // when
        gameController.play(move);
        // then
        then(display).should().displayErrorMessage("field already occupied");
    }

    @Test
    void onePlayerWins() {
        // given
        Move move = new Move(0, 0);
        given(board.hasAWinner()).willReturn(true);
        // when
        gameController.play(move);
        // then
        then(board).should(times(1)).takeField(players.get(0), move);
        then(display).should().render(board, players.get(0));
        then(display).should().displayWinner("player1");
        then(display).should(never()).displayPlayerTurn(anyString());
    }

    @Test
    void drawWhenNoMoreFieldAvailable() {
        // given
        Move move = new Move(0, 0);
        given(board.isFull()).willReturn(true);
        // when
        gameController.play(move);
        // then
        then(board).should(times(1)).takeField(players.get(0), move);
        then(display).should().render(board, players.get(0));
        then(display).should().displayDraw();
    }

    @Test
    void refuseMoveWhenGameIsOver() {
        // given
        Move move = new Move(0, 0);
        given(board.isFull()).willReturn(true);
        Move refusedMove = new Move(1, 1);
        // when
        gameController.play(move);
        gameController.play(refusedMove);
        // then
        then(display).should().displayErrorMessage("Game is over");
    }
}
