package katalog.tictactoe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
class ConsoleDisplayTest {

    @Mock
    private OutputWriter outputWriter;

    @Mock
    private Board board;

    @Test
    void renderEmptyBoard() {
        ConsoleDisplay displayImpl = new ConsoleDisplay(outputWriter);
        given(board.rows()).willReturn(List.of(
                new Row(0, null, null, null),
                new Row(1, null, null, null),
                new Row(2, null, null, null)
        ));
        displayImpl.render(board, new Player("::not used::"));
        then(outputWriter).should().println("    O 1 2");
        then(outputWriter).should().println("    v v v");
        then(outputWriter).should().println("0 > - - -");
        then(outputWriter).should().println("1 > - - -");
        then(outputWriter).should().println("2 > - - -");
        then(outputWriter).should().println("");
    }

    @Test
    void renderANonEmptyBoard() {
        Display displayImpl = new ConsoleDisplay(outputWriter);
        Player player1 = new Player("player1");
        Player player2 = new Player("player2");
        given(board.rows()).willReturn(List.of(
                new Row(0, player1, null, null),
                new Row(1, player1, player2, null),
                new Row(2, null, player2, null)
        ));

        displayImpl.render(board, player1);

        then(outputWriter).should().println("    O 1 2");
        then(outputWriter).should().println("    v v v");
        then(outputWriter).should().println("0 > O - -");
        then(outputWriter).should().println("1 > O X -");
        then(outputWriter).should().println("2 > - X -");
    }
}
