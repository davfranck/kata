package katalog.tictactoe;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.Scanner;

class KeyboardInputTest {

    @Test
    void validMove() {
        Optional<String> move = new KeyboardInput(new Scanner("0-1")).tryMove();
        Assertions.assertThat(move).isPresent().hasValue("0-1");
    }

    @Test
    void manyMoves() {
        KeyboardInput keyboardInput = new KeyboardInput(new Scanner("0-1" + System.lineSeparator() + "0-0" + System.lineSeparator()));

        keyboardInput.tryMove();
        Optional<String> move = keyboardInput.tryMove();

        Assertions.assertThat(move).isPresent().hasValue("0-0");
    }

    @Test
    void invalidMove() {
        Optional<String> move = new KeyboardInput(new Scanner("")).tryMove();
        Assertions.assertThat(move).isEmpty();
    }

    @Test
    void emptyMove() {
        Optional<String> move = new KeyboardInput(new Scanner(System.lineSeparator())).tryMove();
        Assertions.assertThat(move).isEmpty();
    }
}