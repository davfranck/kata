package katalog.tictactoe;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.atIndex;

class BoardImplTest {

    @Test
    void fieldIsNotTaken() {
        // given
        BoardImpl board = new BoardImpl();
        // when
        boolean isTaken = board.isTaken(new Move(0, 0));
        // then
        assertThat(isTaken).isFalse();
    }

    @Test
    void fieldIsTaken() {
        // given
        BoardImpl board = new BoardImpl();
        board.takeField(new Player("player1"), new Move(0, 0));
        // when
        boolean isTaken = board.isTaken(new Move(0, 0));
        // then
        assertThat(isTaken).isTrue();
    }

    @Test
    void hasAWinnerWhenThreeFieldsInTheSameColumn() {
        // given
        BoardImpl board = new BoardImpl();
        Player player1 = new Player("player1");
        board.takeField(player1, new Move(0, 0));
        board.takeField(player1, new Move(1, 0));
        board.takeField(player1, new Move(2, 0));
        // when
        boolean isTaken = board.hasAWinner();
        // then
        assertThat(isTaken).isTrue();
    }

    @Test
    void hasAWinnerWhenThreeFieldsInTheSameRow() {
        // given
        BoardImpl board = new BoardImpl();
        Player player1 = new Player("player1");
        board.takeField(player1, new Move(0, 0));
        board.takeField(player1, new Move(0, 1));
        board.takeField(player1, new Move(0, 2));
        // when
        boolean isTaken = board.hasAWinner();
        // then
        assertThat(isTaken).isTrue();
    }

    @Test
    void hasAWinnerWhenThreeFieldsInLeftToRightDiagonal() {
        // given
        BoardImpl board = new BoardImpl();
        Player player1 = new Player("player1");
        board.takeField(player1, new Move(0, 0));
        board.takeField(player1, new Move(1, 1));
        board.takeField(player1, new Move(2, 2));
        // when
        boolean isTaken = board.hasAWinner();
        // then
        assertThat(isTaken).isTrue();
    }

    @Test
    void hasAWinnerWhenThreeFieldsInRightToLeftDiagonal() {
        // given
        BoardImpl board = new BoardImpl();
        Player player1 = new Player("player1");
        board.takeField(player1, new Move(0, 2));
        board.takeField(player1, new Move(1, 1));
        board.takeField(player1, new Move(2, 0));
        // when
        boolean isTaken = board.hasAWinner();
        // then
        assertThat(isTaken).isTrue();
    }

    @Test
    void isFull() {
        BoardImpl board = new BoardImpl();
        Player thePlayerDoesNotMatter = new Player("::the player does not matter::");
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board.takeField(thePlayerDoesNotMatter, new Move(row, col));
            }
        }
        assertThat(board.isFull()).isTrue();
    }

    @Test
    void emptyRows() {
        BoardImpl board = new BoardImpl();
        assertThat(board.rows())
                .isNotEmpty()
                .allSatisfy(row -> {
                    assertThat(row.playerAt(0)).isEmpty();
                    assertThat(row.playerAt(1)).isEmpty();
                    assertThat(row.playerAt(2)).isEmpty();
                });
    }

    @Test
    void nonEmptyRows() {
        BoardImpl board = new BoardImpl();
        Player player1 = new Player("player1");
        board.takeField(player1, new Move(0, 0));
        Player player2 = new Player("player2");
        board.takeField(player2, new Move(0, 1));
        assertThat(board.rows())
                .satisfies(
                        firstRow -> {
                            assertThat(firstRow.playerAt(0)).hasValue(player1);
                            assertThat(firstRow.playerAt(1)).hasValue(player2);
                            assertThat(firstRow.playerAt(2)).isEmpty();
                        },
                        atIndex(0)
                ).satisfies(
                        secondRow -> {
                            assertThat(secondRow.playerAt(0)).isEmpty();
                            assertThat(secondRow.playerAt(1)).isEmpty();
                            assertThat(secondRow.playerAt(2)).isEmpty();
                        },
                        atIndex(1)
                ).
                satisfies(
                        thirdRow -> {
                            assertThat(thirdRow.playerAt(0)).isEmpty();
                            assertThat(thirdRow.playerAt(1)).isEmpty();
                            assertThat(thirdRow.playerAt(2)).isEmpty();
                        },
                        atIndex(2)
                );
    }
}