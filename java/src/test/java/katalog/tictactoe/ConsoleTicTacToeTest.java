package katalog.tictactoe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

@ExtendWith(MockitoExtension.class)
class ConsoleTicTacToeTest {

    @Mock
    private Display display;

    @Test
    void fullGame() {
        Board board = new BoardImpl();
        List<Player> players = List.of(new Player("player1"), new Player("player2"));
        GameController controller = new GameController(board, display, players);
        List<String> moves = List.of("0-0", "0-1", "1-0", "0-2", "2-0");
        KeyboardInput input = new KeyboardInput(new Scanner(lines(moves)));

        InOrder inOrder = Mockito.inOrder(display);

        new ConsoleTicTacToe(display, controller, input).playGame();

        inOrder.verify(display).displayPlayerTurn("player1");
        inOrder.verify(display).displayPlayerTurn("player2");
        inOrder.verify(display).displayPlayerTurn("player1");
        inOrder.verify(display).displayPlayerTurn("player2");
        inOrder.verify(display).displayPlayerTurn("player1");
        inOrder.verify(display).displayWinner("player1");
    }

    private static String lines(List<String> moves) {
        return moves.stream().collect(Collectors.joining(System.lineSeparator()));
    }
}