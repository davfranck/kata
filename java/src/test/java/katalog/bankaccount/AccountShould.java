package katalog.bankaccount;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
class AccountShould {

    @Mock
    AccountTransactionRepository accountTransactionRepository;

    @Mock
    TimeProvider timeProvider;

    @Test
    void add_a_transaction_when_deposit() {
        // given
        LocalDate depositDate = LocalDate.now();
        given(timeProvider.now()).willReturn(depositDate);
        // when
        new Account(accountTransactionRepository, timeProvider).deposit(675);
        // then
        then(accountTransactionRepository).should().add(675, depositDate);
    }

    @Test
    void add_a_transaction_when_withdraw() {
        // given
        LocalDate withdrawDate = LocalDate.now();
        given(timeProvider.now()).willReturn(withdrawDate);
        // when
        new Account(accountTransactionRepository, timeProvider).withdraw(458);
        // then
        then(accountTransactionRepository).should().add(-458, withdrawDate);
    }

    @Test
    void print_satement_when_one_deposit() {
        // given
        Account account = new Account(accountTransactionRepository, timeProvider);
        given(accountTransactionRepository.all()).willReturn(List.of(
                new Transaction(500, LocalDate.of(2015, 12, 24))
        ));
        // when
        String result = account.printStatement();
        // then
        String expected = String.join(System.lineSeparator(),
                "Date        Amount  Balance",
                "24.12.2015 +500 500");
        assertThat(result).isEqualTo(expected);
    }
}
