package katalog.bankaccount;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

class BankAccountAcceptanceTest {

    @Test
    void should_print_statement_when_one_deposit_and_one_withdraw() {
        // given
        Account account = new Account(new InMemoryAccountTransactionRepository(), new FakeTimeProvider(List.of(
                LocalDate.of(2015, 12, 24),
                LocalDate.of(2016, 8, 23)
        )));
        account.deposit(500);
        account.withdraw(100);
        // when
        String result = account.printStatement();
        // then
        String expected = String.join(System.lineSeparator(),
                "Date        Amount  Balance",
                "24.12.2015 +500 500",
                "23.08.2016 -100 400");
        Assertions.assertThat(result).isEqualTo(expected);
    }
}
