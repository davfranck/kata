package katalog.bankaccount;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InMemoryAccountTransactionRepository implements AccountTransactionRepository {

    private List<Transaction> transactions = new ArrayList<>();

    @Override
    public void add(int amount, LocalDate transactionDate) {
        this.transactions.add(new Transaction(amount, transactionDate));
    }

    @Override
    public List<Transaction> all() {
        return List.copyOf(transactions);
    }

}
