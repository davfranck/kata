package katalog.bankaccount;

import java.time.LocalDate;
import java.util.List;

public class FakeTimeProvider implements TimeProvider {

    private final List<LocalDate> dates;
    private int currentIndex = 0;

    public FakeTimeProvider(List<LocalDate> dates) {
        this.dates = dates;
    }

    @Override
    public LocalDate now() {
        LocalDate localDate = dates.get(currentIndex);
        currentIndex++;
        return localDate;
    }
}
