package yatze;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class YatzeTest {

    @Test
    void should_sum_dice_for_chance() {
        List<Integer> roll = List.of(1, 1, 3, 3, 6);
        Assertions.assertThat(calculateYatzeResult(roll, "chance")).isEqualTo(14);
    }

    @Test
    void should_sum_dice_for_chance_2() {
        List<Integer> roll = List.of(4,5,5,6,1);
        Assertions.assertThat(calculateYatzeResult(roll, "chance")).isEqualTo(21);
    }

    @Test
    void should_give_50_points_for_yatze_when_identical_number_in_roll() {
        List<Integer> roll = List.of(1, 1, 1, 1, 1);
        String category = "yatze";
        Assertions.assertThat(calculateYatzeResult(roll, category)).isEqualTo(50);
    }

    @Test
    void should_give_0_points_for_yatze_when_not_identical_number_in_roll() {
        List<Integer> roll = List.of(1, 1, 1, 1, 2);
        String category = "yatze";
        Assertions.assertThat(calculateYatzeResult(roll, category)).isEqualTo(0);
    }

    @Test
    void should_give_number_of_1_points_for_ones() {
        List<Integer> roll = List.of(1,1,2,4,4);
        String category = "ones";
        Assertions.assertThat(calculateYatzeResult(roll, category)).isEqualTo(2);
    }


    @Test
    void should_give_number_of_1_points_for_ones_2() {
        List<Integer> roll = List.of(1,1,2,4,1);
        String category = "ones";
        Assertions.assertThat(calculateYatzeResult(roll, category)).isEqualTo(3);
    }


    private int calculateYatzeResult(List<Integer> roll, String category) {
        if ("yatze".equals(category)){
            return calculateForYatze(roll);
        } else if ("ones".equals(category)) {
            return roll.stream().filter(value -> value == 1).reduce(Integer::sum).get();
        }
        return calculateForChance(roll);
    }

    private Integer calculateForChance(List<Integer> roll) {
        return roll.stream().reduce(Integer::sum).get();
    }

    private int calculateForYatze(List<Integer> roll) {
        if(isAllDiceHaveSameValue(roll)) {
            return 50;
        }
        return 0;
    }

    private boolean isAllDiceHaveSameValue(List<Integer> roll) {
        return new HashSet<>(roll).size() == 1;
    }
}
