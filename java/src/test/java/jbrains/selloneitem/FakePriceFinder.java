package jbrains.selloneitem;

import java.util.Optional;

public class FakePriceFinder implements IPriceFinder {
    @Override
    public Optional<Integer> findPriceFor(String barcode) {
        if (barcode.equals("1$_VALID_CODE")) {
            return Optional.of(1);
        }
        return Optional.empty();
    }
}
