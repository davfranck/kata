package jbrains.selloneitem;

public class FakeDisplay implements IDisplay {

    private String message;

    public String getOutput() {
        return message;
    }

    @Override
    public void print(String message) {
        this.message = message;
    }
}
