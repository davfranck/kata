package jbrains.selloneitem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BarcodeEventHandlerTest {

    private FakeDisplay fakeDisplay = new FakeDisplay();
    private FakePriceFinder priceFinder = new FakePriceFinder();
    private BarcodeEventHandler barcodeEventHandler;

    @BeforeEach
    void setUp() {
        barcodeEventHandler = new BarcodeEventHandler(fakeDisplay, priceFinder);
    }

    @Test
    void displays_error_message_when_unknown_code() {
        // when
        barcodeEventHandler.onBarcode("NON VALID CODE" + System.lineSeparator());
        // then 
        assertEquals("UNKNOWN CODE", fakeDisplay.getOutput());
    }

    @Test
    void display_amount_when_barcode_is_known() {
        new BarcodeEventHandler(fakeDisplay, priceFinder).onBarcode("1$_VALID_CODE");
        assertEquals("1$", fakeDisplay.getOutput());
    }

    @Test
    void displays_bad_format_when_input_is_empty() {
        new BarcodeEventHandler(fakeDisplay, priceFinder).onBarcode("");
        assertEquals("BAD INPUT", fakeDisplay.getOutput());
    }

    @Test
    void displays_bad_format_when_input_is_null() {
        new BarcodeEventHandler(fakeDisplay, priceFinder).onBarcode(null);
        assertEquals("BAD INPUT", fakeDisplay.getOutput());
    }
}
