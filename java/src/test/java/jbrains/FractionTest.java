package jbrains;

import org.junit.jupiter.api.Test;

import static jbrains.Fraction.from;
import static org.assertj.core.api.Assertions.assertThat;

class FractionTest {

    @Test
    public void basic_integers() {
        assertThat(from(0).add(from(0))).isEqualTo(from(0));
        assertThat(from(1).add(from(0))).isEqualTo(from(1));
        assertThat(from(1).add(from(2))).isEqualTo(from(3));
        assertThat(from(4).add(from(9))).isEqualTo(from(13));
    }

    @Test
    public void same_denominator() {
        assertThat(from(1, 5).add(from(1, 5))).isEqualTo(from(2, 5));
    }

    @Test
    public void different_denominator_without_reduction() {
        assertThat(from(1, 2).add(from(2, 3))).isEqualTo(from(7, 6));
    }

    @Test
    public void simplification_same_denominator_lower_than_numerator() {
        assertThat(from(1, 2).add(from(3, 2))).isEqualTo(from(2, 1));
    }

    @Test
    public void simplification_same_denominator_bigger_than_numerator() {
        assertThat(from(1, 8).add(from(1, 8))).isEqualTo(from(1, 4));
    }

    @Test
    public void not_same_denominator() {
        assertThat(from(1, 4).add(from(5, 6))).isEqualTo(from(13, 12));
        assertThat(from(3, 12).add(from(3, 9))).isEqualTo(from(7, 12));
    }

    @Test
    public void negative() {
        assertThat(from(-1).add(from(1))).isEqualTo(from(0));
        assertThat(from(-1).add(from(-1))).isEqualTo(from(-2));
        assertThat(from(-1, 2).add(from(-1, 2))).isEqualTo(from(-1));
    }


}