package jbrains;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PgcdTest {

    @Test
    public void prime_numbers_pgcd_is_one() {
        assertThat(new Pgcd(5, 3).value()).isEqualTo(1);
        assertThat(new Pgcd(7, 3).value()).isEqualTo(1);
    }

    @Test
    public void first_bigger_than_second() {
        assertThat(new Pgcd(4, 2).value()).isEqualTo(2);
        assertThat(new Pgcd(8, 4).value()).isEqualTo(4);
    }

    @Test
    public void second_bigger_than_first() {
        assertThat(new Pgcd(2, 4).value()).isEqualTo(2);
        assertThat(new Pgcd(4, 8).value()).isEqualTo(4);
    }

}