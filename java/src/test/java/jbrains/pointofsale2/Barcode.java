package jbrains.pointofsale2;

import java.util.Optional;

public class Barcode {
    public final String barcode;

    public Barcode(String barcode) {
        this.barcode = barcode;
    }

    public static Optional<Barcode> from(String barcode) {
        if ("".equals(barcode)) {
            return Optional.empty();
        }
        return Optional.of(new Barcode(barcode));
    }
}
