package jbrains.pointofsale2;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class SellMultipleItemsTest {
    @Test
    void many_valid_barcodes() {
        Display display = new Display();
        Map<String, String> priceByBarcode = Map.of(
                "12345", "$20.00",
                "23456", "$1.10",
                "34567", "$2.35"
        );
        Map<String, Integer> intPriceByBarcode = Map.of(
                "12345", 2000,
                "23456", 110,
                "34567", 235
        );
        Sale sale = new Sale(display, intPriceByBarcode);
        sale.onBarcode("12345");
        sale.onBarcode("23456");
        sale.onBarcode("34567");
        sale.onTotal();
        Assertions.assertThat("$23.45").isEqualTo(display.getText());
    }
}
