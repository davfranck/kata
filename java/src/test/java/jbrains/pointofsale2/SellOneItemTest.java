package jbrains.pointofsale2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Map;

class SellOneItemTest {

    @ParameterizedTest
    @CsvSource({
            "12345,$1.25",
            "54321,$2.45",
            "98765,$4.25"})
    void oneProductAvailableInCatalog(String barcode, String displayedPrice) {
        Display display = new Display();
        Map<String, String> priceByBarcode = Map.of(
                "12345", "$1.25",
                "54321", "$2.45",
                "98765", "$4.25");
        Map<String, Integer> intPriceByBarcode = Map.of(
                "12345", 125,
                "54321", 245,
                "98765", 425
        );
        Sale sale = new Sale(display, intPriceByBarcode);
        sale.onBarcode(barcode);
        Assertions.assertEquals(displayedPrice, display.getText());
    }

    @Test
    void oneProductNotFound() {
        Display display = new Display();
        Sale sale = new Sale(display, Map.of());
        sale.onBarcode("not found barcode");
        Assertions.assertEquals("Product not found", display.getText());
    }

    @Test
    void emptyBarcode() {
        Display display = new Display();
        Sale sale = new Sale(display, Map.of());
        sale.onBarcode("");
        Assertions.assertEquals("Empty barcode", display.getText());
    }

}
