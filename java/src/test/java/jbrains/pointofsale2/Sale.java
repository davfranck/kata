package jbrains.pointofsale2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class Sale {
    private final Display display;
    private final Map<String, Integer> priceByBarcode;
    private final List<Integer> cart = new ArrayList<>();

    public Sale(Display display, Map<String, Integer> priceByBarcode) {
        this.display = display;
        this.priceByBarcode = priceByBarcode;
    }

    public void onBarcode(String inputBarcode) {
        Optional<Barcode> barcode = Barcode.from(inputBarcode);
        barcode.ifPresentOrElse(bc -> {
            Integer price = priceByBarcode.get(bc.barcode);
            if (price != null) {
                cart.add(price);
                display.displayPrice(price);
            } else {
                display.displayProductNotFound();
            }
        }, display::displayEmptyBarcode);
    }

    public void onTotal() {
        int sum = cart.stream().mapToInt(Integer::intValue).sum();
        display.displayPrice(sum);
    }
}
