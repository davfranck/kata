package jbrains.pointofsale2;

import java.util.Locale;

class Display {

    private String text;

    public String getText() {
        return text;
    }

    public void displayPrice(String price) {
        text = price;
    }

    public void displayProductNotFound() {
        text = "Product not found";
    }

    public void displayEmptyBarcode() {
        text = "Empty barcode";
    }

    public void displayPrice(Integer intPrice) {
        text = format(intPrice);
    }

    private String format(Integer intPrice) {
        return String.format(Locale.ENGLISH, "$%.2f", (double) intPrice / 100);
    }
}
