package jbrains.pointofsale2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class DisplayTest {

    @ParameterizedTest
    @CsvSource({
            "100,$1.00",
            "123,$1.23",
            "1234,$12.34",
            "0,$0.00"
    })
    void display_int_price(Integer price, String expected) {
        Display display = new Display();
        display.displayPrice(price);
        Assertions.assertEquals(expected, display.getText());
    }

}