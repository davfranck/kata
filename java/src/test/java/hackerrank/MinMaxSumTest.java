package hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinMaxSumTest {

    public static void main(String[] args) {
        miniMaxSum(List.of(1, 3, 5, 7, 9));
    }

    public static void miniMaxSum(List<Integer> arr) {
        ArrayList<Integer> newArr = new ArrayList<>(arr);
        newArr.sort(Integer::compareTo);
        long min = newArr.subList(0, arr.size() - 1).stream().mapToLong(Integer::intValue).sum();
        Collections.reverse(newArr);
        long max = newArr.subList(0, arr.size() - 1).stream().mapToLong(Integer::intValue).sum();
        System.out.println(min + " " + max);
    }
}
