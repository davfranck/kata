package hackerrank;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlusMinusTest {

    @Test
    void onlyPositive() {
        Ratios ratios = plusMinusResult(List.of(1));
        assertEquals(List.of("1.000000", "0.000000", "0.000000"), ratios.toOutput());
    }

    @Test
    void onlyNegative() {
        Ratios ratios = plusMinusResult(List.of(-1));
        assertEquals(List.of("0.000000", "1.000000", "0.000000"), ratios.toOutput());
    }

    @Test
    void emptyList() {
        Ratios ratios = plusMinusResult(List.of());
        assertEquals(List.of("0.000000", "0.000000", "0.000000"), ratios.toOutput());
    }

    @Test
    void onlyZeroValue() {
        Ratios ratios = plusMinusResult(List.of(0));
        assertEquals(List.of("0.000000", "0.000000", "1.000000"), ratios.toOutput());
    }

    @Test
    void sameAmountOfNegativeAndPositiveNumbers() {
        Ratios ratios = plusMinusResult(List.of(1, -2, 3, -4));
        assertEquals(List.of("0.500000", "0.500000", "0.000000"), ratios.toOutput());
    }

    @Test
    void sameAmountOfAllTypes() {
        Ratios ratios = plusMinusResult(List.of(1, -2, 0, 3, 0, -4));
        assertEquals(List.of("0.333333", "0.333333", "0.333333"), ratios.toOutput());
    }


    private Ratios plusMinusResult(List<Integer> arr) {
        Ratios ratios = new Ratios();
        arr.forEach(num -> {
            if (num > 0) {
                ratios.incPositive();
            } else if (num < 0) {
                ratios.incNegative();
            } else {
                ratios.incZeroValue();
            }
        });
        return ratios;
    }

    private static class Ratios {
        private int nbPositive;
        private int nbNegative;
        private int nbZeroValue;

        public void incPositive() {
            nbPositive++;
        }

        public void incNegative() {
            nbNegative++;
        }

        public void incZeroValue() {
            nbZeroValue++;
        }

        public List<String> toOutput() {
            return List.of(getPositiveRatio(), getNegativeRatio(), getZeroValueRatio());
        }

        private String getPositiveRatio() {
            double positiveRatio = ratio(nbPositive);
            return format(positiveRatio);
        }

        private String getNegativeRatio() {
            double negativeRatio = ratio(nbNegative);
            return format(negativeRatio);
        }

        private String getZeroValueRatio() {
            double zeroValueRatio = ratio(nbZeroValue);
            return format(zeroValueRatio);
        }

        private double ratio(int numerator) {
            int nbNumbers = nbNumbers();
            if (nbNumbers == 0) {
                return 0;
            }
            return (double) numerator / nbNumbers;
        }

        private int nbNumbers() {
            return nbPositive + nbNegative + nbZeroValue;
        }

        private String format(double negativeRatio) {
            return String.format(Locale.ENGLISH, "%.6f", negativeRatio);
        }
    }
}
