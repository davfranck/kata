package sandbox;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.*;

public class StreamTest {

    @Test
    void lazy() {
        Stream.of("a", "b", "c", "d")
                .map(letter -> {
                    System.out.println("ici :" + letter);
                    return letter;
                })
                .map(letter -> {
                    System.out.println("la : " + letter);
                    return letter;
                })
                .collect(Collectors.toList());
    }
}
