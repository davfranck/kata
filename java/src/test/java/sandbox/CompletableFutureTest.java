package sandbox;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;

import static java.util.stream.Collectors.*;

public class CompletableFutureTest {

    @Test
    public void tout_dans_le_pipeline() {
        long start = System.currentTimeMillis();
        List<CompletableFuture<Integer>> intFutures = Stream.of(1, 2, 3, 4)
                .map(input -> CompletableFuture.supplyAsync(() -> computeSomethingLong(input)))
                .map(future -> future.thenApply(input -> input * 2))
                .map(future -> future.thenCompose(computed -> CompletableFuture.supplyAsync(() -> computeSomethingLong(computed))))
                .collect(toList());
        intFutures.stream()
                .map(CompletableFuture::join)
                .forEach(result -> System.out.println("résultat : " + result));
        long end = System.currentTimeMillis();
        System.out.println("Temps total : " + (end - start));
    }

    @Test
    public void apply_dans_future() {
        long start = System.currentTimeMillis();
        List<CompletableFuture<Integer>> intFutures = Stream.of(1, 2, 3, 4)
                .map(input -> CompletableFuture.supplyAsync(() -> computeSomethingLong(input))
                        .thenApply(computed -> computed * 2)
                        .thenCompose(computed -> CompletableFuture.supplyAsync(() -> computeSomethingLong(computed)))
                        // ici j'ai accès à input, ce n'est pas le cas dans la version avec le stream
                        .thenCompose(computed -> CompletableFuture.supplyAsync(() -> identity(input, computed))))
                .collect(toList());
        intFutures.stream()
                .map(CompletableFuture::join)
                .forEach(result -> System.out.println("résultat : " + result));
        long end = System.currentTimeMillis();
        System.out.println("Temps total : " + (end - start));
    }

    public Integer computeSomethingLong(Integer input) {
        System.out.println("Calcul long de : " + input);
        delay();
        return input + 1;
    }

    public Integer identity(Integer number, Integer toreturn) {
        System.out.println("identité : " + number);
        delay();
        return toreturn;
    }

    private void delay() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
    }

}
