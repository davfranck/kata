package sandbox;

import org.junit.jupiter.api.Test;

import java.util.stream.*;

public class LambdaTest {

    @Test
    void sandbox() {
        Stream.iterate(new int[]{0, 1}, ints -> new int[]{ints[1], ints[0] + ints[1]})
                .limit(20)
                .forEach(ints -> System.out.println("ints = " + ints[0] + "-" + ints[1]));
        Stream.iterate(0, value -> value + 1)
                .limit(10)
                .collect(Collectors.toList());
    }
}
