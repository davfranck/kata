package sandbox;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.*;
import java.util.concurrent.*;

import static org.assertj.core.api.Assertions.assertThat;

// https://github.com/java-manning/modern-java/tree/master/ModernJavaInAction-master/src/main/java/modernjavainaction/chap15
class AsyncTest {

    @Nested
    class FutureWithExecutorTest {

        @Test
        public void futureWithExecutor() throws ExecutionException, InterruptedException {
            int x = 1337;

            Instant start = Instant.now();
            ExecutorService executorService = Executors.newFixedThreadPool(2);
            // les 2 threads sont lancés
            Future<Integer> y = executorService.submit(() -> f1(x));
            Future<Integer> z = executorService.submit(() -> g1(x));
            // puis on récupère le résultat
            assertThat(y.get() + z.get()).isEqualTo(1337 + 2 + 1337 + 3);
            Instant end = Instant.now();
            System.out.println(Duration.between(start, end));
            executorService.shutdown();
        }

    }

    @Nested
    class CompletableFutureTest {

        @Test
        void completableFuture() throws ExecutionException, InterruptedException {
            assertThat(f2(2).get() + g2(1).get()).isEqualTo(6);
        }

        @Test
        void completableWithExecutor() throws ExecutionException, InterruptedException {
            ExecutorService executorService = Executors.newFixedThreadPool(10);
            int x = 1;
            CompletableFuture<Integer> a = new CompletableFuture<>();
            CompletableFuture<Integer> b = new CompletableFuture<>();
            // planifie le calcul quand a et b auront terminé - pas de get() donc non bloquant
            CompletableFuture<Integer> c = a.thenCombine(b, (y, z) -> y + z);
            executorService.submit(() -> a.complete(f1(x)));
            executorService.submit(() -> b.complete(g1(x)));

            System.out.println(c.get());
            executorService.shutdown();
        }
    }

    @Nested
    class ScheduleTest {
        @Test
        void schedule() {
            ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

            work1();
            scheduledExecutorService.schedule(this::work2, 10, TimeUnit.SECONDS);

            scheduledExecutorService.shutdown();
            System.out.println("game over");
        }

        public void work1() {
            System.out.println("Hello from Work1!");
        }

        public void work2() {
            System.out.println("Hello from Work2!");
        }
    }

    public static int g1(int x) {
        return x + 2;
    }

    public static int f1(int x) {
        return x + 3;
    }

    public static Future<Integer> f2(int x) {
        return new CompletableFuture<Integer>().completeAsync(() -> Integer.valueOf(x * 2));
    }

    public static Future<Integer> g2(int x) {
        return new CompletableFuture<Integer>().completeAsync(() -> Integer.valueOf(x + 1));
    }
}
