package name.lemerdy.eric;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

class InvestPlanTest {

    @Test
    void approval_1() {
        // given
        String input = "12345" + // ignoré
                System.lineSeparator() + "55" +
                System.lineSeparator() + "1" +
                System.lineSeparator() + "2" +
                System.lineSeparator() + "3" +
                System.lineSeparator() + "4" +
                System.lineSeparator() + "5" +
                System.lineSeparator() + "6" +
                System.lineSeparator() + "7" +
                System.lineSeparator() + "8" +
                System.lineSeparator() + "9" +
                System.lineSeparator() + "10" +
                System.lineSeparator() + "11" +
                System.lineSeparator() + "12";
        // when
        String output = InvestPlan.input(input).output();
        // then
        Approvals.verify(output);
    }

    @Test
    void approval_2() {
        // given
        String input = "12345" + // ignoré
                System.lineSeparator() + "-100" +
                System.lineSeparator() + "-1" +
                System.lineSeparator() + "-2" +
                System.lineSeparator() + "-3" +
                System.lineSeparator() + "-4" +
                System.lineSeparator() + "-5" +
                System.lineSeparator() + "-6" +
                System.lineSeparator() + "-7" +
                System.lineSeparator() + "-8" +
                System.lineSeparator() + "-9" +
                System.lineSeparator() + "-10" +
                System.lineSeparator() + "-11" +
                System.lineSeparator() + "-12";
        // when
        String output = InvestPlan.input(input).output();
        // then
        Approvals.verify(output);
    }

    @Test
    void approval_3() {
        // given
        String iteration = IntStream.range(1, 13)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(System.lineSeparator()));
        String ignored = "999";
        String firstAmount = "666";
        String input = String.join(System.lineSeparator(), ignored, firstAmount, iteration, firstAmount, iteration);
        // when
        String output = InvestPlan.input(input).output();
        // then
        Approvals.verify(output);
    }

    /**
     * This test is added in order to test the synchronized block.
     */
    @Test
    void parallel_executions() {
        // given
        String iteration = IntStream.range(1, 13)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(System.lineSeparator()));
        String ignored = "999";
        String firstAmount = "666";
        String input = String.join(System.lineSeparator(), ignored, firstAmount, iteration);
        // when
        String output = IntStream.range(0, 1000)
                .parallel()
                .mapToObj(iterationIndex -> InvestPlan.input(input).output())
                .collect(Collectors.joining(System.lineSeparator()));
        // then
        Approvals.verify(output);
    }
}