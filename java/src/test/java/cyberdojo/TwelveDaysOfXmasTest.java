package cyberdojo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// http://rosettacode.org/wiki/The_Twelve_Days_of_Christmas
// Write a program that outputs the lyrics of the Christmas carol The Twelve Days of Christmas.
class TwelveDaysOfXmasTest {

    public static final Map<Integer, String> LABEL_BY_DAY = new HashMap<>();
    public static final List<String> GIFTS = List.of(
            "A partridge in a pear tree.",
            "Two turtle doves and",
            "Three french hens",
            "Four calling birds",
            "Five golden rings",
            "Six geese a-laying",
            "Seven swans a-swimming",
            "Eight maids a-milking",
            "Nine ladies dancing",
            "Ten lords a-leaping",
            "Eleven pipers piping",
            "Twelve drummers drumming");

    static {
        LABEL_BY_DAY.put(1, "first");
        LABEL_BY_DAY.put(2, "second");
        LABEL_BY_DAY.put(3, "third");
        LABEL_BY_DAY.put(4, "forth");
        LABEL_BY_DAY.put(5, "fifth");
        LABEL_BY_DAY.put(6, "sixth");
        LABEL_BY_DAY.put(7, "seventh");
        LABEL_BY_DAY.put(8, "eight");
        LABEL_BY_DAY.put(9, "ninth");
        LABEL_BY_DAY.put(10, "tenth");
        LABEL_BY_DAY.put(11, "eleventh");
        LABEL_BY_DAY.put(12, "Twelfth");
    }

    @Test
    void whole_song() {
        String christmasSong = "On the first day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the second day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the third day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the forth day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the fifth day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Five golden rings" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the sixth day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Six geese a-laying" + System.lineSeparator() +
                "Five golden rings" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the seventh day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Seven swans a-swimming" + System.lineSeparator() +
                "Six geese a-laying" + System.lineSeparator() +
                "Five golden rings" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the eight day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Eight maids a-milking" + System.lineSeparator() +
                "Seven swans a-swimming" + System.lineSeparator() +
                "Six geese a-laying" + System.lineSeparator() +
                "Five golden rings" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the ninth day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Nine ladies dancing" + System.lineSeparator() +
                "Eight maids a-milking" + System.lineSeparator() +
                "Seven swans a-swimming" + System.lineSeparator() +
                "Six geese a-laying" + System.lineSeparator() +
                "Five golden rings" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the tenth day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Ten lords a-leaping" + System.lineSeparator() +
                "Nine ladies dancing" + System.lineSeparator() +
                "Eight maids a-milking" + System.lineSeparator() +
                "Seven swans a-swimming" + System.lineSeparator() +
                "Six geese a-laying" + System.lineSeparator() +
                "Five golden rings" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the eleventh day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Eleven pipers piping" + System.lineSeparator() +
                "Ten lords a-leaping" + System.lineSeparator() +
                "Nine ladies dancing" + System.lineSeparator() +
                "Eight maids a-milking" + System.lineSeparator() +
                "Seven swans a-swimming" + System.lineSeparator() +
                "Six geese a-laying" + System.lineSeparator() +
                "Five golden rings" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves and" + System.lineSeparator() +
                "A partridge in a pear tree." + System.lineSeparator() +
                System.lineSeparator() +
                "On the Twelfth day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator() +
                "Twelve drummers drumming" + System.lineSeparator() +
                "Eleven pipers piping" + System.lineSeparator() +
                "Ten lords a-leaping" + System.lineSeparator() +
                "Nine ladies dancing" + System.lineSeparator() +
                "Eight maids a-milking" + System.lineSeparator() +
                "Seven swans a-swimming" + System.lineSeparator() +
                "Six geese a-laying" + System.lineSeparator() +
                "Five golden rings" + System.lineSeparator() +
                "Four calling birds" + System.lineSeparator() +
                "Three french hens" + System.lineSeparator() +
                "Two turtle doves" + System.lineSeparator() +
                "And a partridge in a pear tree.";
        Assertions.assertEquals(christmasSong, xmasSong());
    }

    private String xmasSong() {
        StringBuilder song = new StringBuilder();
        for (int i = 1; i <= 12; i++) {
            song.append(twoFirstVerses(i));
            song.append(gifts(i));
            if (i != 12) {
                song.append(verseSeparator());
            }
        }
        return song.toString();
    }

    private String verseSeparator() {
        return System.lineSeparator() + System.lineSeparator();
    }

    private String twoFirstVerses(int verse) {
        String label = LABEL_BY_DAY.get(verse);
        return "On the " + label + " day of Christmas" + System.lineSeparator() +
                "My true love gave to me:" + System.lineSeparator();
    }

    private String gifts(int index) {
        StringBuilder sb = new StringBuilder();
        for (int i = index - 1; i >= 0; i--) {
            if (sb.length() > 0) {
                sb.append(System.lineSeparator());
            }
            if (index == 12 && i == 1) {
                sb.append("Two turtle doves");
            } else if (index == 12 && i == 0) {
                sb.append("And a partridge in a pear tree.");
            } else {
                sb.append(GIFTS.get(i));
            }
        }
        return sb.toString();
    }
}
