package cyberdojo;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ISBNTest {

    @Test
    void is_isbn_13() {
        assertTrue(isValidIsnb13("9782012903807"));
        assertTrue(isValidIsnb13("9782012903999"));
    }

    @Test
    void is_not_isbn_13() {
        assertFalse(isValidIsnb13("9782012903808"));
        assertFalse(isValidIsnb13("9782012903991"));
        assertFalse(isValidIsnb13("9782012903992"));
    }

    private boolean isValidIsnb13(String isbn) {
        char[] chars = isbn.toCharArray();
        int total = IntStream.range(0, chars.length - 1)
                .map(index -> Character.getNumericValue(chars[index]) * (index % 2 == 0 ? 1 : 3))
                .sum();
        int modulo = (10 - (total % 10)) % 10;
        return Character.getNumericValue(chars[chars.length - 1]) == modulo;
    }
}
