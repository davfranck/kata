package cyberdojo.harrypotter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class BookSaleTest {

    public static final Map<Integer, Double> DISCOUNT_PER_NB_BOOKS = Map.of(
            1, 0d,
            2, 0.05,
            3, 0.1,
            4, 0.2,
            5, 0.25
    );

    @Test
    void no_book_in_cart() {
        double total = sale(List.of());
        Assertions.assertEquals(0d, total);
    }

    @Test
    void sell_one_book() {
        double total = sale(List.of(1));
        Assertions.assertEquals(8d, total);
    }

    @Test
    void sell_two_different_books() {
        double total = sale(List.of(1, 2));
        Assertions.assertEquals(16d - 0.8, total);
    }

    @Test
    void sell_three_different_books() {
        double total = sale(List.of(1, 2, 3));
        Assertions.assertEquals(24d - 2.4, total);
    }

    @Test
    void sell_four_different_books() {
        double total = sale(List.of(1, 2, 3, 4));
        Assertions.assertEquals(32d - 6.4, total);
    }

    @Test
    void sell_five_different_books() {
        double total = sale(List.of(1, 2, 3, 4, 5));
        Assertions.assertEquals(40d - 10d, total);
    }

    @Test
    void sell_twice_first_book() {
        double total = sale(List.of(1, 1));
        Assertions.assertEquals(16d, total);
    }

    @Test
    void sell_twice_first_book_and_once_second_book() {
        double total = sale(List.of(1, 2, 1));
        Assertions.assertEquals(8 + (16 - 0.8), total);
    }

    @Test
    void non_optimal_total() {
        double total = sale(List.of(1, 2, 1, 2, 3, 3, 4, 5));
        Assertions.assertEquals(51.6, total);
    }

    private double sale(List<Integer> books) {
        ArrayList<Integer> booksCopy = new ArrayList<>(books);
        double allGroupsTotal = 0;
        while (booksCopy.size() > 0) {
            double total = computeGroupPrice(booksCopy);
            allGroupsTotal += total;
        }
        return allGroupsTotal;
    }

    private double computeGroupPrice(ArrayList<Integer> booksCopy) {
        HashSet<Integer> uniqueBooks = new HashSet<>(booksCopy);
        for (Integer uniqueBook : uniqueBooks) {
            booksCopy.remove(uniqueBook);
        }
        int nbBooks = uniqueBooks.size();
        Double discount = DISCOUNT_PER_NB_BOOKS.get(nbBooks);
        return 8 * nbBooks - 8 * nbBooks * discount;
    }
}
