package cyberdojo.doors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

// http://rosettacode.org/wiki/100_doors
public class OpenDoorsTest {
    @Test
    void open_one_door() {
        boolean[] doors = {false};
        Assertions.assertArrayEquals(new boolean[]{true}, toggleDoors(1, doors));
    }

    @Test
    void toggle_two_doors_one_iteration() {
        boolean[] doors = {false, false};
        Assertions.assertArrayEquals(new boolean[]{true, true}, toggleDoors(1, doors));
    }

    @Test
    void toggle_two_doors_two_iterations() {
        boolean[] doors = {false, false};
        Assertions.assertArrayEquals(new boolean[]{true, false}, toggleDoors(2, doors));
    }

    @Test
    void toggle_three_doors_three_iterations() {
        boolean[] doors = {false, false, false};
        Assertions.assertArrayEquals(new boolean[]{true, false, false}, toggleDoors(3, doors));
    }

    @Test
    void toggle_four_doors_four_iterations() {
        boolean[] doors = {false, false, false, false};
        Assertions.assertArrayEquals(new boolean[]{true, false, false, true}, toggleDoors(4, doors));
    }

    private boolean[] toggleDoors(int iterations, boolean[] doors) {
        for (int i = 1; i <= iterations; i++) {
            for (int j = i; j <= doors.length; j = j + i) {
                doors[j - 1] = !doors[j - 1];
            }
        }
        return doors;
    }

}
