package codewars;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class FeastOfManyBeastsTest {

    @Test
    void fjdkskj() {
        Assertions.assertThat(FeastOfManyBeastsTest.feast("Ours", "Boeufs"))
                .as("does not start with B").isFalse();
        Assertions.assertThat(FeastOfManyBeastsTest.feast("Ours", "Oeuf")).isFalse();
        Assertions.assertThat(FeastOfManyBeastsTest.feast("Ours", "Oeufs")).isTrue();
    }

    private static boolean feast(String beast, String dish) {
        char[] beastAsCharArray = beast.toCharArray();
        char[] dishAsCharArray = dish.toCharArray();
        return beastAsCharArray[0] == dishAsCharArray[0]
                && beastAsCharArray[beastAsCharArray.length - 1] == dishAsCharArray[dishAsCharArray.length - 1];
    }
}
