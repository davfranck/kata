package codewars;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;

// https://www.codewars.com/kata/52685f7382004e774f0001f7
public class HumanReadableTimeTest {

    @Test
    void zero_second() {
        Assertions.assertEquals("00:00:00", makeReadable(0));
    }

    @Test
    void one_second() {
        Assertions.assertEquals("00:00:01", makeReadable(1));
    }

    @Test
    void ten_seconds() {
        Assertions.assertEquals("00:00:10", makeReadable(10));
    }

    @Test
    void ten_minutes() {
        Assertions.assertEquals("00:10:00", makeReadable(60 * 10));
    }

    @Test
    void two_minutes_ten_seconds() {
        Assertions.assertEquals("00:02:10", makeReadable(60 * 2 + 10));
    }

    @Test
    void ten_hours() {
        Assertions.assertEquals("10:00:00", makeReadable(60 * 60 * 10));
    }

    @Test
    void max_value() {
        Assertions.assertEquals("99:59:59", makeReadable(60 * 60 * 99 + 60 * 59 + 59));
    }

    private String makeReadable(int seconds) {
        Duration duration = Duration.ofSeconds(seconds);
        String hoursAsString = String.format("%02d", duration.toHours());
        String minutesAsString = String.format("%02d", duration.toMinutesPart());
        String secondsString = String.format("%02d", duration.toSecondsPart());

        return hoursAsString + ":" + minutesAsString + ":" + secondsString;
    }
}
