package codewars;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

// https://www.codewars.com/kata/520b9d2ad5c005041100000f
class SimplePigLatinTest {

    @Test
    void empty_input() {
        assertEquals("", pigIt(""));
    }

    @Test
    void one_char_input() {
        assertEquals("day", pigIt("d"));
    }

    @Test
    void one_word_input() {
        assertEquals("igPay", pigIt("Pig"));
    }

    @Test
    void many_words() {
        assertEquals("igPay ogDay irdbay", pigIt("Pig Dog bird"));
    }

    @Test
    void dont_touch_punctuation() {
        assertEquals("igPay ! ogDay irdbay !", pigIt("Pig ! Dog bird !"));
    }

    static final List<Character> punctuations = List.of('!', '.', ',', ';', '?');

    private String pigIt(String str) {
        if (str.isEmpty()) {
            return "";
        }
        String[] tokens = str.split(" ");
        if (tokens.length == 1) {
            char first = str.charAt(0);
            return punctuations.contains(first) ? String.valueOf(first) : str.substring(1) + first + "ay";
        } else {
            return Arrays.stream(tokens)
                    .map(this::pigIt)
                    .collect(Collectors.joining(" "));
        }
    }
}
