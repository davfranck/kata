package codewars;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

// https://www.codewars.com/kata/550f22f4d758534c1100025a
class DirectionReductionsTest {

    @ParameterizedTest(name = "Directions {0} reduced as {1}")
    @MethodSource("dirReducParamsProvider")
    void dirReducTest(String[] input, String[] expected) {
        assertArrayEquals(expected, dirReduc(input));
    }

    static Stream<Arguments> dirReducParamsProvider() {
        return Stream.of(
                arguments(new String[]{}, new String[]{}),
                arguments(new String[]{"NORTH"}, new String[]{"NORTH"}),
                arguments(new String[]{"NORTH", "SOUTH"}, new String[]{}),
                arguments(new String[]{"SOUTH", "NORTH"}, new String[]{}),
                arguments(new String[]{"EAST", "WEST"}, new String[]{}),
                arguments(new String[]{"WEST", "EAST"}, new String[]{}),
                arguments(new String[]{"WEST", "EAST", "NORTH"}, new String[]{"NORTH"}),
                arguments(new String[]{"NORTH", "WEST", "EAST"}, new String[]{"NORTH"}),
                arguments(new String[]{"NORTH", "WEST", "EAST", "SOUTH"}, new String[]{}),
                arguments(new String[]{"SOUTH", "WEST", "NORTH", "WEST", "EAST", "SOUTH", "EAST", "EAST"}, new String[]{"SOUTH", "EAST"})
        );
    }

    /*
    Meilleure implémentation : avec une stack ! (encore...)
     */
    private String[] dirReduc(String[] arr) {
        return dirReduc(arr, 0);
    }

    private String[] dirReduc(String[] arr, int start) {
        if (arr.length == 0 || arr.length == 1 || start == arr.length - 1) {
            return arr;
        }
        if (arr[start].equals("NORTH") && arr[start + 1].equals("SOUTH")
                || arr[start].equals("SOUTH") && arr[start + 1].equals("NORTH")
                || arr[start].equals("EAST") && arr[start + 1].equals("WEST")
                || arr[start].equals("WEST") && arr[start + 1].equals("EAST")) {
            String[] startArray = new String[0];
            if (start != 0) {
                startArray = Arrays.copyOfRange(arr, 0, start);
            }
            String[] endArray = Arrays.copyOfRange(arr, start + 2, arr.length);
            String[] newArray = Stream.concat(Arrays.stream(startArray), Arrays.stream(endArray)).toArray(String[]::new);
            return dirReduc(newArray, 0);
        } else {
            return dirReduc(arr, start + 1);
        }
    }
}
