package codewars;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

// https://www.codewars.com/kata/58a65c82586e98266200005b/train/java
public class FixProgressionTest {

    @Test
    void no_change() {
        assertThat(FixProgression.neededSwaps(new int[]{1, 2})).isEqualTo(0);
    }

    @Test
    void delta_evaluated_from_two_first_values() {
        assertThat(FixProgression.neededSwaps(new int[]{1, 2, 4, 4, 6})).isEqualTo(2);
    }

    @Test
    void delta_evaluated_from_second_and_third_values() {
        assertThat(FixProgression.neededSwaps(new int[]{1, 3, 4, 5})).isEqualTo(1);
    }

    @Test
    void failing_test_wip() {
        // écart entre deux nombres non consécutifs
        assertThat(FixProgression.neededSwaps(new int[]{1, 10, 2, 12, 3, 14, 4, 16, 5})).isEqualTo(5);
    }

    private static class FixProgression {

        public static int neededSwaps(int[] numbers) {
            int smallestSwap = Integer.MAX_VALUE;

            for (int i = 0; i < numbers.length - 1; i++) {
                int currentSwap = nbSwaps(numbers, i, i + 1);
                if (smallestSwap > currentSwap) {
                    System.out.println("better swap : " + i + " = " + currentSwap);
                    smallestSwap = currentSwap;
                }
            }
            return smallestSwap;
        }

        private static int nbSwaps(int[] numbers, int from, int to) {
            int delta = numbers[to] - numbers[from];
            int expectedCurrentValue = numbers[from];
            int nbToChange = 0;
            // on monte à partir de la borne inférieure
            for (int i = to; i < numbers.length; i++) {
                expectedCurrentValue += delta;
                if (expectedCurrentValue != numbers[i]) {
                    nbToChange++;
                }
            }
            // on descend à partir de la borne supérieure
            expectedCurrentValue = numbers[to];
            for (int i = from; i >= 0; i--) {
                expectedCurrentValue -= delta;
                if (expectedCurrentValue != numbers[i]) {
                    nbToChange++;
                }
            }
            return nbToChange;
        }
    }
}
