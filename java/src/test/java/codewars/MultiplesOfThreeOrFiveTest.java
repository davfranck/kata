package codewars;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Je me rends compte après coup que ma façon de procéder n'a pas été la meilleure.
 * Plutôt que de faire un test sur la frontière comme attendu dans la méthode fournie, j'aurais pu faire un test sur
 * une autre méthode qui prend en paramètre une liste.
 * Par exemple : <pre>assertThat(Solution.sumOfMultiplesOf3Or5(List.of(1,2,3,4)).isEqualTo(3)</pre>
 */
class MultiplesOfThreeOrFiveTest {

    @Test
    void sumOfMultiplesOf3or5WhenMaxValueIsTwo() {
        assertThat(Solution.solution(3)).isZero();
    }

    @Test
    void sumOfMultiplesOf3or5WhenMaxValueIs3() {
        assertThat(Solution.solution(4)).isEqualTo(3);
    }

    @Test
    void sumOfMultiplesOf3or5WhenMaxValueIs5() {
        assertThat(Solution.solution(6)).isEqualTo(8);
    }

    @Test
    void sumOfMultiplesOf3or5WhenMaxValueIs6() {
        assertThat(Solution.solution(7)).isEqualTo(14);
    }

    @Test
    void sumOfMultiplesOf3or5WhenMaxValueIs10() {
        assertThat(Solution.solution(10)).isEqualTo(23);
    }

    @Test
    void returnZeroWhenBoundaryIsNegative() {
        assertThat(Solution.solution(-1)).isZero();
    }

    private static class Solution {

        public static int solution(int maxValue) {
            return IntStream.range(0, maxValue)
                    .filter(value -> value % 3 == 0 || value % 5 == 0)
                    .sum();
        }
    }
}
