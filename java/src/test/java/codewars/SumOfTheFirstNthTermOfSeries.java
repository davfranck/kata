package codewars;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * https://www.codewars.com/kata/555eded1ad94b00403000071/train/java
 */
public class SumOfTheFirstNthTermOfSeries {

    @ParameterizedTest
    @CsvSource({"0, 0.00",
            "1, 1.00",
            "2, 1.25",
            "5, 1.57",
            "15, 1.94",
            "27, 2.14"})
    void sumSeriesSequential(Integer input, String expected) {
        // when
        String sum = SumOfTheFirstNthTermOfSeries.seriesSum(input);
        // then
        assertThat(sum).isEqualTo(expected);
    }

    @ParameterizedTest
    @CsvSource({"0, 0.00",
            "1, 1.00",
            "2, 1.25",
            "5, 1.57",
            "15, 1.94",
            "27, 2.14"})
    void sumSeriesParallel(Integer input, String expected) {
        // when
        String sum = SumOfTheFirstNthTermOfSeries.seriesSumParallel(input);

        // then
        assertThat(sum).isEqualTo(expected);
    }

    private static String seriesSum(int n) {
        if (n < 2) {
            return new BigDecimal(n).setScale(2).toString();
        }
        BigDecimal sum = BigDecimal.ONE;
        for (int i = 2; i <= n; i++) {
            int divisor = ((i - 1) * 3) + 1;
            BigDecimal toAdd = BigDecimal.ONE.divide(new BigDecimal(divisor), MathContext.DECIMAL128);
            sum = sum.add(toAdd);
        }
        return sum.setScale(2, RoundingMode.HALF_DOWN).toString();
    }

    private static String seriesSumParallel(int n) {
        if (n < 2) {
            return new BigDecimal(n).setScale(2).toString();
        }
        return IntStream.range(2, n + 1)
                .parallel()
                .mapToObj(operand -> {
                    int divisor = ((operand - 1) * 3) + 1;
                    return BigDecimal.ONE.divide(new BigDecimal(divisor), MathContext.DECIMAL128);
                })
                .reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2))
                .add(BigDecimal.ONE)
                .setScale(2, RoundingMode.HALF_DOWN)
                .toString();
    }

}
