package codewars;

import org.junit.jupiter.api.Test;

import static codewars.MaximumSubArraySum.sequence;
import static org.assertj.core.api.Assertions.assertThat;

class MaximumSubArraySumTest {

    @Test
    public void oneNumber() {
        assertThat(sequence(new int[]{1})).isEqualTo(1);
        assertThat(sequence(new int[]{2})).isEqualTo(2);
    }

    @Test
    public void onlyPositiveNumbers() {
        assertThat(sequence(new int[]{1, 2})).isEqualTo(3);
    }

    @Test
    public void positiveAndNegativeNumbers() {
        assertThat(sequence(new int[]{1, 2, -1, 2})).isEqualTo(4);
        assertThat(sequence(new int[]{2, -1, 2})).isEqualTo(3);
        assertThat(sequence(new int[]{2, -3, 1})).isEqualTo(2);
        assertThat(sequence(new int[]{1, -3, 2})).isEqualTo(2);
        assertThat(sequence(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4})).isEqualTo(6);
    }

    @Test
    public void allNegative() {
        assertThat(sequence(new int[]{-2})).isEqualTo(0);
        assertThat(sequence(new int[]{-2, -6, -9})).isEqualTo(0);
    }

    @Test
    public void emptyList() {
        assertThat(sequence(new int[]{})).isEqualTo(0);
    }

}