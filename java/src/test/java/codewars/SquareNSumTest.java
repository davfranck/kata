package codewars;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

// https://www.codewars.com/kata/515e271a311df0350d00000f/train/java
public class SquareNSumTest {

    @Test
    void empty_array() {
        assertThat(squareSum(new int[]{})).isEqualTo(0);
    }

    @Test
    void one_numer() {
        assertThat(squareSum(new int[]{2})).isEqualTo(4);
    }

    @Test
    void two_numbers() {
        assertThat(squareSum(new int[]{2, 3})).isEqualTo(2 * 2 + 3 * 3);
    }

    private int squareSum(int[] n) {
        return Arrays.stream(n)
                .map(number -> number * number)
                .sum();

    }
}
