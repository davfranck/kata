package codewars;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

// https://www.codewars.com/kata/61559bc4ead5b1004f1aba83
public class CountASpiralTest {

    @ParameterizedTest(name = "when size is {0} the expected sum is {1}")
    @MethodSource("sumArgs")
    void sum_test(BigInteger size, BigInteger expectedSum) {
        assertEquals(expectedSum, sum(size));
    }

    static Stream<Arguments> sumArgs() {
        return Stream.of(
                arguments(BigInteger.valueOf(5), BigInteger.valueOf(5 + 4 + 4 + 2 + 2)),
                arguments(BigInteger.valueOf(6), BigInteger.valueOf(6 + 5 + 5 + 3 + 3 + 1)),
                arguments(BigInteger.valueOf(7), BigInteger.valueOf(7 + 6 + 6 + 4 + 4 + 2 + 2)),
                arguments(BigInteger.valueOf(8), BigInteger.valueOf(8 + 7 + 7 + 5 + 5 + 3 + 3 + 1)),
                arguments(BigInteger.valueOf(9), BigInteger.valueOf(9 + 8 + 8 + 6 + 6 + 4 + 4 + 2 + 2)),
                arguments(BigInteger.valueOf(10), BigInteger.valueOf(10 + 9 + 9 + 7 + 7 + 5 + 5 + 3 + 3 + 1))
        );
    }

    private BigInteger sum(BigInteger size) {
        boolean isEven = size.remainder(BigInteger.valueOf(2)).equals(BigInteger.ZERO);
        BigInteger result = isEven ? BigInteger.ONE.add(size) : size;
        // arithmetic progression n(first + last) / 2
        BigInteger n = size.subtract(BigInteger.ONE).divide(BigInteger.valueOf(2));
        BigInteger first = isEven ? BigInteger.valueOf(3) : BigInteger.valueOf(2);
        BigInteger last = size.subtract(BigInteger.ONE);
        BigInteger test = n.multiply(first.add(last)).divide(BigInteger.valueOf(2));
        return result.add(test.multiply(BigInteger.valueOf(2)));
    }
}
