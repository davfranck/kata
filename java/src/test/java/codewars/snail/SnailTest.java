package codewars.snail;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

// https://www.codewars.com/kata/521c2db8ddc89b9b7a0000c1
class SnailTest {
    @Test
    void empty_array() {
        int[][] input = new int[0][0];
        Assertions.assertArrayEquals(new int[0], snail(input));
    }

    @Test
    void one_per_one_array() {
        int[][] input = new int[][]{{1}};
        Assertions.assertArrayEquals(new int[]{1}, snail(input));
    }

    @Test
    void two_per_two_array() {
        int[][] input = new int[][]{
                {1, 2},
                {4, 3}
        };
        Assertions.assertArrayEquals(new int[]{1, 2, 3, 4}, snail(input));
    }

    @Test
    void three_per_three_array() {
        int[][] input = new int[][]{
                {1, 2, 3},
                {8, 9, 4},
                {7, 6, 5}
        };
        Assertions.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, snail(input));
    }

    @Test
    void four_per_four() {
        int[][] input = new int[][]{
                {1, 2, 3, 5},
                {13, 14, 15, 6},
                {12, 17, 16, 7},
                {11, 10, 9, 8}
        };
        Assertions.assertArrayEquals(new int[]{1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17}, snail(input));
    }

    private int[] snail(int[][] array) {
        if (array.length == 1) {
            return array[0];
        }
        if (array.length == 0) {
            return new int[0];
        }
        int maxArrayIndex = array.length - 1;
        int[] firstRow = array[0];
        int[] lastColumn = lastColumn(array, maxArrayIndex);
        int[] lastRow = lastRow(array, maxArrayIndex);
        int[] firstColumn = firstColumn(array, maxArrayIndex);

        int[] currentArrayPerimeter = concatAll(firstRow, lastColumn, lastRow, firstColumn);

        int[][] innerArray = innerArrayOf(array);

        return concat(currentArrayPerimeter, snail(innerArray));
    }

    private int[] firstColumn(int[][] array, int maxArrayIndex) {
        int[] firstColumn = new int[maxArrayIndex];
        int tmpFirstColumIndex = 0;
        for (int i = maxArrayIndex; i > 0; i--) {
            firstColumn[tmpFirstColumIndex] = array[i][0];
            tmpFirstColumIndex++;
        }
        return firstColumn;
    }

    private int[] lastRow(int[][] array, int maxArrayIndex) {
        int[] lastRow = new int[maxArrayIndex - 1];
        int tmpLastRowIndex = 0;
        for (int i = maxArrayIndex - 1; i > 0; i--) {
            lastRow[tmpLastRowIndex] = array[maxArrayIndex][i];
            tmpLastRowIndex++;
        }
        return lastRow;
    }

    private int[] lastColumn(int[][] array, int maxArrayIndex) {
        int[] lastColumn = new int[maxArrayIndex];
        int tmpIndex = 0;
        for (int i = 1; i < array.length; i++) {
            lastColumn[tmpIndex] = array[i][maxArrayIndex];
            tmpIndex++;
        }
        return lastColumn;
    }

    private int[][] innerArrayOf(int[][] array) {
        int[][] subArray = new int[array.length - 2][array.length - 2];
        int subArrayRowIndex = 0;
        for (int i = 1; i < array.length - 1; i++) {
            int subArrayColIndex = 0;
            for (int j = 1; j < array.length - 1; j++) {
                subArray[subArrayRowIndex][subArrayColIndex] = array[i][j];
                subArrayColIndex++;
            }
            subArrayRowIndex++;
        }
        return subArray;
    }

    private int[] concatAll(int[] firstRow, int[] lastColumn, int[] lastRow, int[] firstColumn) {
        int[] firstRowAndLastColumn = concat(firstRow, lastColumn);
        int[] firstRowLastColumnLastRow = concat(firstRowAndLastColumn, lastRow);
        return concat(firstRowLastColumnLastRow, firstColumn);
    }

    private int[] concat(int[] firstRow, int[] reversedSecondRow) {
        int[] result = Arrays.copyOf(firstRow, firstRow.length + reversedSecondRow.length);
        System.arraycopy(reversedSecondRow, 0, result, firstRow.length, reversedSecondRow.length);
        return result;
    }
}
