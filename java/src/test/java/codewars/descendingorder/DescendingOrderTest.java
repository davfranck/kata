package codewars.descendingorder;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DescendingOrderTest {

    @Test
    public void returnSameNumberWhenOneNumber() {
        assertThat(DescendingOrder.sortDesc(1)).isEqualTo(1);
        assertThat(DescendingOrder.sortDesc(2)).isEqualTo(2);
    }

    @Test
    public void shouldReverseNumber() {
        assertThat(DescendingOrder.sortDesc(12)).isEqualTo(21);
    }

    @Test
    public void shouldReverseAndOrder() {
        assertThat(DescendingOrder.sortDesc(1201)).isEqualTo(2110);
    }
}
