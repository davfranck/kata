package codewars;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ValidParenthesesTest {
    @Test
    void isValidWhenOneOpeningAndOneClosingParenthese() {
        assertThat(Solution.validParentheses("()")).isTrue();
    }

    @Test
    void isInvalidWhenOnlyOneOpeningParenthese() {
        assertThat(Solution.validParentheses("(")).isFalse();
    }

    @Test
    void isInvalidWhenOnlyOneClosingParenthese() {
        assertThat(Solution.validParentheses(")")).isFalse();
    }

    @Test
    void isInvalidWhenMoreOpeningParentheses() {
        assertThat(Solution.validParentheses("(()")).isFalse();
    }

    @Test
    void isInvalidWhenSameAmountOfParenthesesNotWellPlaced() {
        assertThat(Solution.validParentheses(")(")).isFalse();
    }

    private static class Solution {
        public static boolean validParentheses(String input) {
            String[] inputAsArray = input.split("");
            int nbNonClosed = 0;
            boolean isValid = true;
            for (int i = 0; i < inputAsArray.length; i++) {
                String s = inputAsArray[i];
                if (s.equals("(")) {
                    nbNonClosed++;
                } else if (s.equals(")")) {
                    nbNonClosed--;
                }
                if (nbNonClosed < 0) {
                    isValid = false;
                    break;
                }
            }
            if (nbNonClosed != 0) {
                isValid = false;
            }
            return isValid;
        }
    }
}
