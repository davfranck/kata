package codewars;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class OppositesAttractTest {

    @ParameterizedTest(name = "In love when one is even and one is odd : {arguments}")
    @CsvSource({"1,0", "0,1"})
    void in_love_when_one_flower_even(int flower1, int flower2) {
        assertThat(isLove(flower1, flower2)).isTrue();
    }

    @ParameterizedTest(name = "Not in love when both even or both odd : {arguments}")
    @CsvSource({"1,1", "0,0"})
    void not_in_love_when_both_even_or_both_odd(int flower1, int flower2) {
        assertThat(isLove(flower1, flower2)).isFalse();
    }

    private boolean isLove(int flower1, int flower2) {
        return flower1 % 2 != flower2 % 2;
    }
}
