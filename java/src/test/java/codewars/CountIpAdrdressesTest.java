package codewars;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class CountIpAdrdressesTest {

    @ParameterizedTest
    @CsvSource({
            "0.0.0.0,0",
            "0.0.0.1,1",
            "0.0.1.0,256",
            "0.0.1.1,257",
            "0.0.255.255,65535",
            "0.1.0.0,65536",
            "0.255.255.255,16777215",
            "1.0.0.0,16777216",
    })
    void nb_addresses_for_ip(String ip, long expected) {
        long number = ipToNbAdresses(ip);
        assertThat(number).isEqualTo(expected);
    }

    @ParameterizedTest
    @CsvSource({
            "0.0.0.1,0.0.0.0,1",
            "0.0.0.0,0.0.0.1,1",
            "0.0.1.0,0.0.0.1,255",
            "20.0.0.10,20.0.1.0,246",
            "0.0.0.0,255.255.255.255,4294967295"
    })
    void ips_between(String ip1, String ip2, long expectedDiff) {
        long diff = ipsBetween(ip1, ip2);
        assertThat(diff).isEqualTo(expectedDiff);
    }

    private long ipsBetween(String start, String end) {
        long ip1Addresses = ipToNbAdresses(start);
        long ip2Adresses = ipToNbAdresses(end);
        return Math.max(ip1Addresses, ip2Adresses) - Math.min(ip1Addresses, ip2Adresses);
    }

    private long ipToNbAdresses(String ip) {
        long[] ipParts = Arrays.stream(ip.split("\\."))
                .mapToLong(Integer::valueOf)
                .toArray();
        return ipParts[3]
                + ipParts[2] * 256
                + ipParts[1] * 256 * 256
                + ipParts[0] * 256 * 256 * 256;
    }
}
