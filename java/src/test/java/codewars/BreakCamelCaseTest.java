package codewars;

// https://www.codewars.com/kata/5208f99aee097e6552000148/train/java

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Complete the solution so that the function will break up camel casing, using a space between words.
 * Example
 * <ul>
 * <li>"camelCasing"  =>  "camel Casing"</li>
 * <li>"identifier"   =>  "identifier"</li>
 * <li>""             =>  ""</li>
 * </ul>
 */
public class BreakCamelCaseTest {

    @Test
    void empty() {
        Assertions.assertEquals("", camelCase(""));
    }

    @Test
    void single_word() {
        Assertions.assertEquals("single", camelCase("single"));
    }

    @Test
    void two_words() {
        Assertions.assertEquals("t W", camelCase("tW"));
    }

    @Test
    void three_words() {
        Assertions.assertEquals("t W T", camelCase("tWT"));
    }

    @Test
    void four_words() {
        Assertions.assertEquals("t W T Y", camelCase("tWTY"));
    }

    @Test
    void four_words_longer() {
        Assertions.assertEquals("to Wuu Ti Yeah", camelCase("toWuuTiYeah"));
    }

    private String camelCase(String input) {
        char[] chars = input.toCharArray();
        String out = "";
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] >= 65 && chars[i] <= 90) {
                out += " " + chars[i];
            } else {
                out += chars[i];
            }
        }
        return out;
    }
}
