package codewars;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.params.provider.Arguments.arguments;

// Source : https://www.codewars.com/kata/525f50e3b73515a6db000b83/train/java
class PhoneNumberFormatterShould {

    @ParameterizedTest
    @MethodSource("formatPhoneNumbersArgs")
    void format_phone_numbers(int[] inputNumbers, String expected) {
        // when
        String phoneNumber = createPhoneNumber(inputNumbers);
        // then
        Assertions.assertThat(phoneNumber).isEqualTo(expected);
    }

    static Stream<Arguments> formatPhoneNumbersArgs() {
        // target : (123) 456-7890
        return Stream.of(
                arguments(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}, "(123) 456-7890"),
                arguments(new int[]{0, 2, 3, 4, 5, 6, 7, 8, 9, 0}, "(023) 456-7890")
        );
    }

    private String createPhoneNumber(int[] numbers) {
        String inputAsString = Arrays.stream(numbers)
                .mapToObj(value -> String.valueOf(value))
                .collect(Collectors.joining());
        String firstThreeNumbers = inputAsString.substring(0, 3);
        String nextThreeNumbers = inputAsString.substring(3, 6);
        String end = inputAsString.substring(6);
        return "(" + firstThreeNumbers + ") " + nextThreeNumbers + "-" + end;
    }

}
