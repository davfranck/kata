package codewars.convertnumbertoreversedarray;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DigitizeTest {

    @Test
    public void digitize() {
        assertThat(Digitize.digitize(1)).contains(1);
        assertThat(Digitize.digitize(2)).contains(2);
        assertThat(Digitize.digitize(21)).containsExactly(1, 2);
    }
}
