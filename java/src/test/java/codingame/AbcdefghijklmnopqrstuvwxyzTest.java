package codingame;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

// https://www.codingame.com/ide/puzzle/abcdefghijklmnopqrstuvwxyz
public class AbcdefghijklmnopqrstuvwxyzTest {

    public static final List<String> LETTERS = List.of("b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");

    @Test
    void input_to_array() {
        String input = """
                aaa
                bbb
                ccc""";
        String[][] expected = {
                {"a", "a", "a"},
                {"b", "b", "b"},
                {"c", "c", "c"},
        };
        Assertions.assertThat(inputToArray(input)).isEqualTo(expected);
    }

    public String[][] inputToArray(String input) {
        String[] lines = input.trim().split("\\n");
        String[][] result = new String[lines.length][];
        for (int i = 0; i < lines.length; i++) {
            String[] row = lines[i].split("");
            result[i] = row;
        }
        return result;
    }

    @Test
    void test1() {
        String input = """
                vkbjbzmbgb
                abcccpzouv
                fedopwlmcl
                glmnqrszyw
                hkrhiutymj
                ijqcmvwxoc
                pcvlpqzphl
                hsgvoklcxy
                urdjusmbmz
                rchbcausnp""";
        String[][] inputBoard = inputToArray(input);
        String[][] result = findPath(inputBoard);
        String expectedString = """
                ----------
                abc-------
                fedop-----
                glmnqrsz--
                hk---uty--
                ij---vwx--
                ----------
                ----------
                ----------
                ----------""";
        String[][] expected = inputToArray(expectedString);
        Assertions.assertThat(result).isEqualTo(expected);
    }

    @Test
    void test2() {
        String input = """
                xoikvoybwhvhkqs
                abcdpjzteanhnot
                wonezupfvoeetfc
                qpmfxfndfiqafpb
                rklgqyyltzvnugs
                sjihujprijqzvap
                tsoxsaoraruyqwk
                uwwonqolepmuqyy
                vwxwksnuizgbiht
                feyihffuvkbrfjw
                eezqukiqyzqjies
                uarhhcdjvpfwvck
                drtgqmroaaumyaq
                cbntlwlibaavsdc
                jpvogyiibfawjbn""";
        String[][] inputBoard = inputToArray(input);
        String[][] result = findPath(inputBoard);
        String expectedString = """
                ---------------
                abcd-----------
                -one-----------
                qpmf-----------
                rklg-----------
                sjih-----------
                t--------------
                u--------------
                vwx------------
                --y------------
                --z------------
                ---------------
                ---------------
                ---------------
                ---------------""";
        String[][] expected = inputToArray(expectedString);
        Assertions.assertThat(result).isEqualTo(expected);
    }

    @Test
    void test3() {
        String input = """
                qrnwxqufaqpmehg
                gqpsdpeqovfjtbv
                azdizzjbedhsyzt
                mzbegprlfcbaxwv
                hoibquzhgbdrggu
                gynohfmijkdgpqt
                lwemjvulqlmnors
                eckkhvwucrblxwc
                hxunydbmsxdmpkh
                drdzvbronvfjlwz
                npnjnjqkfvupezb
                hvdaqvyrwhdtxbb
                nfpgaffyuefppae
                forhbqkezipkinq
                usxszgnwvwndzrl""";
        String[][] inputBoard = inputToArray(input);
        String[][] result = findPath(inputBoard);
        String expectedString = """
                ---------------
                ---------------
                --------ed--yz-
                --------fcbaxwv
                -------hg-----u
                -------ijk--pqt
                ---------lmnors
                ---------------
                ---------------
                ---------------
                ---------------
                ---------------
                ---------------
                ---------------
                ---------------""";
        String[][] expected = inputToArray(expectedString);
        Assertions.assertThat(result).isEqualTo(expected);
    }


    @Test
    void test4() {
        String input = """
                qaytksajbfggzscxlwss
                avotuqnvgvmnghnwchnu
                aceglpwkcwyxqnjxetag
                ilddvpjguotxrnvkznkc
                wnbquhpzgpjjvaxblckl
                blyccpjqkwosrjvcouyt
                dlattqnvvgawdkcyhbhx
                alvpqxspvbeizbxaubby
                xtvxhvgfjiiffcynzfdo
                xlroyrcwaydggabiodrq
                hydaxaveuuextqzcgewo
                dcvbvqmsvebmbghclgbs
                izpesrsutapzuvrapkzg
                ratpfqwmokaujqhihdei
                jaabcvxvwpwozvoiwvya
                pdnedstuxrbxjuqnqfat
                xhgfqrezyiabcrupxwpi
                dijopgmxgezbgnyhwzih
                ceknitwldqjbxdgnbgbu
                yllmerzjxqjzrqlwfjwi""";
        String[][] inputBoard = inputToArray(input);
        String[][] result = findPath(inputBoard);
        String expectedString = """
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --------------------
                --abc--vw-----------
                ---edstux-----------
                -hgfqr-zy-----------
                -ijop---------------
                --kn----------------
                --lm----------------""";
        String[][] expected = inputToArray(expectedString);
        Assertions.assertThat(result).isEqualTo(expected);
    }

    @Test
    void test5() {
        String input = """
                enzronekzsmncqrupfjkzokzutbibz
                ddxkntwwwuriunbkfxwpsarujlkrhn
                unfsatsydaagxqhbfrzeigawsewixb
                kwmmudxpneowrxhzzicwrybmrawqhu
                gfyxpdnironminfhsevmvlxbriunky
                xkoaqlnzawcyyjpngycidgzqnjbgeh
                zjtxaqigtbpqhpkmdbghfwfawxvzeo
                tqoehzynwngzhfwouurpnfjzesgksn
                urmgqhukjplzvaumvrjuszkjruvoee
                cbwubfptjhefhqcwrtooikwtawavnr
                okphckdqkmvpcktenlevtwdwakzpvk
                quqoqveedzotxslsmxwnyojfxzrbgp
                svdymebhpuzgcfhulhnnptgsrijwkv
                zggdylnbarhonzkewlnmbnfimjvsou
                mguhqcbcdegftxmfvixspurjjbramw
                ldyrykyryfrlmzlwauatxcmtzalrtx
                udgepapmugnrzlcyothsdlbohzrnyj
                cscmdpkjihcvmocjqxxmeovkbkngbk
                fkqzdhloppadpaazoanoizuqlhyeqa
                xextdlmnqvbefgiujrihhmcviymbrn
                tpyjnutsrwvipotvcoexyrowabbcns
                imkyxvficculyqlipcvbiuygnaxwup
                yzodmwdbwtijkpolrdqwkwvbrivhrd
                hbquqxyzuqqynrgppitablcsxxvjya
                amzrkgnvijaohgapixspodcsbhphqg
                zijfoehygareiidyltgscqojnsolvu
                cnosdtngtetlvcriosgfzzbtsgqfjq
                flarkqdnisdjzwjjsshmxydorltxau
                bdmeahzediqhokktuasaxywkkjwali
                cclixxfhozajhkawqdzoilwwsvyzyl""";
        String[][] inputBoard = inputToArray(input);
        String[][] result = findPath(inputBoard);
        String expectedString = """
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                -------ba---------------------
                -------cde--------------------
                ---------f--------------------
                ---------g--------------------
                ------kjih--------------------
                ------lop---------------------
                ------mnq---------------------
                -----utsr---------------------
                -----v------------------------
                -----w------------------------
                -----xyz----------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------
                ------------------------------""";
        String[][] expected = inputToArray(expectedString);
        Assertions.assertThat(result).isEqualTo(expected);
    }

    public String[][] findPath(String[][] inputBoard) {
        Pathfinder pathfinder = new Pathfinder(inputBoard, new Coordinates(0, 0));
        while (true) {
            Optional<String[][]> maybeResult = pathfinder.resolve();
            if (maybeResult.isEmpty()) {
                pathfinder = pathfinder.incrementALetterCoordinatesCoordinates();
            } else {
                return maybeResult.get();
            }
        }
    }

    class Pathfinder {
        private final String[][] inputBoard;
        private final Coordinates aLetterCoordinates;
        private final String[][] result;

        public Pathfinder(String[][] inputBoard, Coordinates aLetterCoordinates) {
            this.inputBoard = inputBoard;
            this.aLetterCoordinates = findNextCoordinatesOfA(inputBoard, inputBoard.length, aLetterCoordinates);
            this.result = initResult(inputBoard.length);
            this.result[this.aLetterCoordinates.i][this.aLetterCoordinates.j] = "a";

        }

        public Optional<String[][]> resolve() {
            Coordinates currentLetterCoordinates = aLetterCoordinates;
            for (String currentLetter : LETTERS) {
                Optional<Coordinates> maybeCoordinates = findLetterInNeighborhood(currentLetter, currentLetterCoordinates);
                if (maybeCoordinates.isPresent()) {
                    result[maybeCoordinates.get().i][maybeCoordinates.get().j] = currentLetter;
                    currentLetterCoordinates = maybeCoordinates.get();
                } else {
                    return Optional.empty();
                }
            }
            return Optional.of(result);
        }

        // on pourrait contrôler ici la faisabilité d'incrémenter
        public Pathfinder incrementALetterCoordinatesCoordinates() {
            if (aLetterCoordinates.j < inputBoard.length - 1) {
                return new Pathfinder(inputBoard, new Coordinates(aLetterCoordinates.i, aLetterCoordinates.j + 1));
            }
            return new Pathfinder(inputBoard, new Coordinates(aLetterCoordinates.i + 1, 0));
        }


        private Optional<Coordinates> findLetterInNeighborhood(String neighbourLetter, Coordinates startCoordinates) {
            return getCoordinates(startCoordinates, 1, 0, inputBoard, neighbourLetter)
                    .or(() -> getCoordinates(startCoordinates, 0, -1, inputBoard, neighbourLetter))
                    .or(() -> getCoordinates(startCoordinates, 0, 1, inputBoard, neighbourLetter))
                    .or(() -> getCoordinates(startCoordinates, -1, 0, inputBoard, neighbourLetter));
        }

    }


    private Optional<Coordinates> getCoordinates(Coordinates startCoordinates, int addToI, int addToJ, String[][] inputBoard, String neighbourLetter) {
        Coordinates newCoordinates = new Coordinates(startCoordinates.i + addToI, startCoordinates.j + addToJ);
        if (newCoordinates.isInBounds(inputBoard.length)) {
            if (inputBoard[newCoordinates.i][newCoordinates.j].equals(neighbourLetter)) {
                return Optional.of(newCoordinates);
            }
        }
        return Optional.empty();
    }

    private static Coordinates findNextCoordinatesOfA(String[][] inputBoard, int length, Coordinates startCoordinates) {
        for (int i = startCoordinates.i; i < length; i++) {
            int startJ = startCoordinates.j;
            if (i != startCoordinates.i) {
                startJ = 0;
            }
            for (int j = startJ; j < length; j++) {
                if (inputBoard[i][j].equals("a")) {
                    return new Coordinates(i, j);
                }
            }
        }
        throw new IllegalStateException("pas de " + "a");
    }

    private static String[][] initResult(int length) {
        String[][] result = new String[length][length];
        for (int i = 0; i < length; i++) {
            Arrays.fill(result[i], "-");
        }
        return result;
    }

    record Coordinates(int i, int j) {
        public boolean isInBounds(int length) {
            return i <= length - 1 && i >= 0 && j <= length - 1 && j >= 0;
        }
    }

}
