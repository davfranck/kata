package codingame.asteroids;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class AsteroidsTest {

    @Test
    void from_positions() {
        Map<String, Position> asteroidPositionsAtT1ByName = Map.of(
                "A", new Position(1, 1),
                "B", new Position(2, 2));
        Map<String, Position> asteroidPositionsAtT2ByName = Map.of(
                "A", new Position(3, 3),
                "B", new Position(5, 6));

        int t2 = 3;
        int t1 = 1;

        AsteroidPositionsAtTime asteroidsPositionAtT1 = new AsteroidPositionsAtTime(asteroidPositionsAtT1ByName, t1);
        AsteroidPositionsAtTime asteroidsPositionAtT2 = new AsteroidPositionsAtTime(asteroidPositionsAtT2ByName, t2);
        // when
        Asteroids asteroids = Asteroids.from(asteroidsPositionAtT1, asteroidsPositionAtT2);
        // then
        Asteroid asteroidA = new Asteroid("A", new Position(3, 3), new SpeedPerRound(1, 1));
        Asteroid asteroidB = new Asteroid("B", new Position(5, 6), new SpeedPerRound(1.5, 2));
        assertThat(asteroids).isEqualTo(new Asteroids(List.of(asteroidA, asteroidB), t2));
    }

    @Test
    void move_asteroids() {
        int t2 = 3;
        Asteroid asteroidAAtPositionT2 = new Asteroid("A", new Position(3, 3), new SpeedPerRound(1, 1));
        Asteroid asteroidBAtPositionT2 = new Asteroid("B", new Position(5, 6), new SpeedPerRound(1.5, 2));
        Asteroids asteroids = new Asteroids(List.of(asteroidAAtPositionT2, asteroidBAtPositionT2), t2);
        int t3 = 4;
        // when
        Asteroids asteroidsAtT3 = asteroids.moveTo(t3);
        // then
        Asteroid asteroidA = new Asteroid("A", new Position(4, 4), new SpeedPerRound(1, 1));
        Asteroid asteroidB = new Asteroid("B", new Position(6, 8), new SpeedPerRound(1.5, 2));
        Asteroids expectedAsteroids = new Asteroids(List.of(asteroidA, asteroidB), t3);
        assertThat(asteroidsAtT3).isEqualTo(expectedAsteroids);
    }
}
