package codingame.asteroids;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

public class RegionTest {

    @Test
    void multiple_asteroids() {
        String input = "" +
                "6 6 1 3 5" + System.lineSeparator() +
                "A..... .A...." + System.lineSeparator() +
                "...... B....." + System.lineSeparator() +
                "B..... ......" + System.lineSeparator() +
                "...... ......" + System.lineSeparator() +
                "...... ......" + System.lineSeparator() +
                "...... ......";
        String regionAsString = execute(input);
        String expected = "" +
                "B.A..." + System.lineSeparator() +
                "......" + System.lineSeparator() +
                "......" + System.lineSeparator() +
                "......" + System.lineSeparator() +
                "......" + System.lineSeparator() +
                "......";
        assertThat(regionAsString).isEqualTo(expected);
    }

    @Test
    void depth() {
        String input = "" +
                "6 6 1 6 11" + System.lineSeparator() +
                "..H... ......" + System.lineSeparator() +
                "...... ..H..." + System.lineSeparator() +
                "E...G. .E.G.." + System.lineSeparator() +
                "...... ..F..." + System.lineSeparator() +
                "..F... ......" + System.lineSeparator() +
                "...... ......";
        String regionAsString = execute(input);
        String expected = "" +
                "......" + System.lineSeparator() +
                "......" + System.lineSeparator() +
                "..E..." + System.lineSeparator() +
                "......" + System.lineSeparator() +
                "......" + System.lineSeparator() +
                "......";
        assertThat(regionAsString).isEqualTo(expected);
    }

    @Test
    void out_of_bounds() {
        String input = "" +
                "10 10 100 200 300" + System.lineSeparator() +
                "A......... .A........" + System.lineSeparator() +
                "B......... ..B......." + System.lineSeparator() +
                "C......... ...C......" + System.lineSeparator() +
                "D......... ....D....." + System.lineSeparator() +
                "E......... .....E...." + System.lineSeparator() +
                ".........F ........F." + System.lineSeparator() +
                ".........G .......G.." + System.lineSeparator() +
                ".........H ......H..." + System.lineSeparator() +
                ".........I .....I...." + System.lineSeparator() +
                ".........J ....J.....";
        String regionAsString = execute(input);
        String expected = "" +
                "..A......." + System.lineSeparator() +
                "....B....." + System.lineSeparator() +
                "......C..." + System.lineSeparator() +
                "........D." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                ".......F.." + System.lineSeparator() +
                ".....G...." + System.lineSeparator() +
                "...H......" + System.lineSeparator() +
                ".I........" + System.lineSeparator() +
                "..........";
        assertThat(regionAsString).isEqualTo(expected);
    }

    @Test
    void armageddon() {
        String input = "" +
                "20 20 25 75 100" + System.lineSeparator() +
                ".................O.. G..................." + System.lineSeparator() +
                ".....N...........U.. ...............W...." + System.lineSeparator() +
                ".............L.R.... ...................C" + System.lineSeparator() +
                ".................... ...E................" + System.lineSeparator() +
                "..........Z..V.H.... ..............K....." + System.lineSeparator() +
                "................X... ...........T........" + System.lineSeparator() +
                ".............P...... ............A......." + System.lineSeparator() +
                ".............A...... .....P...FLI......N." + System.lineSeparator() +
                ".Q.............T.... ...................." + System.lineSeparator() +
                "..................F. ........D..........." + System.lineSeparator() +
                ".................... ......S..Y.........M" + System.lineSeparator() +
                "......K............W .........B....Z....." + System.lineSeparator() +
                "...............Y.... ...................." + System.lineSeparator() +
                "..............S..... ....V.............J." + System.lineSeparator() +
                "...........JE......D .........O.........." + System.lineSeparator() +
                "...M................ ..X...........U....." + System.lineSeparator() +
                "......B..G...C....I. ...................." + System.lineSeparator() +
                ".................... ...................." + System.lineSeparator() +
                ".................... ..Q................R" + System.lineSeparator() +
                ".................... .......H............";
        String regionAsString = execute(input);
        String expected = "" +
                "..................K." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                ".......I............" + System.lineSeparator() +
                ".........T.........." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "...........A........" + System.lineSeparator() +
                "..D.F..............." + System.lineSeparator() +
                ".P.................." + System.lineSeparator() +
                "..S.......B........." + System.lineSeparator() +
                "......Y.L..........." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "................Z..." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "...................." + System.lineSeparator() +
                "....................";
        assertThat(regionAsString).isEqualTo(expected);
    }

    private String execute(String input) {
        Scanner in = new Scanner(input);
        int W = in.nextInt();
        int H = in.nextInt();
        int T1 = in.nextInt();
        int T2 = in.nextInt();
        int T3 = in.nextInt();
        Map<String, Position> asteroidPositionsAtT1ByName = new HashMap<>();
        Map<String, Position> asteroidPositionsAtT2ByName = new HashMap<>();
        for (int i = 0; i < H; i++) {
            String firstPictureRow = in.next();
            addAsteroids(firstPictureRow, asteroidPositionsAtT1ByName, i);
            String secondPictureRow = in.next();
            addAsteroids(secondPictureRow, asteroidPositionsAtT2ByName, i);
        }
        AsteroidPositionsAtTime asteroidsPositionAtT1 = new AsteroidPositionsAtTime(asteroidPositionsAtT1ByName, T1);
        AsteroidPositionsAtTime asteroidsPositionAtT2 = new AsteroidPositionsAtTime(asteroidPositionsAtT2ByName, T2);
        Asteroids asteroidsAtT2 = Asteroids.from(asteroidsPositionAtT1, asteroidsPositionAtT2);
        Asteroids asteroidsAtT3 = asteroidsAtT2.moveTo(T3);
        String regionAsString = new Region(W, H, asteroidsAtT3).toString();
        return regionAsString;
    }

    private void addAsteroids(String pictureRow, Map<String, Position> asteroidPositionsAtTimeByName, int row) {
        String[] square = pictureRow.split("");
        for (int j = 0; j < square.length; j++) {
            String asteroidName = square[j];
            if (!asteroidName.equals(".")) {
                asteroidPositionsAtTimeByName.put(asteroidName, new Position(j, row));
            }
        }
    }

    @Test
    void test_toString() {
        int h = 10;
        int w = 10;
        Asteroid asteroidA = new Asteroid("A", new Position(0, 0), new SpeedPerRound(1, 1));
        Asteroids asteroids = new Asteroids(List.of(asteroidA), 3);
        // when
        String regionAsString = new Region(w, h, asteroids).toString();
        // then
        String expected = "" +
                "A........." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                ".........." + System.lineSeparator() +
                "..........";
        assertThat(regionAsString).isEqualTo(expected);
    }

    private class Region {
        private final int w;
        private final int h;
        private final Asteroids asteroids;

        public Region(int w, int h, Asteroids asteroids) {
            this.w = w;
            this.h = h;
            this.asteroids = asteroids;
        }

        @Override
        public String toString() {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < w; i++) {
                if (i != 0) {
                    output.append(System.lineSeparator());
                }
                for (int j = 0; j < h; j++) {
                    Optional<Asteroid> optionalAsteroid = asteroids.at(j, i);
                    String current = optionalAsteroid.map(asteroid -> asteroid.name())
                            .orElse(".");
                    output.append(current);
                }
            }
            return output.toString();
        }
    }
}
