package codingame;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class LogiGatesTest {
    private static String and(char in1Char, char in2Char) {
        if (in1Char == '_' || in2Char == '_') {
            return "_";
        } else {
            return "-";
        }
    }

    private static String or(char in1Char, char in2Char) {
        if (in1Char == '-' || in2Char == '-') {
            return "-";
        } else {
            return "_";
        }
    }

    @ParameterizedTest
    @CsvSource({
            "_,_,_",
            "-,-,-",
            "_,-,_",
            "__,__,__",
            "--,--,--",
            "__---___---___---___---___,____---___---___---___---_,____-_____-_____-_____-___"
    })
    void and(String input, String input2, String expected) {
        // when
        String out = logical(input, input2, LogiGatesTest::and);
        // then
        Assertions.assertThat(out).isEqualTo(expected);
    }

    @ParameterizedTest
    @CsvSource({
            "_,_,_",
            "_,-,-",
            "__---___---___---___---___,____---___---___---___---_,__-----_-----_-----_-----_"
    })
    void or(String input, String input2, String expected) {
        // when
        String out = logical(input, input2, LogiGatesTest::or);
        // then
        Assertions.assertThat(out).isEqualTo(expected);
    }

    private String logical(String in1, String in2, LogicalOperation operation) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < in1.length(); i++) {
            char in1Char = in1.charAt(i);
            char in2Char = in2.charAt(i);
            String charToAppend = operation.apply(in1Char, in2Char);
            result.append(charToAppend);
        }
        return result.toString();
    }

    interface LogicalOperation {

        String apply(char in1Char, char in2Char);
    }
}
