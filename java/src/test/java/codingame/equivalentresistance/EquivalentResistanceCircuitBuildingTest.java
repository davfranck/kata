package codingame.equivalentresistance;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.atIndex;

// https://www.codingame.com/ide/puzzle/equivalent-resistance-circuit-building
class EquivalentResistanceCircuitBuildingTest {

    @Test
    void one_leave() {
        Node rootNode = Graph.from("( A )").root();
        assertThat(rootNode.toString()).isEqualTo("( A )");
    }

    @Test
    void two_leaves() {
        Node rootNode = Graph.from("( A A )").root();
        assertThat(rootNode.toString()).isEqualTo("( A A )");
    }

    @Test
    void one_sub_node_alone() {
        Node rootNode = Graph.from("( ( A ) )").root();
        assertThat(rootNode.toString()).isEqualTo("( ( A ) )");
    }

    @Test
    void one_sub_node_one_leave_alone() {
        Node rootNode = Graph.from("( A ( A ) )").root();
        assertThat(rootNode.toString()).isEqualTo("( A ( A ) )");
    }

    @Test
    void sub_sub_nodes() {
        Node rootNode = Graph.from("( A ( B ( C ) ) )").root();
        assertThat(rootNode.toString()).isEqualTo("( A ( B ( C ) ) )");
    }

    @Test
    void sub_nodes_series_and_parallel() {
        Node rootNode = Graph.from("[ A ( B ( C ) [ D E ] ) ]").root();
        assertThat(rootNode.toString()).isEqualTo("[ A ( B ( C ) [ D E ] ) ]");
    }

    @Test
    void compute_simple() {
        assertThat(Graph.from("(A B)").compute(Map.of("A", 20, "B", 10))).isEqualTo(30.0);
    }

    @Test
    void compute_more_complex() {
        Map<String, Integer> resistances = Map.of("Alef", 30, "Bet", 20, "Vet", 10);
        assertThat(Graph.from("( Alef [ ( Bet Bet Bet ) ( Vet [ ( Vet Vet ) ( Vet [ Bet Bet ] ) ] ) ] )").compute(resistances)).isEqualTo(45.0);
    }

    // with input
    @Test
    void input() {
        String input = "3\n" +
                "\n" +
                "A 24\n" +
                "B 8\n" +
                "C 48\n" +
                "[ ( A B ) [ C A ] ]";
        Scanner in = new Scanner(input);
        int N = in.nextInt();
        Map<String, Integer> resistances = new HashMap<>();
        for (int i = 0; i < N; i++) {
            String name = in.next();
            int R = in.nextInt();
            resistances.put(name, R);
        }
        in.nextLine();
        String circuit = in.nextLine();
        double compute = Graph.from(circuit).compute(resistances);

        //assertThat(df.format(compute)).isEqualTo("10.7");
        assertThat(String.format(Locale.ENGLISH, "%.1f", compute)).isEqualTo("10.7");
    }

    // arrondis

    // typer les noeuds en fonction du type serie ou parallèle

    /////////////:::: SPLITTER
    @Test
    void nothing_to_split() {
        assertThat(new Circuit(" A ").splitToString()).singleElement().isEqualTo("A");
    }

    @Test
    void one_sub_part() {
        assertThat(new Circuit("( A )").splitToString()).singleElement().isEqualTo("( A )");
    }

    @Test
    void one_with_name() {
        assertThat(new Circuit("( Alex )").splitToString()).singleElement().isEqualTo("( Alex )");
    }

    @Test
    void one_sub_part_brackets() {
        assertThat(new Circuit("[ A ]").splitToString()).singleElement().isEqualTo("[ A ]");
    }

    @Test
    void two_sub_parts() {
        assertThat(new Circuit("( A ) ( A )").splitToString())
                .hasSize(2)
                .satisfies(first -> assertThat(first).isEqualTo("( A )"), atIndex(0))
                .satisfies(second -> assertThat(second).isEqualTo("( A )"), atIndex(1));
    }

    @Test
    void two_sub_parts_with_names() {
        assertThat(new Circuit("( Alex ) ( Alice )").splitToString())
                .hasSize(2)
                .satisfies(first -> assertThat(first).isEqualTo("( Alex )"), atIndex(0))
                .satisfies(second -> assertThat(second).isEqualTo("( Alice )"), atIndex(1));
    }

    @Test
    void two_sub_parts_bracket() {
        assertThat(new Circuit("[ A ] [ A ]").splitToString())
                .hasSize(2)
                .satisfies(first -> assertThat(first).isEqualTo("[ A ]"), atIndex(0))
                .satisfies(second -> assertThat(second).isEqualTo("[ A ]"), atIndex(1));
    }

    @Test
    void one_sub_group_one_leave() {
        assertThat(new Circuit("( A ) A ").splitToString())
                .hasSize(2)
                .satisfies(first -> assertThat(first).isEqualTo("( A )"), atIndex(0))
                .satisfies(second -> assertThat(second).isEqualTo("A"), atIndex(1));
    }

    @Test
    void one_leave_one_sub_group() {
        assertThat(new Circuit(" A ( A )").splitToString())
                .hasSize(2)
                .satisfies(first -> assertThat(first).isEqualTo("A"), atIndex(0))
                .satisfies(second -> assertThat(second).isEqualTo("( A )"), atIndex(1));
    }

    @Test
    void sub_part_with_sub_part() {
        assertThat(new Circuit("( ( A ) ) ").splitToString()).singleElement().isEqualTo("( ( A ) )");
    }

    @Test
    void complex_split() {
        assertThat(new Circuit("Alef [ ( Bet Bet Bet ) ( Vet [ ( Vet Vet ) ( Vet [ Bet Bet ] ) ] ) ]").splitToString())
                .hasSize(2)
                .satisfies(s -> assertThat(s).isEqualTo("Alef"), atIndex(0))
                .satisfies(s -> assertThat(s).isEqualTo("[ ( Bet Bet Bet ) ( Vet [ ( Vet Vet ) ( Vet [ Bet Bet ] ) ] ) ]"), atIndex(1));
    }

    @Test
    void another_complex_split() {
        assertThat(new Circuit("Vet [ Bet Bet ]").splitToString())
                .hasSize(2)
                .satisfies(s -> assertThat(s).isEqualTo("Vet"), atIndex(0))
                .satisfies(s -> assertThat(s).isEqualTo("[ Bet Bet ]"), atIndex(1));
    }
}
