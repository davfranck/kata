package codingame.achildsplay;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.StringBufferInputStream;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("A childs play solution")
class SolutionTest {

    @Nested
    @DisplayName("No Move")
    class NoMove {
        @Test
        public void noMove_start_at_0_0() {
            // given
            Solution.Map map = new Solution.Map(
                    "O..#........" + System.lineSeparator() +
                            "...........#" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "..#........." + System.lineSeparator() +
                            "..........#.");

            Solution.Robot robot = new Solution.Robot(map);
            // when
            Solution.Position position = robot.position();
            // then
            assertThat(position.x()).isEqualTo(0);
            assertThat(position.y()).isEqualTo(0);
        }

        @Test
        public void noMove_start_at_0_1() {
            // given
            Solution.Map map = new Solution.Map(
                    "...#........" + System.lineSeparator() +
                            "O..........#" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "..#........." + System.lineSeparator() +
                            "..........#.");
            Solution.Robot robot = new Solution.Robot(map);
            // when
            Solution.Position position = robot.position();
            // then
            assertThat(position.x()).isEqualTo(0);
            assertThat(position.y()).isEqualTo(1);
        }
    }

    @Nested
    class MoveWithoutObstacle {
        @Test
        public void one_move_no_obstacle() {
            // given
            Solution.Map map = new Solution.Map(
                    "...#........" + System.lineSeparator() +
                            "O..........#" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "..#........." + System.lineSeparator() +
                            "..........#.");
            Solution.Robot robot = new Solution.Robot(map);
            // when
            robot.move(1);
            // then
            Solution.Position position = robot.position();
            assertThat(position.x()).isEqualTo(0);
            assertThat(position.y()).isEqualTo(0);
        }

        @Test
        public void two_moves_no_obstacle() {
            // given
            Solution.Map map = new Solution.Map(
                    "...#........" + System.lineSeparator() +
                            "...........#" + System.lineSeparator() +
                            "O..........." + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "..#........." + System.lineSeparator() +
                            "..........#.");
            Solution.Robot robot = new Solution.Robot(map);
            // when
            robot.move(2);
            // then
            Solution.Position position = robot.position();
            assertThat(position.x()).isEqualTo(0);
            assertThat(position.y()).isEqualTo(0);
        }
    }

    @Nested
    class MoveWithObstacle {
        @Test
        public void one_move_with_obstacle() {
            // given
            Solution.Map map = new Solution.Map(
                    "#..#........" + System.lineSeparator() +
                            "O..........#" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "..#........." + System.lineSeparator() +
                            "..........#.");
            Solution.Robot robot = new Solution.Robot(map);
            // when
            robot.move(1);
            // then
            Solution.Position position = robot.position();
            assertThat(position.x()).isEqualTo(1);
            assertThat(position.y()).isEqualTo(1);
        }

        @Test
        public void many_moves() {
            // given
            Solution.Map map = new Solution.Map(
                    "#..#........" + System.lineSeparator() +
                            "O..........#" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "............" + System.lineSeparator() +
                            "..#........." + System.lineSeparator() +
                            "..........#.");
            Solution.Robot robot = new Solution.Robot(map);
            // when
            robot.move(127);
            // then
            Solution.Position position = robot.position();
            assertThat(position.x()).isEqualTo(7);
            assertThat(position.y()).isEqualTo(1);
        }

        @Test
        public void big_number_of_moves() {
            // given
            Solution.Map map = new Solution.Map(
                    "..#..........." + System.lineSeparator() +
                            "....#..#......" + System.lineSeparator() +
                            ".#O.....#....." + System.lineSeparator() +
                            ".............." + System.lineSeparator() +
                            ".............." + System.lineSeparator() +
                            ".......##...#." + System.lineSeparator() +
                            "............#." + System.lineSeparator() +
                            ".#........###." + System.lineSeparator() +
                            ".#.#.........." + System.lineSeparator() +
                            "..............");
            Solution.Robot robot = new Solution.Robot(map);
            // when
            robot.move(123456789);
            // then
            Solution.Position position = robot.position();
            assertThat(position.x()).isEqualTo(2);
            assertThat(position.y()).isEqualTo(1);
        }
    }

}