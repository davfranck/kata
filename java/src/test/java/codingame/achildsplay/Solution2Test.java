package codingame.achildsplay;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static codingame.achildsplay.Solution2.MapDimensions.fromWidthAndHeight;
import static codingame.achildsplay.Solution2.Obstacle.fromCoordinate;
import static codingame.achildsplay.Solution2.Robot.atStartPosition;
import static codingame.achildsplay.Solution2.Robot.atStartPositionWithObstacles;
import static org.assertj.core.api.Assertions.assertThat;

// https://www.codingame.com/ide/puzzle/a-childs-play
public class Solution2Test {

    // RG : The robot initially moves upwards.
    @Test
    public void should_move_one_square_upwards_no_obstacle() {
        // given
        Solution2.Robot robot = atStartPosition(0, 1);
        // when
        robot.move(1, fromWidthAndHeight(2, 2));
        // then
        assertThat(robot.isAt(0, 0)).isTrue();
    }

    @Nested
    @DisplayName("Boundary reached")
    class BoundayReached {

        @Test
        public void should_change_direction_when_top_boundary_reached() {
            // given
            Solution2.Robot robot = atStartPosition(0, 0);
            // when
            robot.move(1, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(1, 0)).isTrue();
        }

        @Test
        public void should_change_direction_when_right_boundary_reached() {
            // given
            Solution2.Robot robot = atStartPosition(0, 0);
            Solution2.MapDimensions mapDimensions = fromWidthAndHeight(2, 2);
            // when
            robot.move(2, mapDimensions);
            // then
            assertThat(robot.isAt(1, 1)).as("position du robot : %s", robot).isTrue();
        }
    }

    // The robot stops after n moves.
    @Nested
    @DisplayName("many moves without obstacle")
    class ManyMovesWithoutObstacle {
        @Test
        public void should_move_two_times_up() {
            // given
            Solution2.Robot robot = atStartPosition(0, 2);
            // when
            robot.move(2, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(0, 0)).as("position du robot : %s", robot).isTrue();
        }

        @Test
        public void should_move_two_times_right() {
            // given
            Solution2.Robot robot = atStartPosition(0, 0);
            // when
            robot.move(2, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(1, 1)).as("position du robot : %s", robot).isTrue();
        }

        @Test
        public void should_be_at_start_position_after_four_moves() {
            // given
            Solution2.Robot robot = atStartPosition(0, 0);
            // when
            robot.move(4, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(0, 0)).as("position du robot : %s", robot).isTrue();
        }

        @Test
        public void should_be_at_start_position_after_eight_moves() {
            // given
            Solution2.Robot robot = atStartPosition(0, 0);
            // when
            robot.move(8, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(0, 0)).as("position du robot : %s", robot).isTrue();
        }

        @Test
        public void loop_4_nine_moves() {
            // given
            Solution2.Robot robot = atStartPosition(0, 0);
            // when
            robot.move(9, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(1, 0)).as("position du robot : %s", robot).isTrue();
        }
    }

    @Nested
    @DisplayName("With obstacles")
    class WithObstacles {

        @Test
        public void one_obstacle_should_change_direction() {
            // given
            Solution2.Obstacle obstacle = fromCoordinate(0, 0);
            Solution2.Robot robot = atStartPositionWithObstacles(0, 1, List.of(obstacle));
            // when
            robot.move(1, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(1, 1)).as("position du robot : %s", robot).isTrue();
        }

        @Test
        public void one_obstacle_and_one_boundary_should_change_direction_twice() {
            // given
            Solution2.Obstacle obstacle = fromCoordinate(1, 0);
            Solution2.Robot robot = atStartPositionWithObstacles(0, 0, List.of(obstacle));
            // when
            robot.move(1, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(0, 1)).as("position du robot : %s", robot).isTrue();
        }

        @Test
        public void smallLoop() {
            // given
            Solution2.Obstacle obstacle = fromCoordinate(0, 1);
            Solution2.Robot robot = atStartPositionWithObstacles(0, 0, List.of(obstacle));
            // when
            robot.move(9, fromWidthAndHeight(3, 2));
            // then
            assertThat(robot.isAt(1, 0)).as("position du robot : %s", robot).isTrue();
        }

        //@Disabled
        @Test
        public void a_lot_of_moves_with_obstacles() {
            // given
            Solution2.Obstacle obstacle = fromCoordinate(1, 0);
            Solution2.Robot robot = atStartPositionWithObstacles(0, 0, List.of(obstacle));
            // when
            robot.move(1234567898765434L, fromWidthAndHeight(2, 2));
            // then
            assertThat(robot.isAt(0, 0)).as("position du robot : %s", robot).isTrue();
        }
    }

    @Test
    public void example() {
        // given
        Solution2.Robot robot = atStartPositionWithObstacles(3, 4, List.of(
                fromCoordinate(3, 0),
                fromCoordinate(11, 1),
                fromCoordinate(2, 4),
                fromCoordinate(10, 5)));
        // when
        robot.move(987, fromWidthAndHeight(12, 6));
        // then
        assertThat(robot.isAt(7, 1)).as("position du robot : %s", robot).isTrue();
    }
}
