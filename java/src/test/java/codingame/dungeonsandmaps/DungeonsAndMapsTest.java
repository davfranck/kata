package codingame.dungeonsandmaps;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.atIndex;
import static org.junit.jupiter.params.provider.Arguments.arguments;

// https://www.codingame.com/ide/puzzle/dungeons-and-maps
class DungeonsAndMapsTest {

    @ParameterizedTest
    @MethodSource("pathLengthForValidMap")
    void path_length_for_valid_map(char[][] map, int startRow, int startCol, int expectedLength) {
        // when
        Optional<Integer> pathLength = new DungeonMap(map, startRow, startCol).getPathLength();
        // then
        assertThat(pathLength).isPresent().hasValue(expectedLength);
    }

    @Test
    void invalid_path() {
        char[][] map = {
                {'>', '.'},
                {'.', '.'}
        };
        Optional<Integer> result = new DungeonMap(map, 0, 0).getPathLength();
        assertThat(result).isEmpty();
    }

    @Test
    void path_is_invalid_when_infinite_loop() {
        char[][] map = {
                {'>', 'v'},
                {'^', '<'}
        };
        Optional<Integer> result = new DungeonMap(map, 0, 0).getPathLength();
        assertThat(result).isEmpty();
    }

    @Test
    void path_is_invalid_when_next_index_is_bigger_than_max_row() {
        char[][] map = {
                {'>', 'v'},
                {'T', 'v'}
        };
        Optional<Integer> result = new DungeonMap(map, 0, 0).getPathLength();
        assertThat(result).isEmpty();
    }

    @Test
    void path_is_invalid_when_next_index_is_bigger_than_max_col() {
        char[][] map = {
                {'>', '>'},
                {'T', '.'}
        };
        Optional<Integer> result = new DungeonMap(map, 0, 0).getPathLength();
        assertThat(result).isEmpty();
    }

    @Test
    void should_return_best_path_when_many_maps() {
        // given
        DungeonMap mapLength4 = new DungeonMap(new char[][]{{'>', 'T'}, {'^', '<'}}, 1, 1);
        DungeonMap mapLength3 = new DungeonMap(new char[][]{{'T', '.'}, {'^', '<'}}, 1, 1);
        DungeonMap invalidMap = new DungeonMap(new char[][]{{'.', '.'}, {'^', '<'}}, 1, 1);
        // when
        Optional<Integer> bestDungeonMap = DungeonMaps.bestDungeonMap(List.of(invalidMap, mapLength4, mapLength3));
        // then
        assertThat(bestDungeonMap).isPresent().hasValue(2);
    }

    @Test
    void no_best_path_when_invalid_map() {
        // given
        DungeonMap invalidMap1 = new DungeonMap(new char[][]{{'.', 'T'}, {'^', '<'}}, 1, 1);
        DungeonMap invalidMap2 = new DungeonMap(new char[][]{{'.', '.'}, {'^', '<'}}, 1, 1);
        // when
        Optional<Integer> bestDungeonMap = DungeonMaps.bestDungeonMap(List.of(invalidMap1, invalidMap2));
        // then
        assertThat(bestDungeonMap).isEmpty();
    }

    @Test
    void should_parse_one_dungeon_map_from_file() throws IOException {
        // given
        try (FileInputStream fileInputStream = new FileInputStream("build/resources/test/dungeonsandmaps/example_one_map.txt")) {
            // when
            // contrainte : sur coding game le parsing se fait System.in qui est un InputStream
            List<DungeonMap> maps = DungeonMaps.parse(fileInputStream);
            // then
            DungeonMap expectedMap = new DungeonMap(new char[][]{
                    {'.', '>', '>', 'v'},
                    {'.', '^', '#', 'v'},
                    {'.', '.', '#', 'v'},
                    {'.', '.', '.', 'T'}
            }, 1, 1);
            assertThat(maps).singleElement().isEqualTo(expectedMap);
        }
    }

    @Test
    void should_parse_two_dungeon_maps_from_file() throws IOException {
        // given
        try (FileInputStream fileInputStream = new FileInputStream("build/resources/test/dungeonsandmaps/example_two_maps.txt")) {
            // when
            // contrainte : sur coding game le parsing se fait System.in qui est un InputStream
            List<DungeonMap> maps = DungeonMaps.parse(fileInputStream);
            // then
            DungeonMap expectedMap1 = new DungeonMap(new char[][]{
                    {'.', '>', '>', 'v'},
                    {'.', '^', '#', 'v'},
                    {'.', '.', '#', 'v'},
                    {'.', '.', '.', 'T'}
            }, 1, 1);
            DungeonMap expectedMap2 = new DungeonMap(new char[][]{
                    {'.', '.', '.', '.'},
                    {'.', 'v', '#', '.'},
                    {'.', 'v', '#', '.'},
                    {'.', '>', '>', 'T'}
            }, 1, 1);
            assertThat(maps).hasSize(2)
                    .satisfies(map1 -> assertThat(map1).isEqualTo(expectedMap1), atIndex(0))
                    .satisfies(map2 -> assertThat(map2).isEqualTo(expectedMap2), atIndex(1));
        }
    }

    @Test
    void acceptance() throws IOException {
        //
        try (FileInputStream fileInputStream = new FileInputStream("build/resources/test/dungeonsandmaps/acceptance.txt")) {
            List<DungeonMap> dungeonMaps = DungeonMaps.parse(fileInputStream);
            Optional<Integer> bestMap = DungeonMaps.bestDungeonMap(dungeonMaps);
            assertThat(bestMap).isPresent().hasValue(1);
        }
    }

    static Stream<Arguments> pathLengthForValidMap() {
        return Stream.of(
                arguments(new char[][]{
                        {'.', 'T'},
                        {'.', '.'}}, 0, 1, 1),
                arguments(new char[][]{
                        {'>', 'T'},
                        {'.', '.'}}, 0, 0, 2),
                arguments(new char[][]{
                        {'>', 'T'},
                        {'^', '.'}}, 1, 0, 3),
                arguments(new char[][]{
                        {'>', 'T'},
                        {'^', '<'}}, 1, 1, 4),
                arguments(new char[][]{
                        {'>', '>', 'T'},
                        {'^', 'v', '.'},
                        {'^', '<', '.'}}, 1, 1, 7)
        );
    }

}
