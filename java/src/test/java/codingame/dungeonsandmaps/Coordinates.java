package codingame.dungeonsandmaps;

import java.util.Objects;

public class Coordinates {
    public final int row;
    public final int col;

    public Coordinates(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public Coordinates addToCol(int toAdd) {
        return new Coordinates(row, col + toAdd);
    }

    public Coordinates addToRow(int toAdd) {
        return new Coordinates(row + toAdd, col);
    }

    public boolean isOutOfBound(int maxRow, int maxCol) {
        return row > maxRow || col > maxCol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinates that = (Coordinates) o;
        return row == that.row && col == that.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }
}
