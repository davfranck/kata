package codingame.dungeonsandmaps;

import java.util.*;

public class DungeonMap {
    private final char[][] map;
    private final Coordinates coordinates;
    private final Optional<Integer> pathLength;

    public DungeonMap(char[][] map, int startRow, int startCol) {
        this.map = map;
        this.coordinates = new Coordinates(startRow, startCol);
        this.pathLength = pathLength();
    }

    public Optional<Integer> getPathLength() {
        return pathLength;
    }

    private Optional<Integer> pathLength() {
        try {
            return Optional.of(computePathLength(coordinates, List.of()));
        } catch (InvalidPathException e) {
            return Optional.empty();
        }
    }

    private int computePathLength(Coordinates coordinates, List<Coordinates> visited) {
        if (coordinates.isOutOfBound(map.length - 1, map[map.length - 1].length - 1)) {
            throw new InvalidPathException("Out of bound");
        }
        if (visited.contains(coordinates)) {
            throw new InvalidPathException("Infinite loop");
        }
        List<Coordinates> updatedVisited = new ArrayList<>(visited);
        updatedVisited.add(coordinates);
        switch (map[coordinates.row][coordinates.col]) {
            case 'T':
                return 1;
            case '>':
                return computePathLength(coordinates.addToCol(1), updatedVisited) + 1;
            case '<':
                return computePathLength(coordinates.addToCol(-1), updatedVisited) + 1;
            case 'v':
                return computePathLength(coordinates.addToRow(1), updatedVisited) + 1;
            case '^':
                return computePathLength(coordinates.addToRow(-1), updatedVisited) + 1;
            default:
                throw new InvalidPathException("symbole innatendu");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DungeonMap that = (DungeonMap) o;
        return Arrays.deepEquals(map, that.map)
                && that.coordinates.equals(coordinates);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(coordinates, pathLength);
        result = 31 * result + Arrays.deepHashCode(map);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (char[] row : map) {
            for (char c : row) {
                out.append(c);
            }
            out.append(System.lineSeparator());
        }
        return out.toString();
    }

}
