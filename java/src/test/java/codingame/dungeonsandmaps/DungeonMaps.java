package codingame.dungeonsandmaps;

import java.io.InputStream;
import java.util.*;

class DungeonMaps {

    public static Optional<Integer> bestDungeonMap(List<DungeonMap> maps) {
        Optional<DungeonMap> bestMap = maps.stream().filter(map -> map.getPathLength().isPresent())
                .min(Comparator.comparing(o -> o.getPathLength().get()));
        return bestMap.map(maps::indexOf);
    }

    public static List<DungeonMap> parse(InputStream inputStream) {
        Scanner in = new Scanner(inputStream);
        int w = in.nextInt();
        int h = in.nextInt();
        int startRow = in.nextInt();
        int startCol = in.nextInt();
        int n = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        List<DungeonMap> dungeonMaps = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            char[][] map = new char[h][w];
            for (int j = 0; j < h; j++) {
                String mapRow = in.nextLine();
                map[j] = mapRow.toCharArray();
            }
            dungeonMaps.add(new DungeonMap(map, startRow, startCol));
        }
        return dungeonMaps;
    }
}
