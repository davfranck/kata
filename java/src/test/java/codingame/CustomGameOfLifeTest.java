package codingame;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;

// https://www.codingame.com/ide/puzzle/custom-game-of-life
class CustomGameOfLifeTest {

    @Test
    void one_cell_one_turn_survived() {
        int[] survivingCondition = {1, 0, 0, 0, 0, 0, 0, 0};
        String[][] startSate = {{"O"}};
        int nbTurn = 1;
        String[][] endState = {{"O"}};
        assertThat(new CustomGameOfLife(survivingCondition, new int[]{0, 0, 0, 0, 0, 0, 0, 0}, startSate).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void one_cell_one_turn_not_surviving() {
        int[] survivingCondition = {0, 0, 0, 0, 0, 0, 0, 0};
        String[][] startSate = {{"O"}};
        int nbTurn = 1;
        String[][] endState = {{"."}};
        assertThat(new CustomGameOfLife(survivingCondition, new int[]{0, 0, 0, 0, 0, 0, 0, 0}, startSate).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void two_cells_in_first_row_survived() {
        int[] survivingCondition = {0, 1, 0, 0, 0, 0, 0, 0};
        String[][] startState = {{"O", "O"}};
        int nbTurn = 1;
        String[][] endState = {{"O", "O"}};
        assertThat(new CustomGameOfLife(survivingCondition, new int[]{0, 0, 0, 0, 0, 0, 0, 0}, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void two_cells_in_first_row_die() {
        int[] survivingCondition = {0, 0, 0, 0, 0, 0, 0, 0};
        String[][] startState = {{"O", "O"}};
        int nbTurn = 1;
        String[][] endState = {{".", "."}};
        assertThat(new CustomGameOfLife(survivingCondition, new int[]{0, 0, 0, 0, 0, 0, 0, 0}, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void three_cells_in_first_row_survived() {
        String[][] startState = {{"O", "O", "O"}};
        int[] survivingCondition = {0, 1, 1, 0, 0, 0, 0, 0};
        int nbTurn = 1;
        String[][] endState = {{"O", "O", "O"}};
        assertThat(new CustomGameOfLife(survivingCondition, new int[]{0, 0, 0, 0, 0, 0, 0, 0}, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void three_cells_in_first_row_one_die() {
        String[][] startState = {{"O", "O", "O"}};
        int[] survivingCondition = {0, 1, 0, 0, 0, 0, 0, 0};
        int[] birthCondition = {0, 0, 0, 0, 0, 0, 0, 0};
        int nbTurn = 1;
        String[][] endState = {{"O", ".", "O"}};
        assertThat(new CustomGameOfLife(survivingCondition, birthCondition, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void two_rows_two_cells_survived() {
        String[][] startState = {{"O"}, {"O"}};
        int[] survivingCondition = {0, 1, 0, 0, 0, 0, 0, 0};
        int nbTurn = 1;
        String[][] endState = {{"O"}, {"O"}};
        assertThat(new CustomGameOfLife(survivingCondition, new int[]{0, 0, 0, 0, 0, 0, 0, 0}, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void two_rows_two_columns_all_survived() {
        String[][] startState = {{"O", "O"}, {"O", "O"}};
        int[] survivingCondition = {0, 0, 0, 1, 0, 0, 0, 0};
        int nbTurn = 1;
        String[][] endState = {{"O", "O"}, {"O", "O"}};
        assertThat(new CustomGameOfLife(survivingCondition, new int[]{0, 0, 0, 0, 0, 0, 0, 0}, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void two_rows_two_columns_one_dead_cell() {
        String[][] startState = {{"O", "."}, {"O", "O"}};
        int[] survivingCondition = {0, 0, 1, 1, 0, 0, 0, 0};
        int[] birthCondition = {0, 0, 0, 0, 0, 0, 0, 0};
        int nbTurn = 1;
        String[][] endState = {{"O", "."}, {"O", "O"}};
        assertThat(new CustomGameOfLife(survivingCondition, birthCondition, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void back_to_life_if_no_neighbour() {
        String[][] startState = {{"."}};
        int[] survivingCondition = {0, 0, 0, 0, 0, 0, 0, 0};
        int[] birthCondition = {1, 0, 0, 0, 0, 0, 0, 0};
        int nbTurn = 1;
        String[][] endState = {{"O"}};
        assertThat(new CustomGameOfLife(survivingCondition, birthCondition, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void two_turns() {
        String[][] startState = {{"."}};
        int[] survivingCondition = {0, 0, 0, 0, 0, 0, 0, 0};
        int[] birthCondition = {1, 0, 0, 0, 0, 0, 0, 0};
        int nbTurn = 2;
        String[][] endState = {{"."}};
        assertThat(new CustomGameOfLife(survivingCondition, birthCondition, startState).play(nbTurn)).isEqualTo(endState);
    }

    @Test
    void test_with_input() {
        String input = "3 4 1" + System.lineSeparator() +
                "001100000" + System.lineSeparator() +
                "000100000" + System.lineSeparator() +
                ".OO." + System.lineSeparator() +
                "O..O" + System.lineSeparator() +
                ".OO.";
        CustomGameOfLife customGameOfLife = execute(input);
        String expected = ".OO." + System.lineSeparator() +
                "O..O" + System.lineSeparator() +
                ".OO.";
        assertThat(customGameOfLife.toString()).isEqualTo(expected);
    }

    @Test
    void test_with_another_input() {

        String input = "5 5 1" + System.lineSeparator() +
                "001100000" + System.lineSeparator() +
                "000100000" + System.lineSeparator() +
                "....." + System.lineSeparator() +
                ".OOO." + System.lineSeparator() +
                ".OOO." + System.lineSeparator() +
                ".OOO." + System.lineSeparator() +
                ".....";
        CustomGameOfLife customGameOfLife = execute(input);
        String expected = "..O.." + System.lineSeparator() +
                ".O.O." + System.lineSeparator() +
                "O...O" + System.lineSeparator() +
                ".O.O." + System.lineSeparator() +
                "..O..";
        assertThat(customGameOfLife.toString()).isEqualTo(expected);
    }

    private CustomGameOfLife execute(String input) {
        Scanner in = new Scanner(input);
        int h = in.nextInt();
        int w = in.nextInt();
        int n = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        String alive = in.nextLine();
        String dead = in.nextLine();
        String[][] startState = new String[h][];
        for (int i = 0; i < h; i++) {
            String line = in.nextLine();
            startState[i] = line.split("");
        }

        int[] survivingCondition = Arrays.stream(alive.split(""))
                .mapToInt(Integer::valueOf)
                .toArray();
        int[] birthCondition = Arrays.stream(dead.split(""))
                .mapToInt(Integer::valueOf)
                .toArray();
        CustomGameOfLife customGameOfLife = new CustomGameOfLife(survivingCondition, birthCondition, startState);
        customGameOfLife.play(n);
        return customGameOfLife;
    }
}
