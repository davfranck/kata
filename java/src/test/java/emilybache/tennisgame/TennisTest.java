package emilybache.tennisgame;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

// Source : https://github.com/emilybache/Tennis-Refactoring-Kata
public class TennisTest {

    static Stream<Arguments> getAllScores() {
        return Stream.of(
                Arguments.arguments(0, 0, "Love-All"),
                Arguments.arguments(1, 1, "Fifteen-All"),
                Arguments.arguments(2, 2, "Thirty-All"),
                Arguments.arguments(3, 3, "Deuce"),
                Arguments.arguments(4, 4, "Deuce"),

                Arguments.arguments(1, 0, "Fifteen-Love"),
                Arguments.arguments(0, 1, "Love-Fifteen"),
                Arguments.arguments(2, 0, "Thirty-Love"),
                Arguments.arguments(0, 2, "Love-Thirty"),
                Arguments.arguments(3, 0, "Forty-Love"),
                Arguments.arguments(0, 3, "Love-Forty"),
                Arguments.arguments(4, 0, "Win for player1"),
                Arguments.arguments(0, 4, "Win for player2"),

                Arguments.arguments(2, 1, "Thirty-Fifteen"),
                Arguments.arguments(1, 2, "Fifteen-Thirty"),
                Arguments.arguments(3, 1, "Forty-Fifteen"),
                Arguments.arguments(1, 3, "Fifteen-Forty"),
                Arguments.arguments(4, 1, "Win for player1"),
                Arguments.arguments(1, 4, "Win for player2"),

                Arguments.arguments(3, 2, "Forty-Thirty"),
                Arguments.arguments(2, 3, "Thirty-Forty"),
                Arguments.arguments(4, 2, "Win for player1"),
                Arguments.arguments(2, 4, "Win for player2"),

                Arguments.arguments(4, 3, "Advantage player1"),
                Arguments.arguments(3, 4, "Advantage player2"),
                Arguments.arguments(5, 4, "Advantage player1"),
                Arguments.arguments(4, 5, "Advantage player2"),
                Arguments.arguments(15, 14, "Advantage player1"),
                Arguments.arguments(14, 15, "Advantage player2"),

                Arguments.arguments(6, 4, "Win for player1"),
                Arguments.arguments(4, 6, "Win for player2"),
                Arguments.arguments(16, 14, "Win for player1"),
                Arguments.arguments(14, 16, "Win for player2")
        );
    }

    public void checkAllScores(TennisGame game, int player1Score, int player2Score, String expectedScore) {
        int highestScore = Math.max(player1Score, player2Score);
        for (int i = 0; i < highestScore; i++) {
            if (i < player1Score)
                game.wonPoint("player1");
            if (i < player2Score)
                game.wonPoint("player2");
        }
        assertEquals(expectedScore, game.getScore());
    }

    @ParameterizedTest
    @MethodSource("getAllScores")
    public void checkAllScoresTennisGame1(int player1Score, int player2Score, String expectedScore) {
        TennisGame1 game = new TennisGame1("player1", "player2");
        checkAllScores(game, player1Score, player2Score, expectedScore);
    }

    @ParameterizedTest
    @MethodSource("getAllScores")
    public void checkAllScoresTennisGame2(int player1Score, int player2Score, String expectedScore) {
        TennisGame2 game = new TennisGame2("player1", "player2");
        checkAllScores(game, player1Score, player2Score, expectedScore);
    }

    @ParameterizedTest
    @MethodSource("getAllScores")
    public void checkAllScoresTennisGame3(int player1Score, int player2Score, String expectedScore) {
        TennisGame3 game = new TennisGame3("player1", "player2");
        checkAllScores(game, player1Score, player2Score, expectedScore);
    }

    @ParameterizedTest
    @MethodSource("getAllScores")
    public void checkAllScoresTennisGame4(int player1Score, int player2Score, String expectedScore) {
        TennisGame game = new TennisGame4("player1", "player2");
        checkAllScores(game, player1Score, player2Score, expectedScore);
    }

}
