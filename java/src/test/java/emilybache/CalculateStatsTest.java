package emilybache;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

// https://www.sammancoaching.org/kata_descriptions/calc_stats.html
public class CalculateStatsTest {

    @Test
    void one_number() {
        Stats stats = calculateStats(List.of(1));
        assertThat(stats).isEqualTo(new Stats(1, 1, 1, 1d));
    }

    @Test
    void many_numbers() {
        Stats stats = calculateStats(List.of(6, 9, 15, -2, 92, 11));
        assertThat(stats).isEqualTo(new Stats(-2, 92, 6, 21.833333d));
    }

    private Stats calculateStats(List<Integer> inputNumbers) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int sum = 0;
        for (Integer current : inputNumbers) {
            if (current < min) {
                min = current;
            }
            if (current > max) {
                max = current;
            }
            sum += current;
        }
        int nbElem = inputNumbers.size();
        double scale = Math.pow(10, 6);
        double average = Math.round((double) sum / nbElem * scale) / scale;
        return new Stats(min, max, nbElem, average);
    }

    private class Stats {
        private final int min;
        private final int max;
        private final int nbElements;
        private final double average;

        public Stats(int min, int max, int nbElements, double average) {
            this.min = min;
            this.max = max;
            this.nbElements = nbElements;
            this.average = average;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Stats stats = (Stats) o;
            return min == stats.min && max == stats.max && nbElements == stats.nbElements && Double.compare(stats.average, average) == 0;
        }

        @Override
        public int hashCode() {
            return Objects.hash(min, max, nbElements, average);
        }

        @Override
        public String toString() {
            return "Stats{" +
                    "min=" + min +
                    ", max=" + max +
                    ", nbElements=" + nbElements +
                    ", average=" + average +
                    '}';
        }
    }
}
