package emilybache.gildedrose;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Nested
    class AgedBrieTest {
        @Test
        void aged_brie() {
            Item agedBrie = new Item("Aged Brie", 1, 1);
            GildedRose app = new GildedRose(new Item[]{agedBrie});
            app.updateQuality();
            assertEquals(2, agedBrie.quality);
            assertEquals(0, agedBrie.sellIn);
        }

        @Test
        void aged_brie_with_sell_in_goes_under_zero() {
            Item agedBrie = new Item("Aged Brie", 0, 1);
            GildedRose app = new GildedRose(new Item[]{agedBrie});
            app.updateQuality();
            assertEquals(3, agedBrie.quality);
            assertEquals(-1, agedBrie.sellIn);
        }

        @Test
        void aged_brie_no_extra_quality_update_when_sell_in_below_zero() {
            Item agedBrie = new Item("Aged Brie", 0, 49);
            GildedRose app = new GildedRose(new Item[]{agedBrie});
            app.updateQuality();
            assertEquals(50, agedBrie.quality);
            assertEquals(-1, agedBrie.sellIn);
        }

        @Test
        void aged_brie_quality_above_fifty() {
            Item agedBrie = new Item("Aged Brie", 10, 80);
            GildedRose app = new GildedRose(new Item[]{agedBrie});
            app.updateQuality();
            assertEquals(80, agedBrie.quality);
            assertEquals(9, agedBrie.sellIn);
        }
    }

    @Nested
    class Dexterity {

        @Test
        void dexterity_vest() {
            Item dexterityVest = new Item("+5 Dexterity Vest", 10, 20);
            GildedRose app = new GildedRose(new Item[]{dexterityVest});
            app.updateQuality();
            assertEquals(19, dexterityVest.quality);
            assertEquals(9, dexterityVest.sellIn);
        }

        @Test
        void dexterity_vest_sell_in_below_zero() {
            Item dexterityVest = new Item("+5 Dexterity Vest", 0, 20);
            GildedRose app = new GildedRose(new Item[]{dexterityVest});
            app.updateQuality();
            assertEquals(18, dexterityVest.quality);
            assertEquals(-1, dexterityVest.sellIn);
        }

        @Test
        void dexterity_vest_quality_is_zero() {
            Item dexterityVest = new Item("+5 Dexterity Vest", 0, 0);
            GildedRose app = new GildedRose(new Item[]{dexterityVest});
            app.updateQuality();
            assertEquals(0, dexterityVest.quality);
            assertEquals(-1, dexterityVest.sellIn);
        }
    }

    @Nested
    class BackstageTest {
        @Test
        void backstage() {
            Item backstage = new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20);
            GildedRose app = new GildedRose(new Item[]{backstage});
            app.updateQuality();
            assertEquals(21, backstage.quality);
            assertEquals(14, backstage.sellIn);
        }

        @Test
        void backstage_sell_in_less_than_11() {
            Item backstage = new Item("Backstage passes to a TAFKAL80ETC concert", 10, 20);
            GildedRose app = new GildedRose(new Item[]{backstage});
            app.updateQuality();
            assertEquals(22, backstage.quality);
            assertEquals(9, backstage.sellIn);
        }

        @Test
        void backstage_sell_in_less_than_11_with_quantity_more_than_50_after_update() {
            Item backstage = new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49);
            GildedRose app = new GildedRose(new Item[]{backstage});
            app.updateQuality();
            assertEquals(50, backstage.quality);
            assertEquals(9, backstage.sellIn);
        }

        @Test
        void backstage_sell_in_less_than_6() {
            Item backstage = new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20);
            GildedRose app = new GildedRose(new Item[]{backstage});
            app.updateQuality();
            assertEquals(23, backstage.quality);
            assertEquals(4, backstage.sellIn);
        }

        @Test
        void backstage_sell_in_less_than_6_and_quality_more_than_50_afer_update() {
            Item backstage = new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49);
            GildedRose app = new GildedRose(new Item[]{backstage});
            app.updateQuality();
            assertEquals(50, backstage.quality);
            assertEquals(4, backstage.sellIn);
        }

        @Test
        void backstage_sell_in_less_than_zero() {
            Item backstage = new Item("Backstage passes to a TAFKAL80ETC concert", 0, 20);
            GildedRose app = new GildedRose(new Item[]{backstage});
            app.updateQuality();
            assertEquals(0, backstage.quality);
            assertEquals(-1, backstage.sellIn);
        }
    }

    @Nested
    class SufurusTest {

        @Test
        void sulfurus() {
            Item sulfurus = new Item("Sulfuras, Hand of Ragnaros", 0, 30);
            GildedRose app = new GildedRose(new Item[]{sulfurus});
            app.updateQuality();
            assertEquals(30, sulfurus.quality);
            assertEquals(0, sulfurus.sellIn);
        }

        @Test
        void sulfurus_sell_in_less_that_zero() {
            Item sulfurus = new Item("Sulfuras, Hand of Ragnaros", -1, 30);
            GildedRose app = new GildedRose(new Item[]{sulfurus});
            app.updateQuality();
            assertEquals(30, sulfurus.quality);
            assertEquals(-1, sulfurus.sellIn);
        }

    }

    @Nested
    class ElixirTest {

        @Test
        void elixir() {
            Item elixir = new Item("Elixir of the Mongoose", 5, 7);
            GildedRose app = new GildedRose(new Item[]{elixir});
            app.updateQuality();
            assertEquals(6, elixir.quality);
            assertEquals(4, elixir.sellIn);
        }
    }

    @Test
    void many_items() {
        Item elixir = new Item("Elixir of the Mongoose", 5, 7);
        Item sulfurus = new Item("Sulfuras, Hand of Ragnaros", -1, 30);
        GildedRose app = new GildedRose(new Item[]{elixir, sulfurus});
        app.updateQuality();
        assertEquals(6, elixir.quality);
        assertEquals(4, elixir.sellIn);
        assertEquals(30, sulfurus.quality);
        assertEquals(-1, sulfurus.sellIn);
    }

    @Nested
    class ConjuredTest {

        @Test
        void conjured_sell_in_remains_above_zero() {
            Item conjured = new Item("Conjured", 5, 7);
            GildedRose app = new GildedRose(new Item[]{conjured});
            app.updateQuality();
            assertEquals(5, conjured.quality);
            assertEquals(4, conjured.sellIn);
        }

        @Test
        void conjured_sell_in_below_zero() {
            Item conjured = new Item("Conjured", 0, 7);
            GildedRose app = new GildedRose(new Item[]{conjured});
            app.updateQuality();
            assertEquals(3, conjured.quality);
            assertEquals(-1, conjured.sellIn);
        }
    }
}
