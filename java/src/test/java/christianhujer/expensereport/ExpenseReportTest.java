package christianhujer.expensereport;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;

import static christianhujer.expensereport.ExpenseType.*;

class ExpenseReportTest {

    private final Clock fixedClock = Clock.fixed(Instant.parse("2007-12-03T10:15:30.00Z"), ZoneId.of("Europe/Paris"));
    private final StringReport report = new StringReport();

    @Test
    void printReport_all_expenses() {
        // given
        Expense dinner = new Expense(DINNER, 1);
        Expense dinner2 = new Expense(DINNER, 5001);
        Expense breakfast = new Expense(BREAKFAST, 2);
        Expense breakfastWithMark = new Expense(BREAKFAST, 1001);
        Expense carRental = new Expense(CAR_RENTAL, 1);
        Expense lunch = new Expense(LUNCH, 1000);
        Expense lunchWithMark = new Expense(LUNCH, 2001);

        // when
        new ExpenseReport(report, fixedClock).printReport(List.of(dinner, breakfast, dinner2, breakfastWithMark, carRental, lunch, lunchWithMark));

        // then
        Approvals.verify(report.content);
    }
}