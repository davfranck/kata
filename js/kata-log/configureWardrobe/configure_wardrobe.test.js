const Wardrobes = require('./configure_wardrobe.js');


describe('Wardrobess', () => {
    test('one available size', () => {
        expect(new Wardrobes([50], 50).configurations()).toEqual([[50]]);
        expect(new Wardrobes([40], 50).configurations()).toEqual([[40]]);
        expect(new Wardrobes([60], 50).configurations()).toEqual([]);
        expect(new Wardrobes([40], 80).configurations()).toEqual([[40, 40]]);
        expect(new Wardrobes([10], 30).configurations()).toEqual([[10, 10, 10]]);
    });

    test('two available sizes', () => {
        expect(new Wardrobes([10, 20], 30).configurations()).toEqual([[10, 10, 10], [10, 20]]);
        expect(new Wardrobes([10, 20], 40).configurations()).toEqual([[10, 10, 10, 10], [10, 10, 20], [20, 20]]);
    });

    test('three available size', () => {
        expect(new Wardrobes([10, 20, 30], 30).configurations()).toEqual([[10, 10, 10], [10, 20], [30]]);
    });
});