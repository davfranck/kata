class Wardrobes {

    constructor(availableWardrobes, wallLength) {
        this.availableWardrobes = availableWardrobes.sort();
        this.wallLength = wallLength;
        this.combinations = [];
    }

    firstWardrobe() {
        return this.availableWardrobes[0];
    }
    
    /**
     * Retourne les combinaisons qui replissent le mur à 100%
     */
    configurations() {
        const smallesWardrobeIsBiggerThanWall = this.firstWardrobe() > this.wallLength;
        if(smallesWardrobeIsBiggerThanWall) {
            return [];
        }

        const smallest = this.smallestCombination();
        this.combinations.push(smallest.values);
        if(this.availableWardrobes.length > 1) {
            this.explore(1, smallest.values);
        }
        return this.combinations;
    }

    /**
     * La plus petite combinaison de départ
     */
    smallestCombination() {
        let sum = 0;
        const currentCombination = [];
        while (sum + this.firstWardrobe() <= this.wallLength) {
            currentCombination.push(this.firstWardrobe());
            sum += this.firstWardrobe();
        }
        return new Combination(currentCombination);
    }

    explore(indexOfNewValue, previousCombination){
        const nextValue = this.availableWardrobes[indexOfNewValue];
        const indexOfLastReplacableValue = this.indexOfLastReplacableValue(previousCombination, nextValue);
        if(indexOfLastReplacableValue != -1) {
            const currentCombination = [];
            currentCombination.push(nextValue);
            const remainder = previousCombination.slice(indexOfLastReplacableValue + 1);
            const nextToExplore = remainder.concat(currentCombination)
            this.combinations.push(nextToExplore);
            if(nextToExplore.filter((it) => it !== nextValue).length !== 0) {
                return this.explore(indexOfNewValue, nextToExplore);
            }
        }
        // on n'a pas réussi à remplacer, on passe alors à la valeur suivante s'il y en a
        if(indexOfNewValue < this.availableWardrobes.length) {
            return this.explore(indexOfNewValue + 1, previousCombination);
        }
    }

    indexOfLastReplacableValue(previousCombination, nextValue) {
        let sum = 0;
        for(let i = 0; i < previousCombination.length; i++) {
            sum += previousCombination[i];
            if(sum === nextValue) {
                return i;
            }
        }
        return -1;
    }

}

class Combination {
    
    constructor(values) {
        this.values = values;
    }
}

module.exports = Wardrobes;