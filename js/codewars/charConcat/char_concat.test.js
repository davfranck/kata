function char_concat(input) {
	let result = ''
	const inputLength = input.length
	for (let index = 0; index < Math.floor(inputLength / 2); index++) {
		const left = input[index]
		const right = input[inputLength - index - 1]
		result += left + right + (index + 1)
	}
	return result
}

test('two chars', ()=>{
 expect(char_concat('ab')).toBe('ab1')
})

test('4 chars', () => {
	expect(char_concat('abcd')).toBe('ad1bc2')
});
test('abcdef', () => {
	expect(char_concat('abcdef')).toBe('af1be2cd3')
})
test('abc!def', () => {
	expect(char_concat('abc!def')).toBe('af1be2cd3')
})
