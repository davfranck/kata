const getSum = require('./sum_of_numbers');

test('same numbers', () => {
    expect(getSum(0,0)).toBe(0);
    expect(getSum(1,1)).toBe(1);
});

test('first lower', () => {
    expect(getSum(0,1)).toBe(1);
    expect(getSum(1,2)).toBe(3);
    expect(getSum(-1,2)).toBe(2);
})

test('first greater', () => {
    expect(getSum(2,1)).toBe(3);
    expect(getSum(-2,-3)).toBe(-5);
});

test('equals', () => {
    expect(getSum(2,2)).toBe(2);
});