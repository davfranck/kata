const isPangram = require('./pangram.js');

describe('wip', () => {
    test('upper case', () => {
        expect(isPangram('ABCDEFGHIJKLMNOPQRSTUVWXYZ')).toBe(true);
    });
    test('lower case', () => {
        expect(isPangram('abcdefghijklmnopqrstuvwxyz')).toBe(true);
    });
    test('same char upper and lower', () => {
        expect(isPangram('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')).toBe(true);
    });
    test('invalid char number', () => {
        expect(isPangram('ABCDEFGHIJKLMNOPQRSTUVWXYZ1111')).toBe(true);
    });
    test('invalid char punctuation', () => {
        expect(isPangram('ABCDEFGHIJKLMNOPQRSTUVWXYZ,')).toBe(true);
    });
    test('miss one letter', () => {
        expect(isPangram('ABCDEFGHIJKLMNOPQRSTUVWXY,')).toBe(false);
    });
    test('spaces with doobloon', () => {
        expect(isPangram('AABBCDEFGHIJ KLMNOPQRST UVWXYZ')).toBe(true);
    });
    test('inverse order', () => {
        expect(isPangram('BACDEFGHIJ KLMNOPQRST UVWXYZ')).toBe(true);
    });
});