function isPangram(string) {
    const onlyChars = [...string.toUpperCase()].filter((it) => new RegExp("[a-zA-Z]").test(it));
    return new Set(onlyChars).size === 26;
}

module.exports = isPangram;