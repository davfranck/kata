const duplicateEncode = require('./duplicate_encoder.js');

test('one letter', () => {
    expect(duplicateEncode('a')).toBe('(');
});

test('two distinct letters', () => {
    expect(duplicateEncode('ab')).toBe('((');
});

test('two common letters', () => {
    expect(duplicateEncode('aa')).toBe('))');
});

test('two common letters should ignore case', () => {
    expect(duplicateEncode('Aa')).toBe('))');
});

test('more complex word', () => {
    expect(duplicateEncode('abaibu')).toBe(')))()(');
});