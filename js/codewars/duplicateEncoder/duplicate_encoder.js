// https://www.codewars.com/kata/54b42f9314d9229fd6000d9c/javascript
function duplicateEncode(word) {
    const wordAsArray = [...word.toLowerCase()];
    return wordAsArray
        .map((it) => (wordAsArray.filter((it2) => it === it2).length > 1 ? ')' : '('))
        .join('');
}

module.exports = duplicateEncode;