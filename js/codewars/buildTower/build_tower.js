function towerBuilder(nFloors) {
    const result = [];
    for(let i = 0; i < nFloors; i++) {
        const spaces = duplicateChar(' ', nFloors - 1 - i);
        const stars = duplicateChar('*', 1 + (2 * i));
        result[i] = spaces + stars + spaces;
    }
    return result;
}

function duplicateChar(c, times) {
    return times === 0 ? '' : Array.from(Array(times).keys()).map(() => c).join('');
}

module.exports = towerBuilder ;