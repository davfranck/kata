const towerBuilder = require('./build_tower.js');

test('one floor', () => {
    expect(towerBuilder(1)).toEqual(['*']);
});

test('two floors', () => {
    expect(towerBuilder(2)).toEqual([' * ', '***']);
});