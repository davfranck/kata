const maxSequence = require('./max_sub_array_sum');

test('empty list', () => {
    expect(maxSequence([])).toBe(0);
});

test('one number', () => {
    expect(maxSequence([1])).toEqual(1);
});

test('two positive numbers', () => {
    expect(maxSequence([1,2])).toEqual(3);
});

test('one positive one negative', () =>{
    expect(maxSequence([1,-1])).toBe(1);
});

test('one negative one positive', () => {
    expect(maxSequence([-1, 1])).toBe(1);
});

test('sequence with negative inside to include in calculus', () => {
    expect(maxSequence([2, -1, 4])).toBe(5);
});

test('sequence with negative inside to exclude from calculus', () => {
    expect(maxSequence([1, -2, 3])).toBe(3);
});

test('exercise sample sequence', () => {
    expect(maxSequence([-2, 1, -3, 4, -1, 2, 1, -5, 4])).toBe(6);
});

test('all negative sum should be zero', () => {
    expect(maxSequence([-1, -3])).toBe(0);
});