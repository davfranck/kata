// https://www.codewars.com/kata/54521e9ec8e60bc4de000d6c/javascript
function maxSequence(arr){
    let maxSum = 0;
    let currentSum = 0;
    arr.forEach((number) => {
        let newSum = currentSum + number;
        currentSum = newSum > 0 ? newSum : 0;
        maxSum = currentSum > maxSum ? currentSum : maxSum;        
    });
    return maxSum;
}

module.exports = maxSequence;