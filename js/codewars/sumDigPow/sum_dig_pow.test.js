const sumDigPow = require('./sum_dig_pow.js');

test('one number', () => {
    expect(sumDigPow(1,1)).toEqual([1]);
    expect(sumDigPow(89, 89)).toEqual([89]);
});

test('two numbers', () => {
    expect(sumDigPow(1,2)).toEqual([1, 2]);
    expect(sumDigPow(9,10)).toEqual([9]);
});

test('10 to 100', () => {
    expect(sumDigPow(10,100)).toEqual([89]);
});

test('expect empty', () => {
    expect(sumDigPow(90, 100)).toEqual([]);
});
