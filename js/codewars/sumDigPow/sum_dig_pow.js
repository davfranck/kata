function sumDigPow(lowerBound, upperBound) {
    const expected = [];
    for(let i = lowerBound; i <= upperBound; i++) {
        if(sumOfNumEqualsPow(i)) {
            expected.push(i);
        }
    }
    return expected;
}

function sumOfNumEqualsPow(number) {
    let result = 0;
    new String(number).split('').forEach((n, i) => {
        result += Math.pow(n, i+1);
    });
    return result === number;
}

module.exports = sumDigPow;