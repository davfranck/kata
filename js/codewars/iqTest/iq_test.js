function iqTest(numbers) {
    const even = [];
    const odd = [];
    const numbersAsArray = numbers.split(" ");
    numbersAsArray.forEach(it => {
        let array = it % 2 === 0 ? even : odd;
        array.push(it);
    });
    const arrayWithOneItem = even.length === 1 ? even : odd;
    return numbersAsArray.indexOf(arrayWithOneItem[0]) + 1;
}

module.exports = iqTest;