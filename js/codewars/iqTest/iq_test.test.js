const iqTest = require('./iq_test');

test('one number', () =>  {
    expect(iqTest("1")).toEqual(1);
    expect(iqTest("2")).toEqual(1);
});

test('many numbers', () => {
    expect(iqTest("2 3 3")).toBe(1);
    expect(iqTest("2 4 7 8 10")).toBe(3);
    expect(iqTest("1 2 1 1")).toBe(2);
});