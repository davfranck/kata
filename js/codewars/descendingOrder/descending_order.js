function descendingOrder(n){
    const ordredString = [...new String(n)]
        .map((it) => parseInt(it))
        .sort()
        .reverse()
        .map((it) => new String(it))
        .join('')
    return parseInt(ordredString);
}

module.exports = descendingOrder;