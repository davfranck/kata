const descendingOrder = require('./descending_order.js');

test('one number', () => {
    expect(descendingOrder(1)).toBe(1);
});

test('two already sorted numbers', () => {
    expect(descendingOrder(21)).toBe(21);
});

test('two unsorted numbers', () => {
    expect(descendingOrder(12)).toBe(21);
});

test('with repetition', () => {
    expect(descendingOrder(123354)).toBe(543321);
});