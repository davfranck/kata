const oddOrEven = require('./sum_odd_or_even');

test('empty', () => {
    expect(oddOrEven([])).toBe("even");
})

test('one digit', () =>  {
    expect(oddOrEven([0])).toBe("even");
    expect(oddOrEven([1])).toBe("odd");
});

test('two digits', () => {
    expect(oddOrEven([1,1])).toBe("even");
    expect(oddOrEven([1,2])).toBe("odd");
    expect(oddOrEven([1,-2])).toBe("odd");
});

test('mor digits', () => {
    expect(oddOrEven([1,2,3])).toBe("even");
});
