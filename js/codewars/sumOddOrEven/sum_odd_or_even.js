function oddOrEven(array) {
    if(array.length === 0) array[0] = 0;
    return array.reduce((acc, value) => acc + value) % 2 === 0 ? "even" : "odd";
}

module.exports = oddOrEven;