function rot13(message) {
    return message.split('').map((it) => rot13Letter(it)).join('');
}

function rot13Letter(letter) {
    if(!RegExp(/[A-Za-z]/).test(letter)) {
        return letter;
    }
    const letterCode = letter.charCodeAt(0);
    let letterRot13 = letterCode + 13;
    if (letterCode < 91 && letterRot13 >= 91) {
        letterRot13 = 65 + letterRot13 - 91;
    } else if(letterRot13 >= 123) {
        letterRot13 = 97 + letterRot13 - 123;
    }
    return String.fromCharCode(letterRot13);
}

module.exports = rot13;