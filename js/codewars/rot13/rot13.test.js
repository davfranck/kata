const rot13 = require('./rot13');

test('one letter', () => {
    expect(rot13("e")).toBe("r");
    expect(rot13("s")).toBe("f");
    expect(rot13("S")).toBe("F");
});

test('two letters', () => {
    expect(rot13("es")).toBe("rf");
    expect(rot13("eS")).toBe("rF");
});

test('not alphabetic char', ()  => {
    expect(rot13("12&")).toBe("12&");
});

test('empty', () => {
    expect(rot13("")).toBe("");
})