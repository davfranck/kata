 function circleArea(radius) {
    if (radius <= 0) {
        throw new Error()
    }
    return Math.round(Math.PI * radius * radius * 1000) / 1000;
}

module.exports = circleArea