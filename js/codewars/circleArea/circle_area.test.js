const circleArea = require('./circle_area.js')

test('radius is one', () => {
    expect(circleArea(1)).toBe(3.142)
})
test('radius is 68', () => {
    expect(circleArea(68)).toBe(14526.724)
})
test('radius is 43.2673', () => {
    expect(circleArea(43.2673)).toBe(5881.248)
})
// test skip car pour une raison que j'ignore ça plante
test.skip('radius is 0 then throw error', () => {
    expect(circleArea(0)).toThrow()
})