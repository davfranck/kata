const numberToString = require('./number_to_string.js');

test('number 1 as string', () => {
    expect(numberToString(1)).toBe('1');
});