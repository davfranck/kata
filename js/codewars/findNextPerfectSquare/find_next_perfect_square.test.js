const findNextSquare = require('./find_next_perfect_square.js');

test('not perfect square', () => {
    expect(findNextSquare(114)).toBe(-1);
});

test('next perfect square', () => {
    expect(findNextSquare(4)).toBe(9);
});

test('next perfect square 121', () => {
    expect(findNextSquare(121)).toBe(144);
});

test('next perfect square 625', () => {
    expect(findNextSquare(625)).toBe(676);
});