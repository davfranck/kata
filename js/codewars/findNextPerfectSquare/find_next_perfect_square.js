function findNextSquare(n) {
    return isPerfectSquare(n) ? nextPerfectSquare(n + 1) : -1;
}

function isPerfectSquare(n) {
    return Number.isInteger(Math.sqrt(n));
}

function nextPerfectSquare(n) {
    while(!isPerfectSquare(n)) {
        n = n + 1;
    }
    return n;
}

module.exports = findNextSquare;