const validatePIN = require('./regex_pin_code.js');

test('valid 4 numbers', () => {
    expect(validatePIN("1234")).toBe(true);
});
test('valid 6 numbers', () => {
    expect(validatePIN("123456")).toBe(true);
});

test('5 numbers', () => {
    expect(validatePIN("12345")).toBe(false);
});

test('3 numbers', () => {
    expect(validatePIN("123")).toBe(false);
});

test('4 chars', () => {
    expect(validatePIN("a234")).toBe(false);
});

test('negative number', () => {
    expect(validatePIN("-12345")).toBe(false);
});