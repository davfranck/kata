function validatePIN (pin) {
    return (pin.length === 4 || pin.length === 6) && pin.replace(/[^0-9]/, '').length == pin.length;
    // return /^(\d{4}|\d{6})$/.test(pin); // Solution plus efficace
}

module.exports = validatePIN;