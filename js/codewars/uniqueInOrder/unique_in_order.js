function uniqueInOrder(iterable) {
    const result = [];
    let previous = null;
    for(let i of iterable) {
        if(i !== previous) {
            result.push(i);
        }
        previous = i;
    }
    return result;
}

module.exports = uniqueInOrder;