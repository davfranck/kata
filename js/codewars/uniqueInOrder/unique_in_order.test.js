const uniqueInOrder = require('./unique_in_order');

test('no repetition element', () => {
    expect(uniqueInOrder("A")).toEqual(['A']);
    expect(uniqueInOrder([1])).toEqual([1]);
    expect(uniqueInOrder("AB")).toEqual(['A', 'B']);
    expect(uniqueInOrder([1,2])).toEqual([1,2]);
});

test('repetition', () => {
    expect(uniqueInOrder("AA")).toEqual(['A']);
    expect(uniqueInOrder("AAAABBBCCDAABBB")).toEqual(['A', 'B', 'C', 'D', 'A', 'B']);
    expect(uniqueInOrder([1,2,2,3,3])).toEqual([1,2,3]);
})

test('empty', () => {
    expect(uniqueInOrder("")).toEqual([]);
    expect(uniqueInOrder([])).toEqual([]);
});

// TODO tableau de numbers