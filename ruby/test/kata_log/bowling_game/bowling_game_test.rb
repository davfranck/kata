# frozen_string_literal: true

require 'minitest/autorun'

require_relative '../../../lib/kata_log/bowling_game/bowling_game'

# https://kata-log.rocks/bowling-game-kata
class BowlingGameTest < Minitest::Test
  
  def setup
    @bowling_game = BowlingGame.new
  end

  def test_that_score_is_zero_when_no_ball_rolled
    assert_equal 0, @bowling_game.score
  end

  def test_that_score_is_zero_when_a_roll_with_no_hit
    @bowling_game.roll(0)
    assert_equal 0, @bowling_game.score
  end

  def test_that_score_is_zero_when_a_roll_with_one_hit
    @bowling_game.roll(1)
    assert_equal 1, @bowling_game.score
  end
  
  def test_that_score_is_the_sum_of_two_rolls
    @bowling_game.roll(2)
    @bowling_game.roll(3)
    assert_equal 5, @bowling_game.score
  end

  def test_that_bonus_is_added_when_spare
    @bowling_game.roll(8)
    @bowling_game.roll(2)
    @bowling_game.roll(3)
    assert_equal 8 + 2 + 2 * 3, @bowling_game.score
  end

  def test_that_bonus_is_added_when_spare_and_last_frame_complete
    @bowling_game.roll(8)
    @bowling_game.roll(2)
    @bowling_game.roll(3)
    @bowling_game.roll(5)
    assert_equal 8 + 2 + 2 * 3 + 5, @bowling_game.score
  end

  def test_that_bonus_is_not_added_when_strike_and_frame_not_complete
    @bowling_game.roll(5)
    @bowling_game.roll(2)
    
    @bowling_game.roll(10)

    @bowling_game.roll(3)

    assert_equal 5 + 2 + 10 + 3, @bowling_game.score
  end

  def test_last_frame_is_strike
    @bowling_game.roll(8)
    @bowling_game.roll(1)

    @bowling_game.roll(10)

    @bowling_game.roll(5)
    @bowling_game.roll(1)

    @bowling_game.roll(3)

    assert_equal 8 + 1 + 10 + (5 + 1) * 2 + 3, @bowling_game.score
  end
end
