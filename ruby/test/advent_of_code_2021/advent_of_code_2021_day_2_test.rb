# frozen_string_literal: true

require 'minitest/autorun'

require_relative '../../lib/advent_of_code/advent_of_code_day_2'

class AdventOfCodeDay2 < MiniTest::Test

  def setup
    @submarine = Submarine.new
  end

  def test_no_move
    assert_equal Position.new(0, 0), @submarine.position
  end

  def test_one_move_forward
    Command.of('forward 4').execute(@submarine)
    assert_equal Position.new(4, 0), @submarine.position
  end

  def test_one_down_command_should_not_move_the_submarine
    Command.of('down 7').execute(@submarine)
    assert_equal Position.new(0, 0), @submarine.position
  end

  def test_one_down_command_one_forward_should_change_depth
    Command.of('down 7').execute(@submarine)
    Command.of('forward 2').execute(@submarine)
    assert_equal Position.new(2, 7 * 2), @submarine.position
  end

  def test_one_move_down_one_move_up
    Command.of('down 7').execute(@submarine)
    Command.of('up 3').execute(@submarine)
    Command.of('forward 2').execute(@submarine)
    assert_equal Position.new(2, 4 * 2), @submarine.position
  end

  def test_many_moves
    Command.of('down 7').execute(@submarine)
    Command.of('forward 2').execute(@submarine)
    Command.of('up 3').execute(@submarine)
    Command.of('forward 3').execute(@submarine)
    assert_equal Position.new(5, 26), @submarine.position
  end

  def test_with_input
    input_file = open('D:\Perso\dev\kata\ruby\lib\advent_of_code\resources\input_day_2.txt')
    input_file.each_line do |str_command|
      Command.of(str_command).execute(@submarine)
    end
    assert_equal Position.new(1868, 1078987), @submarine.position
  end

end
