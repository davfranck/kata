# frozen_string_literal: true

require 'minitest/autorun'

require_relative '../../lib/advent_of_code/advent_of_code_day_3'

# https://adventofcode.com/2021/day/3
class AdventOfCodeDay3 < MiniTest::Test

  def setup
  end

  def test_gamma_rate_with_one_number
    input = ['00100']
    assert_equal '00100', Rates.new(input).gamma_rate
  end

  def test_gamma_rate_with_three_numbers
    input = %w[00100 11110 10110]
    assert_equal '10110', Rates.new(input).gamma_rate
  end

  def test_gamma_rate_with_samples
    input = %w[00100 11110 10110 10111 10101 01111 00111 11100 10000 11001 00010 01010]
    assert_equal '10110', Rates.new(input).gamma_rate
  end

  def test_epsilon_rate
    input = %w[00100 11110 10110 10111 10101 01111 00111 11100 10000 11001 00010 01010]
    assert_equal '01001', Rates.new(input).epsilon_rate
  end

  def test_result_part1
    input_file = File.open('D:\Perso\dev\kata\ruby\lib\advent_of_code\resources\input_day_3.txt')
    input = []
    input_file.each_line { |number| input << number.chomp }
    input_file.close
    gamma = Rates.new(input).gamma_rate
    epsilon = Rates.new(input).epsilon_rate
    assert_equal '001100010000', gamma
    assert_equal '110011101111', epsilon
    assert_equal 2595824, gamma.to_i(2) * epsilon.to_i(2)
  end
end
