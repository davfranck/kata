# frozen_string_literal: true

require 'minitest/autorun'

require_relative '../../lib/advent_of_code/advent_of_code_day_1'

class AdventOfCodeDay1 < MiniTest::Test

  def test_increase_is_zero_when_only_one_measure
    measures = [100]
    assert_equal 0, count_increase(measures)
  end

  def test_increase_is_one_given_two_measures
    measures = [100, 101]
    assert_equal 1, count_increase(measures)
  end

  def test_no_increase_when_second_number_is_lower
    measures = [100, 99]
    assert_equal 0, count_increase(measures)
  end

  def test_many_measures
    measures = [100, 99, 100, 102, 103, 80, 81]
    assert_equal 4, count_increase(measures)
  end

  def test_sliding_window_without_increase
    measures = [100, 101, 102, 99]
    assert_equal 0, count_sliding_window_increase(measures)
  end

  def test_sliding_window_with_one_increase
    measures = [100, 101, 102, 103]
    assert_equal 1, count_sliding_window_increase(measures)
  end

  def test_sliding_window_with_sum_equal
    measures = [100, 101, 102, 103, 101]
    assert_equal 1, count_sliding_window_increase(measures)
  end

  def test_sliding_window_with_many_increases
    measures = [100, 101, 102, 50, 152, 103]
    assert_equal 2, count_sliding_window_increase(measures)
  end

  def test_count_from_input
    assert_equal 1311, count_from_input
  end

end
