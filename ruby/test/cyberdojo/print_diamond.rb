# frozen_string_literal: true

require 'minitest/autorun'

require_relative '../../lib/cyberdojo/print_diamond'

class PrintDiamond < Minitest::Test

  def test_diamond_a_letter
    assert_equal "A", Diamond.new.print(0)
  end

  def test_diamond_b_letter
    expected = " A \n" \
               "B B\n" \
               " A "
    assert_equal expected, Diamond.new.print(1)
  end

  def test_diamond_c_letter
    expected = "  A  \n" \
               " B B \n" \
               "C   C\n" \
               " B B \n" \
               "  A  "
    assert_equal expected, Diamond.new.print(2)
  end

  def test_diamond_c_letter
    expected = "   A   \n" \
               "  B B  \n" \
               " C   C \n" \
               "D     D\n" \
               " C   C \n" \
               "  B B  \n" \
               "   A   "
    assert_equal expected, Diamond.new.print(3)
  end

end