# frozen_string_literal: true

require 'minitest/autorun'

require_relative '../../lib/cyberdojo/tennis_game'

# source : https://cyber-dojo.org/creator/choose_problem
# or : https://codingdojo.org/kata/Tennis/
class TennisGameTest < MiniTest::Test

  def test_fifteen_love
    game = TennisGame.new
    game.first_player_win_a_point
    assert_equal 'fifteen-love', game.score
  end

  def test_forty_fifteen
    game = TennisGame.new
    3.times { game.first_player_win_a_point }
    1.times { game.second_player_win_a_point }
    assert_equal 'forty-fifteen', game.score
  end

  def test_deuce_when_both_players_have_3_points
    game = TennisGame.new
    3.times { game.first_player_win_a_point }
    3.times { game.second_player_win_a_point }
    assert_equal 'deuce', game.score
  end

  def test_deuce_when_both_players_have_4_points
    game = TennisGame.new
    4.times { game.first_player_win_a_point }
    4.times { game.second_player_win_a_point }
    assert_equal 'deuce', game.score
  end

  def test_advantage_for_first_player
    game = TennisGame.new
    4.times { game.first_player_win_a_point }
    3.times { game.second_player_win_a_point }
    assert_equal 'advantage player 1', game.score
  end

  def test_advantage_for_second_player
    game = TennisGame.new
    3.times { game.first_player_win_a_point }
    4.times { game.second_player_win_a_point }
    assert_equal 'advantage player 2', game.score
  end

  def test_advantage_for_second_player_when_many_points
    game = TennisGame.new
    13.times { game.first_player_win_a_point }
    14.times { game.second_player_win_a_point }
    assert_equal 'advantage player 2', game.score
  end

  def test_first_player_win_the_game_with_four_points_and_second_player_has_two_points
    game = TennisGame.new
    4.times { game.first_player_win_a_point }
    2.times { game.second_player_win_a_point }
    assert_equal 'win for player 1', game.score
  end

  def test_player_two_wins_with_four_points_and_first_player_has_one_point
    game = TennisGame.new
    1.times { game.first_player_win_a_point }
    4.times { game.second_player_win_a_point }
    assert_equal 'win for player 2', game.score
  end

  def test_player_two_wins_with_ten_points_and_first_player_has_8_points
    game = TennisGame.new
    8.times { game.first_player_win_a_point }
    10.times { game.second_player_win_a_point }
    assert_equal 'win for player 2', game.score
  end

  def test_player_one_wins_with_twelve_points_and_second_player_has_ten_points
    game = TennisGame.new
    12.times { game.first_player_win_a_point }
    10.times { game.second_player_win_a_point }
    assert_equal 'win for player 1', game.score
  end
end