# frozen_string_literal: true

require 'minitest/autorun'

require_relative '../../lib/codewars/stop_spinning_my_words.rb'

# https://www.codewars.com/kata/5264d2b162488dc400000001/train/ruby
describe SpinningWords do

  before do
    @spinning_words = SpinningWords.new
  end

  describe 'when the string is empty' do
    it 'should return an empty string' do
      _(@spinning_words.spin_words('')).must_equal ''
    end
  end
  
  describe 'when one word not longer than 4 letters' do
    it 'nothing change' do
      _(@spinning_words.spin_words('four')).must_equal 'four'
    end
  end

  describe 'when one word with 5 letters' do
    it 'should be reversed' do
      _(@spinning_words.spin_words('fifth')).must_equal 'htfif'
    end
  end

  describe 'two words with less than five letters but a sentence longer than 4 letters' do
    it 'should not reversed any word' do
      _(@spinning_words.spin_words('one two')).must_equal 'one two'
    end
  end

  describe 'two words, one with more than four letters' do
    it 'should reversed one word' do
      _(@spinning_words.spin_words('one thetwo')).must_equal 'one owteht'
    end
  end

  describe 'a bigger sentence with some words bigger than 4 chars' do
    it 'should reversed only words with more than 4 chars' do
      _(@spinning_words.spin_words('one thetwo four thefive')).must_equal 'one owteht four evifeht'
    end
  end

end