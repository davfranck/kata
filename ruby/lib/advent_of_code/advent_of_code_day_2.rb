# frozen_string_literal: true

class Submarine
  attr_reader :position

  def initialize
    @position = Position.new(0, 0)
    @aim = 0
  end

  def forward(amount)
    @position = @position.add_amounts(amount, amount * @aim)
  end

  def down(amount)
    @aim += amount
  end

  def up(amount)
    @aim -= amount
  end
end

# depth and horizontal position
class Position
  attr_reader :horizontal, :depth

  def initialize(horizontal, depth)
    @horizontal = horizontal
    @depth = depth
  end

  def add_amounts(horizontal_amount, depth_amount)
    Position.new(@horizontal + horizontal_amount, depth + depth_amount)
  end

  def eql?(other)
    @horizontal == other.horizontal && @depth == other.depth
  end

  def ==(other)
    eql?(other)
  end

  def hash
    @horizontal.hash ^ @depth.hash
  end
end

class Command
  attr_reader :type, :amount

  def initialize(type, amount)
    @type = type
    @amount = amount.to_i
  end

  def self.of(str_command)
    arr_cmd = str_command.split(' ')
    Command.new(arr_cmd[0], arr_cmd[1])
  end

  def execute(submarine)
    case @type
    when 'forward'
      submarine.forward(@amount)
    when 'down'
      submarine.down(@amount)
    else
      submarine.up(@amount)
    end
  end
end
