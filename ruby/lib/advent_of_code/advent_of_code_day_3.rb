# frozen_string_literal: true

class Rates

  def initialize(input)
    @nb_zero_per_index = Array.new(input[0].length, 0)
    @nb_one_per_index = Array.new(input[0].length, 0)
    init_arrays(input)
  end

  def gamma_rate
    rate = ''
    @nb_zero_per_index.each_index do |index|
      value = @nb_zero_per_index[index] >= @nb_one_per_index[index] ? '0' : '1'
      rate += value
    end
    rate
  end

  def epsilon_rate
    rate = ''
    @nb_zero_per_index.each_index do |index|
      value = @nb_zero_per_index[index] < @nb_one_per_index[index] ? '0' : '1'
      rate += value
    end
    rate
  end

  private

  def init_arrays(input)
    input.each do |binary_str|
      #puts "binary_str : #{binary_str} #{binary_str.split(//)}"
      update_arrays_for_binary_arr(binary_str.split(//))
    end
  end

  def update_arrays_for_binary_arr(binary_arr)
    binary_arr.each_index do |index|
      if binary_arr[index] == '0'
        @nb_zero_per_index[index] += 1
      else
        @nb_one_per_index[index] += 1
      end
    end
  end

end
