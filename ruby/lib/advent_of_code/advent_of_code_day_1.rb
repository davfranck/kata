# frozen_string_literal: true

def count_from_input
  input_file = open('D:\Perso\dev\kata\ruby\lib\advent_of_code\resources\input_day_1_part_1.txt')
  measures = []
  input_file.each_line do |measure|
    measures << measure.to_i
  end
  count_sliding_window_increase(measures)
end

def count_increase(measures)
  return 0 if measures.length.zero?
  
  previous = measures[0]
  count = 0
  measures.each do |measure|
    count += 1 if measure > previous
    previous = measure
  end
  count
end

def count_sliding_window_increase(measures)
  nb_increase = 0
  (1..measures.length - 3).each do |i|
    nb_increase += 1 if sum_three_measures_from(i, measures) > sum_three_measures_from(i - 1, measures)
  end
  nb_increase
end

def sum_three_measures_from(start_index, measures)
  measures[start_index] + measures[start_index + 1] + measures[start_index + 2]
end
