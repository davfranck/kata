# frozen_string_literal: true

class TennisGame
  def initialize
    @first_player_score = 0
    @second_player_score = 0
  end

  def first_player_win_a_point
    @first_player_score += 1
  end

  def second_player_win_a_point
    @second_player_score += 1
  end

  def score
    if a_winner?
      @first_player_score > @second_player_score ? 'win for player 1' : 'win for player 2'
    elsif @first_player_score >= 3 && @second_player_score >= 3 && @first_player_score == @second_player_score
      'deuce'
    elsif @first_player_score >= 3 && @second_player_score >= 3
      @first_player_score > @second_player_score ? 'advantage player 1' : 'advantage player 2'
    else
      scores = { 0 => 'love', 1 => 'fifteen', 2 => 'thirty', 3 => 'forty' }
      "#{scores[@first_player_score]}-#{scores[@second_player_score]}"
    end
  end

  private

  def a_winner?
    (@first_player_score >= 4 || @second_player_score >= 4) && (@first_player_score - @second_player_score).abs >= 2
  end
end
