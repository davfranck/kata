# frozen_string_literal: true

class Diamond
  @@alphabet = %w[A B C D]

  def print(index)
    result = first_half_of_diamond(index)
    result += second_half_of_diamond(index)
    result
  end

  def first_half_of_diamond(index)
    result = ''
    (0..index).each do |i|
      result += "\n" unless result.empty?
      result += line(i, index)
    end
    result
  end

  def second_half_of_diamond(index)
    result = ''
    unless index.zero?
      (index - 1).downto(0) do |i|
        result += "\n#{line(i, index)}"
      end
    end
    result
  end

  def line(current_index, max_index)
    current_letter = @@alphabet[current_index]
    side_spaces = ' ' * (max_index - current_index)
    if !current_index.zero?
      middle_spaces = ' ' * (1 + 2 * (current_index - 1))
      return "#{side_spaces}#{current_letter}#{middle_spaces}#{current_letter}#{side_spaces}"
    else
      return "#{side_spaces}#{current_letter}#{side_spaces}"
    end
  end
end
