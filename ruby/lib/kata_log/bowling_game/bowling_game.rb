# frozen_string_literal: true

class BowlingGame

  def initialize
    @frames = []
  end

  def roll(hit)
    if @frames.empty? || @frames.last.complete? 
      @frames.push Frame.new(hit)
    else
      @frames.last.second_hit=(hit)
    end
  end

  def score
    local_score = 0
    @frames.each_index do |index|
      current_frame = @frames[index]
      local_score += current_frame.score
      local_score += bonus(index, current_frame)
    end

    local_score
  end

  private

  def bonus(index, current_frame)
    if index.positive?
      previous_frame = @frames[index - 1]
      return current_frame.first_hit if previous_frame.spare?
      return current_frame.score if current_frame.complete? && previous_frame.strike?
    end
    0
  end
end

class Frame
  attr_reader :first_hit
  attr_writer :second_hit

  def initialize(hit)
    @first_hit = hit
    @second_hit = nil
  end

  def complete?
    @first_hit == 10 || (@first_hit && @second_hit)
  end

  def score
    @second_hit ? @first_hit + @second_hit : @first_hit
  end

  def spare?
    @second_hit && @first_hit + @second_hit == 10
  end

  def strike?
    @first_hit == 10
  end
end