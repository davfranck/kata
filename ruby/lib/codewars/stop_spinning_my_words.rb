class SpinningWords
  
  def spin_words(sentence)
    words = sentence.split(' ')
    output = ''
    words.each do |word|
      output += output.length.positive? ? ' ' : ''
      output += word.length > 4 ? word.reverse : word
    end
    output
  end
end